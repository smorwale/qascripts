
DECLARE @RoleId nvarchar(1000)

--Create 'Allow All Admin' Role
IF NOT EXISTS (SELECT * FROM DigitalRoles WHERE Name = 'Dev_Allow_All_Admin')
   BEGIN
       INSERT INTO DigitalRoles (Id, InsertAt, UpdateAt, Name, NormalizedName, ConcurrencyStamp, IsAdmin, [Description], IsDefault)
       VALUES (newid(), getdate(), getdate(), 'Dev_Allow_All_Admin', 'DEV_ALLOW_ALL_ADMIN', newid(), 1, 'Development Allow All Admin Group', 0)
   END

SELECT @RoleId = Id FROM DigitalRoles WHERE Name = 'Dev_Allow_All_Admin'

--Assign all feaure permissions to the role
INSERT INTO DigitalRolePermissions (InsertAt, UpdateAt, DigitalRoleId, FeaturePermissionId, Allowed)
SELECT getdate(), getdate(), @RoleId, Id, 1 FROM FeaturePermissions
WHERE Id NOT IN (SELECT FeaturePermissionId FROM DigitalRolePermissions WHERE DigitalRoleId = @RoleId)

--Assign all digital admins to the role
INSERT INTO DigitalAdminRoles(InsertAt, UpdateAt, DigitalRoleId, DigitalAdminId, AssignedBy)
SELECT getdate(), getdate(), @RoleId, Id, Id FROM DigitalAdmins
WHERE Id NOT IN (SELECT DigitalAdminId FROM DigitalAdminRoles WHERE DigitalRoleId = @RoleId)
