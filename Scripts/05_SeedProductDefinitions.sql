
-- Seed DefaultShare

begin tran
if (not exists (select * from ProductDefinitions with (updlock,serializable) where DefaultName = 'DefaultShare'))
begin
   insert into ProductDefinitions (InsertAt, UpdateAt, DefaultName, DefaultTypeName, ExternalIdentifiers, [Type], SubType, ClosedDaysBeforeDelete)
   values (GetDate(), GetDate(), 'DefaultShare', 'DefaultShare', '["ST*"]', 'SubAccount', 'Deposit', 5)
end
commit tran

-- Seed DefaultLoan

begin tran
if (not exists (select * from ProductDefinitions with (updlock,serializable) where DefaultName = 'DefaultLoan'))
begin
   insert into ProductDefinitions (InsertAt, UpdateAt, DefaultName, DefaultTypeName, ExternalIdentifiers, [Type], SubType, ClosedDaysBeforeDelete)
   values (GetDate(), GetDate(), 'DefaultLoan', 'DefaultLoan', '["LT*"]', 'SubAccount', 'Loan', 5)
end
commit tran


begin tran
if (not exists (select * from ProductDefinitions with (updlock,serializable) where DefaultName = 'DefaultCard'))
begin
   insert into ProductDefinitions (InsertAt, UpdateAt, DefaultName, DefaultTypeName, ExternalIdentifiers, [Type], SubType, ClosedDaysBeforeDelete)
   values (GetDate(), GetDate(), 'DefaulCard', 'DefaulCard', '["CRD*"]', 'SubAccount', 'Card', 5)
end
commit tran
