--Seed metadata definitions

Declare @Name varchar(1000), @DisplayName varchar(1000), @Description varchar(1000), @Type varchar(1000), @Template varchar(1000), @FullType varchar(1000)

Set @Name = 'Balance'
Set @DisplayName = 'Balance'
Set @Description = 'Balance'
Set @Type = 'String'
Set @Template = '{model.Balance.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'BalanceAvailable'
Set @DisplayName = 'Balance Available'
Set @Description = 'BalanceAvailable'
Set @Type = 'String'
Set @Template = '{model.BalanceAvailable.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'CloseAt'
Set @DisplayName = 'Close At'
Set @Description = 'CloseAt'
Set @Type = 'String'
Set @Template = '{model.CloseAt.HasValue ? model.CloseAt.Value.ToString("d") : ""}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'CreditAvailable'
Set @DisplayName = 'Credit Available'
Set @Description = 'CreditAvailable'
Set @Type = 'String'
Set @Template = '{model.CreditAvailable.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'CreditBalance'
Set @DisplayName = 'Credit Balance'
Set @Description = 'CreditBalance'
Set @Type = 'String'
Set @Template = '{model.CreditBalance.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'CreditLimit'
Set @DisplayName = 'Credit Limit'
Set @Description = 'CreditLimit'
Set @Type = 'String'
Set @Template = '{model.CreditLimit.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'DividendFromOpen'
Set @DisplayName = 'Dividend From Open'
Set @Description = 'DividendFromOpen'
Set @Type = 'String'
Set @Template = '{model.DividendFromOpen.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'DividendLastMonth'
Set @DisplayName = 'Dividend Last Month'
Set @Description = 'DividendLastMonth'
Set @Type = 'String'
Set @Template = '{model.DividendLastMonth.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'DividendLastYear'
Set @DisplayName = 'Dividend Last Year'
Set @Description = 'DividendLastYear'
Set @Type = 'String'
Set @Template = '{model.DividendLastYear.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'DividendRate'
Set @DisplayName = 'Dividend Rate'
Set @Description = 'DividendRate'
Set @Type = 'String'
Set @Template = '{model.DividendRate}%'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'DividendThisYear'
Set @DisplayName = 'Dividend This Year'
Set @Description = 'DividendThisYear'
Set @Type = 'String'
Set @Template = '{model.DividendThisYear.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'InterestFromOpen'
Set @DisplayName = 'Interest From Open'
Set @Description = 'InterestFromOpen'
Set @Type = 'String'
Set @Template = '{model.InterestFromOpen.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'InterestLastMonth'
Set @DisplayName = 'Interest Last Month'
Set @Description = 'InterestLastMonth'
Set @Type = 'String'
Set @Template = '{model.InterestLastMonth.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'InterestLastYear'
Set @DisplayName = 'Interest Last Year'
Set @Description = 'InterestLastYear'
Set @Type = 'String'
Set @Template = '{model.InterestLastYear.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'InterestRate'
Set @DisplayName = 'Interest Rate'
Set @Description = 'InterestRate'
Set @Type = 'String'
Set @Template = '{model.InterestRate}%'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'InterestThisYear'
Set @DisplayName = 'Interest This Year'
Set @Description = 'InterestThisYear'
Set @Type = 'String'
Set @Template = '{model.InterestThisYear.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'LastPaymentAmount'
Set @DisplayName = 'Last Payment Amount'
Set @Description = 'LastPaymentAmount'
Set @Type = 'String'
Set @Template = '{model.LastPaymentAmount.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'LastPaymentDate'
Set @DisplayName = 'Last Payment Date'
Set @Description = 'LastPaymentDate'
Set @Type = 'String'
Set @Template = '{model.LastPaymentDate.HasValue ? model.LastPaymentDate.Value.ToString("d") : ""}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'MaturityDate'
Set @DisplayName = 'Maturity Date'
Set @Description = 'MaturityDate'
Set @Type = 'String'
Set @Template = '{model.MaturityDate.HasValue ? model.MaturityDate.Value.ToString("d") : ""}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'LastMaturityDate'
Set @DisplayName = 'Last Maturity Date'
Set @Description = 'LastMaturityDate'
Set @Type = 'String'
Set @Template = '{model.LastMaturityDate.HasValue ? model.LastMaturityDate.Value.ToString("d") : ""}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'DividendAvailable'
Set @DisplayName = 'Available Dividend Balance'
Set @Description = 'DividendAvailable'
Set @Type = 'String'
Set @Template = '{model.DividendAvailable.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'OpenAt'
Set @DisplayName = 'Open At'
Set @Description = 'OpenAt'
Set @Type = 'String'
Set @Template = '{model.OpenAt.HasValue ? model.OpenAt.Value.ToString("d") : ""}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'PaymentAmountDue'
Set @DisplayName = 'Payment Amount'
Set @Description = 'PaymentAmountDue'
Set @Type = 'String'
Set @Template = '{model.PaymentAmountDue.ToString("c")}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'PaymentDueDate'
Set @DisplayName = 'Payment Due Date'
Set @Description = 'PaymentDueDate'
Set @Type = 'String'
Set @Template = '{model.PaymentDueDate.HasValue ? model.PaymentDueDate.Value.ToString("d") : ""}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran

Set @Name = 'Micr'
Set @DisplayName = 'MICR Account Number'
Set @Description = 'Micr'
Set @Type = 'String'
Set @Template = '{model.Micr}'

begin tran
if exists (select * from MetadataDefinitions with (updlock,serializable) where Name = @Name)
begin
   update MetadataDefinitions set DisplayName = @DisplayName, [Description] = @Description, [Type] = @Type, ModelTemplateString = @Template
   where Name = @Name
end
else
begin
   insert into MetadataDefinitions (InsertAt, UpdateAt, Name, DisplayName, [Description], [Type], [ModelTemplateString])
   values (GetDate(), GetDate(), @Name, @DisplayName, @Description, @Type, @Template)
end
commit tran


-- Seed view contexts

Set @Name = 'TransactionHistory'
Set @Description = 'Transaction History - Account Details'
Set @Type = 'SubAccount'
Set @FullType = 'Alogent.Digital.Accounts.SubAccount'

begin tran
if exists (select * from MetadataContexts with (updlock,serializable) where Name = @Name)
begin
   update MetadataContexts set [Description] = @Description, ModelTypeName = @Type, ModelTypeFullName = @FullType
   where Name = @Name
end
else
begin
   insert into MetadataContexts (InsertAt, UpdateAt, Name, [Description], [ModelTypeName], [ModelTypeFullName])
   values (GetDate(), GetDate(), @Name, @Description, @Type, @FullType)
end
commit tran

Set @Name = 'Payments'
Set @Description = 'Payment - Loan Details'
Set @Type = 'SubAccount'
Set @FullType = 'Alogent.Digital.Accounts.SubAccount'

begin tran
if exists (select * from MetadataContexts with (updlock,serializable) where Name = @Name)
begin
   update MetadataContexts set [Description] = @Description, ModelTypeName = @Type, ModelTypeFullName = @FullType
   where Name = @Name
end
else
begin
   insert into MetadataContexts (InsertAt, UpdateAt, Name, [Description], [ModelTypeName], [ModelTypeFullName])
   values (GetDate(), GetDate(), @Name, @Description, @Type, @FullType)
end
commit tran

Set @Name = 'LoanPayoff'
Set @Description = 'Payoff - Loan Details'
Set @Type = 'SubAccount'
Set @FullType = 'Alogent.Digital.Accounts.SubAccount'

begin tran
if exists (select * from MetadataContexts with (updlock,serializable) where Name = @Name)
begin
   update MetadataContexts set [Description] = @Description, ModelTypeName = @Type, ModelTypeFullName = @FullType
   where Name = @Name
end
else
begin
   insert into MetadataContexts (InsertAt, UpdateAt, Name, [Description], [ModelTypeName], [ModelTypeFullName])
   values (GetDate(), GetDate(), @Name, @Description, @Type, @FullType)
end
commit tran

Set @Name = 'CreditAdvance'
Set @Description = 'Credit Line Advance - Loan Details'
Set @Type = 'SubAccount'
Set @FullType = 'Alogent.Digital.Accounts.SubAccount'

begin tran
if exists (select * from MetadataContexts with (updlock,serializable) where Name = @Name)
begin
   update MetadataContexts set [Description] = @Description, ModelTypeName = @Type, ModelTypeFullName = @FullType
   where Name = @Name
end
else
begin
   insert into MetadataContexts (InsertAt, UpdateAt, Name, [Description], [ModelTypeName], [ModelTypeFullName])
   values (GetDate(), GetDate(), @Name, @Description, @Type, @FullType)
end
commit tran

Set @Name = 'LoanSkipPay'
Set @Description = 'Skip Pay - Loan Details'
Set @Type = 'SubAccount'
Set @FullType = 'Alogent.Digital.Accounts.SubAccount'

begin tran
if exists (select * from MetadataContexts with (updlock,serializable) where Name = @Name)
begin
   update MetadataContexts set [Description] = @Description, ModelTypeName = @Type, ModelTypeFullName = @FullType
   where Name = @Name
end
else
begin
   insert into MetadataContexts (InsertAt, UpdateAt, Name, [Description], [ModelTypeName], [ModelTypeFullName])
   values (GetDate(), GetDate(), @Name, @Description, @Type, @FullType)
end
commit tran


--Seed default groups

Declare @GroupName varchar(1000), @MetadataName varchar(1000), @GroupId bigint, @MetadataId bigint, @MetadataContextId bigint

Set @GroupName = 'Checking'
begin tran
if not exists (select * from MetadataDefinitionGroups with (updlock,serializable) where Name = @GroupName)
begin
   insert into MetadataDefinitionGroups (InsertAt, UpdateAt, Name, [Description], Type, FullTypeName)
   values (GetDate(), GetDate(), @GroupName, @GroupName, 'SubAccount', 'Alogent.Digital.Accounts.SubAccount')
end
commit tran

select @GroupId = Id from MetadataDefinitionGroups where Name = @GroupName

		Set @MetadataId = 0
	Set @MetadataName = 'SubType'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'BalanceAvailable'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'Balance'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'OpenAt'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'DividendRate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'Micr'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		

Set @GroupName = 'Savings'
begin tran
if not exists (select * from MetadataDefinitionGroups with (updlock,serializable) where Name = @GroupName)
begin
   insert into MetadataDefinitionGroups (InsertAt, UpdateAt, Name, [Description], Type, FullTypeName)
   values (GetDate(), GetDate(), @GroupName, @GroupName, 'SubAccount', 'Alogent.Digital.Accounts.SubAccount')
end
commit tran

select @GroupId = Id from MetadataDefinitionGroups where Name = @GroupName

		Set @MetadataId = 0
	Set @MetadataName = 'SubType'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'BalanceAvailable'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'Balance'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'OpenAt'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestRate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'MaturityDate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'DividendRate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'Micr'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		

Set @GroupName = 'CreditCard'
begin tran
if not exists (select * from MetadataDefinitionGroups with (updlock,serializable) where Name = @GroupName)
begin
   insert into MetadataDefinitionGroups (InsertAt, UpdateAt, Name, [Description], Type, FullTypeName)
   values (GetDate(), GetDate(), @GroupName, @GroupName, 'SubAccount', 'Alogent.Digital.Accounts.SubAccount')
end
commit tran

select @GroupId = Id from MetadataDefinitionGroups where Name = @GroupName

		Set @MetadataId = 0
	Set @MetadataName = 'SubType'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'OpenAt'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestRate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestFromOpen'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestLastMonth'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestLastYear'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'CreditBalance'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'CreditAvailable'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'CreditLimit'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'PaymentDueDate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'PaymentAmountDue'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		

Set @GroupName = 'Certificate'
begin tran
if not exists (select * from MetadataDefinitionGroups with (updlock,serializable) where Name = @GroupName)
begin
   insert into MetadataDefinitionGroups (InsertAt, UpdateAt, Name, [Description], Type, FullTypeName)
   values (GetDate(), GetDate(), @GroupName, @GroupName, 'SubAccount', 'Alogent.Digital.Accounts.SubAccount')
end
commit tran

select @GroupId = Id from MetadataDefinitionGroups where Name = @GroupName

		Set @MetadataId = 0
	Set @MetadataName = 'SubType'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'Balance'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'OpenAt'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'MaturityDate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'DividendRate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'DividendLastMonth'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'DividendThisYear'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'DividendLastYear'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'DividendFromOpen'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'LastMaturityDate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'DividendAvailable'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		

Set @GroupName = 'AutoLoan'
begin tran
if not exists (select * from MetadataDefinitionGroups with (updlock,serializable) where Name = @GroupName)
begin
   insert into MetadataDefinitionGroups (InsertAt, UpdateAt, Name, [Description], Type, FullTypeName)
   values (GetDate(), GetDate(), @GroupName, @GroupName, 'SubAccount', 'Alogent.Digital.Accounts.SubAccount')
end
commit tran

select @GroupId = Id from MetadataDefinitionGroups where Name = @GroupName

		Set @MetadataId = 0
	Set @MetadataName = 'SubType'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'OpenAt'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestRate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestFromOpen'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestLastMonth'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'InterestLastYear'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'CreditBalance'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'PaymentDueDate'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		Set @MetadataId = 0
	Set @MetadataName = 'PaymentAmountDue'

	select @MetadataId = Id from MetadataDefinitions where Name = @MetadataName
	begin tran

	if (@MetadataId > 0 and @GroupId > 0)
	begin
		if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId)
		begin
			insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, [Order])
			values (GetDate(), GetDate(), @MetadataId, @GroupId, 1)
		end
		
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'TransactionHistory'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'Payments'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanPayoff'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'CreditAdvance'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
				
		select @MetadataContextId = Id from MetadataContexts where Name = 'LoanSkipPay'
		if (@MetadataId > 0 and @GroupId > 0)
		begin
			if not exists (select * from MetadataDefinitionGroupLinks where MetadataDefinitionId = @MetadataId and MetadataDefinitionGroupId = @GroupId and @MetadataContextId = MetadataContextId)
			begin
				insert into MetadataDefinitionGroupLinks (InsertAt, UpdateAt, MetadataDefinitionId, MetadataDefinitionGroupId, MetadataContextId, [Order])
				values (GetDate(), GetDate(), @MetadataId, @GroupId, @MetadataContextId, 1)
			end
		end
			end

	

	commit tran
		


