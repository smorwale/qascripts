			begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'View Accounts')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'View account balances and details', FullyQualifiedName = 'BusinessAccountAccess_View', IsAdmin = 1
	where DisplayName = 'View Accounts'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'View Accounts', 'View account balances and details', 'BusinessAccountAccess_View',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Transaction History')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'View account transaction history details', FullyQualifiedName = 'BusinessAccountAccess_ViewTransactions', IsAdmin = 1
	where DisplayName = 'Transaction History'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Transaction History', 'View account transaction history details', 'BusinessAccountAccess_ViewTransactions',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Transaction Download')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Export transaction history as a file download', FullyQualifiedName = 'BusinessAccountAccess_ExportTransactions', IsAdmin = 1
	where DisplayName = 'Transaction Download'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Transaction Download', 'Export transaction history as a file download', 'BusinessAccountAccess_ExportTransactions',1)
	end
	commit tran
			begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'View Users')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'View the current list of business users', FullyQualifiedName = 'BusinessAdministration_Access', IsAdmin = 1
	where DisplayName = 'View Users'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'View Users', 'View the current list of business users', 'BusinessAdministration_Access',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Create Users')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Create a new user to allow access to business banking', FullyQualifiedName = 'BusinessAdministration_CreateUser', IsAdmin = 1
	where DisplayName = 'Create Users'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Create Users', 'Create a new user to allow access to business banking', 'BusinessAdministration_CreateUser',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Edit User Profile')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Edit sub-user profile details - name, email address, phone number, etc.', FullyQualifiedName = 'BusinessAdministration_EditUser', IsAdmin = 1
	where DisplayName = 'Edit User Profile'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Edit User Profile', 'Edit sub-user profile details - name, email address, phone number, etc.', 'BusinessAdministration_EditUser',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Manage Roles')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Add, edit and delete roles available for use by business users', FullyQualifiedName = 'BusinessAdministration_EditRoles', IsAdmin = 1
	where DisplayName = 'Manage Roles'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Manage Roles', 'Add, edit and delete roles available for use by business users', 'BusinessAdministration_EditRoles',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Assign Roles')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Assign existing users to roles currently available', FullyQualifiedName = 'BusinessAdministration_AssignRoles', IsAdmin = 1
	where DisplayName = 'Assign Roles'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Assign Roles', 'Assign existing users to roles currently available', 'BusinessAdministration_AssignRoles',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Assign Account Access')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Assign existing users to allow or block access to accounts for their assigned roles', FullyQualifiedName = 'BusinessAdministration_AssignAccountAccess', IsAdmin = 1
	where DisplayName = 'Assign Account Access'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Assign Account Access', 'Assign existing users to allow or block access to accounts for their assigned roles', 'BusinessAdministration_AssignAccountAccess',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Access Restrictions')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Set access restrictions for existing users', FullyQualifiedName = 'BusinessAdministration_AccessRestrictions', IsAdmin = 1
	where DisplayName = 'Access Restrictions'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Access Restrictions', 'Set access restrictions for existing users', 'BusinessAdministration_AccessRestrictions',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Edit Limits')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Set financial limits and thresholds per user before actions are blocked or require additional confirmation.', FullyQualifiedName = 'BusinessAdministration_EditLimits', IsAdmin = 1
	where DisplayName = 'Edit Limits'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Edit Limits', 'Set financial limits and thresholds per user before actions are blocked or require additional confirmation.', 'BusinessAdministration_EditLimits',1)
	end
	commit tran
			begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Create Transfer')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Initiate transfers between business accounts held within the financial institution.', FullyQualifiedName = 'BusinessTransfers_Create', IsAdmin = 1
	where DisplayName = 'Create Transfer'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Create Transfer', 'Initiate transfers between business accounts held within the financial institution.', 'BusinessTransfers_Create',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Edit Transfer')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Edit scheduled transfers between business accounts held within the financial institution.', FullyQualifiedName = 'BusinessTransfers_Edit', IsAdmin = 1
	where DisplayName = 'Edit Transfer'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Edit Transfer', 'Edit scheduled transfers between business accounts held within the financial institution.', 'BusinessTransfers_Edit',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Delete Transfer')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Cancel currently scheduled transfers between business accounts held within the financial institution.', FullyQualifiedName = 'BusinessTransfers_Delete', IsAdmin = 1
	where DisplayName = 'Delete Transfer'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Delete Transfer', 'Cancel currently scheduled transfers between business accounts held within the financial institution.', 'BusinessTransfers_Delete',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Create External Accounts')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Create a new record of account information held at other institutions that can be used for transfers and payments.', FullyQualifiedName = 'BusinessTransfers_CreateExternalAccount', IsAdmin = 1
	where DisplayName = 'Create External Accounts'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Create External Accounts', 'Create a new record of account information held at other institutions that can be used for transfers and payments.', 'BusinessTransfers_CreateExternalAccount',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Edit External Accounts')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Manage account information for accounts held at other institutions that can be used for transfers and payments.', FullyQualifiedName = 'BusinessTransfers_EditExternalAccount', IsAdmin = 1
	where DisplayName = 'Edit External Accounts'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Edit External Accounts', 'Manage account information for accounts held at other institutions that can be used for transfers and payments.', 'BusinessTransfers_EditExternalAccount',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Delete External Accounts')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Delete account information for accounts held at other institutions that can be used for transfers and payments.', FullyQualifiedName = 'BusinessTransfers_DeleteExternalAccount', IsAdmin = 1
	where DisplayName = 'Delete External Accounts'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Delete External Accounts', 'Delete account information for accounts held at other institutions that can be used for transfers and payments.', 'BusinessTransfers_DeleteExternalAccount',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Create Payment')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Initiate payments to business loan or credit card accounts.', FullyQualifiedName = 'BusinessPayments_Create', IsAdmin = 1
	where DisplayName = 'Create Payment'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Create Payment', 'Initiate payments to business loan or credit card accounts.', 'BusinessPayments_Create',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Edit Payment')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Edit scheduled payments to business loan or credit card accounts to modify amounts, dates, etc.', FullyQualifiedName = 'BusinessPayments_Edit', IsAdmin = 1
	where DisplayName = 'Edit Payment'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Edit Payment', 'Edit scheduled payments to business loan or credit card accounts to modify amounts, dates, etc.', 'BusinessPayments_Edit',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Delete Payment')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Cancel currently scheduled payments to business loan or credit card accounts.', FullyQualifiedName = 'BusinessPayments_Delete', IsAdmin = 1
	where DisplayName = 'Delete Payment'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Delete Payment', 'Cancel currently scheduled payments to business loan or credit card accounts.', 'BusinessPayments_Delete',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Bill Pay Access')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Access payments for paying via the institution''s bill payment system.', FullyQualifiedName = 'BusinessPayments_BillPay', IsAdmin = 1
	where DisplayName = 'Bill Pay Access'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Bill Pay Access', 'Access payments for paying via the institution''s bill payment system.', 'BusinessPayments_BillPay',1)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Access Budgeting')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Access budgeting in business banking.', FullyQualifiedName = 'BusinessBudgeting_Access', IsAdmin = 0
	where DisplayName = 'Access Budgeting'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Access Budgeting', 'Access budgeting in business banking.', 'BusinessBudgeting_Access', 0)
	end
	commit tran
	begin tran
	if exists (select * from BusinessBankingPermissions with (updlock,serializable) where DisplayName = 'Manage Budgets')
	begin
	update BusinessBankingPermissions set UpdateAt = GetDate(), Description = 'Manage budgeting in business banking.', FullyQualifiedName = 'BusinessBudgeting_ManageBudgets', IsAdmin = 0
	where DisplayName = 'Manage Budgets'
	end
	else
	begin
	insert into BusinessBankingPermissions (InsertAt, UpdateAt, DisplayName, Description, FullyQualifiedName, IsAdmin)
	values (GetDate(), GetDate(),'Manage Budgets', 'Manage budgeting in business banking.', 'BusinessBudgeting_ManageBudgets', 0)
	end
	commit tran