/*
    Insert some Categories for Personal Analytics
*/
IF NOT EXISTS (SELECT * FROM Categories WITH (UPDLOCK,SERIALIZABLE))
BEGIN
    INSERT INTO Categories(Name, DisplayName) VALUES ('Rent','Rent')
    INSERT INTO Categories(Name, DisplayName) VALUES ('Property taxes','Property taxes')
    INSERT INTO Categories(Name, DisplayName) VALUES ('Household repairs','Household repairs')
    INSERT INTO Categories(Name, DisplayName) VALUES ('HOA fees','HOA fees')
    INSERT INTO Categories(Name, DisplayName) VALUES ('Car payment','Car payment')
    INSERT INTO Categories(Name, DisplayName) VALUES ('Car warranty','Car warranty')
    INSERT INTO Categories(Name, DisplayName) VALUES ('Groceries','Groceries')
    INSERT INTO Categories(Name, DisplayName) VALUES ('Utilities','Utilities')
    INSERT INTO Categories(Name, DisplayName) VALUES ('Medications','Medications')
    INSERT INTO Categories(Name, DisplayName) VALUES ('Gym memberships','Gym memberships')
END

