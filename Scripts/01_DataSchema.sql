IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [ActivityLogLabels] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ModuleName] nvarchar(max) NULL,
        [ActionName] nvarchar(max) NULL,
        [ModuleDisplayName] nvarchar(max) NULL,
        [ActionDisplayName] nvarchar(max) NULL,
        CONSTRAINT [PK_ActivityLogLabels] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Announcements] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [StartDate] datetime2 NULL,
        [EndDate] datetime2 NULL,
        [Name] nvarchar(max) NULL,
        [Body] nvarchar(max) NULL,
        [CreatedBy] nvarchar(max) NULL,
        [CreatedAt] datetime2 NOT NULL,
        [Published] bit NOT NULL,
        [PublishedBy] nvarchar(max) NULL,
        [PublishedAt] datetime2 NULL,
        [DeactivatedBy] nvarchar(max) NULL,
        [DeactivatedAt] datetime2 NULL,
        [LastEditedBy] nvarchar(max) NULL,
        [LastEditedAt] datetime2 NULL,
        [DeletedBy] nvarchar(max) NULL,
        [Dismissable] bit NOT NULL,
        [DeleteAt] datetime2 NULL,
        CONSTRAINT [PK_Announcements] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Assets] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [CreatedBy] nvarchar(max) NULL,
        [UpdatedBy] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [MimeType] nvarchar(max) NULL,
        [File] varbinary(max) NULL,
        CONSTRAINT [PK_Assets] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Businesses] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        CONSTRAINT [PK_Businesses] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Configurations] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [PropertyNames] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        [Category] nvarchar(max) NULL,
        [DisplayName] nvarchar(max) NULL,
        [FullTypeName] nvarchar(450) NULL,
        [AssemblyQualifiedTypeName] nvarchar(max) NULL,
        [InstanceName] nvarchar(450) NULL,
        [IsEncrypted] bit NOT NULL,
        [Data] nvarchar(max) NULL,
        CONSTRAINT [PK_Configurations] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Countries] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [CountryCode] nvarchar(max) NULL,
        [Alpha3Code] nvarchar(max) NULL,
        [NumericCode] nvarchar(max) NULL,
        [CurrencyCode] nvarchar(max) NULL,
        CONSTRAINT [PK_Countries] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DeviceSettings] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Key] nvarchar(450) NULL,
        [Enabled] bit NOT NULL,
        [DeviceType] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_DeviceSettings] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DigitalAdmins] (
        [Id] nvarchar(450) NOT NULL,
        [InsertAt] datetime2 NOT NULL,
        [UpdateAt] datetime2 NOT NULL,
        [UserName] nvarchar(256) NULL,
        [NormalizedUserName] nvarchar(256) NULL,
        [PasswordHash] nvarchar(max) NULL,
        [PasswordHistory] nvarchar(max) NULL,
        [PasswordChangedUtc] datetime2 NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [AccessLocked] bit NOT NULL DEFAULT CAST(0 AS bit),
        [AccessFailedCount] int NOT NULL DEFAULT 0,
        [AccessFailedCountTotal] int NOT NULL DEFAULT 0,
        [AccessSucceededCount] int NOT NULL DEFAULT 0,
        [AccessSucceededCountTotal] int NOT NULL DEFAULT 0,
        [RegistrationSucceededUtc] datetime2 NULL,
        [LastAccessSucceededUtc] datetime2 NULL,
        [LastAccessFailedUtc] datetime2 NULL,
        [LastAccessLockedUtc] datetime2 NULL,
        [FirstName] nvarchar(max) NULL,
        [LastName] nvarchar(max) NULL,
        [Department] nvarchar(max) NULL,
        [Branch] nvarchar(max) NULL,
        [Email] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_DigitalAdmins] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DigitalRoles] (
        [Id] nvarchar(450) NOT NULL,
        [InsertAt] datetime2 NOT NULL,
        [UpdateAt] datetime2 NOT NULL,
        [Name] nvarchar(max) NULL,
        [NormalizedName] nvarchar(450) NULL,
        [Description] nvarchar(max) NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [IsAdmin] bit NOT NULL,
        [CreatedBy] nvarchar(max) NULL,
        [CreatedAt] datetime2 NOT NULL DEFAULT (GetUtcDate()),
        [LastEditedBy] nvarchar(max) NULL,
        [LastEditedAt] datetime2 NULL,
        [IsDefault] bit NOT NULL,
        CONSTRAINT [PK_DigitalRoles] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Features] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Enabled] bit NOT NULL,
        [Name] nvarchar(max) NULL,
        [DisplayName] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        [BaseApiRoute] nvarchar(max) NULL,
        [BaseUiRoute] nvarchar(max) NULL,
        [IsAdmin] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_Features] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [FinancialInstitutions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [RoutingNumber] nvarchar(450) NULL,
        [Address] nvarchar(max) NULL,
        [NewRoutingNumber] nvarchar(max) NULL,
        CONSTRAINT [PK_FinancialInstitutions] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Jobs] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [InstanceGuid] uniqueidentifier NOT NULL,
        [Name] nvarchar(max) NULL,
        [Schedule] nvarchar(max) NULL,
        [Active] bit NOT NULL,
        [Running] bit NOT NULL,
        [LastRun] datetime2 NOT NULL,
        [LastRunServer] nvarchar(max) NULL,
        CONSTRAINT [PK_Jobs] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Logs] (
        [Id] bigint NOT NULL IDENTITY,
        [TimeStamp] datetimeoffset NOT NULL,
        [Message] nvarchar(max) NULL,
        [MessageTemplate] nvarchar(max) NULL,
        [Level] nvarchar(max) NULL,
        [Exception] nvarchar(max) NULL,
        [LogEvent] nvarchar(max) NULL,
        [SessionCorrelationId] nvarchar(max) NULL,
        CONSTRAINT [PK_Logs] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [MetadataDefinitionGroups] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        CONSTRAINT [PK_MetadataDefinitionGroups] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [MetadataDefinitions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [DisplayName] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        [Target] nvarchar(max) NULL,
        [Type] nvarchar(max) NOT NULL,
        [ModelTemplateString] nvarchar(max) NULL,
        CONSTRAINT [PK_MetadataDefinitions] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Portfolios] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Identifier] nvarchar(max) NULL,
        [OwnerUserId] nvarchar(max) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        CONSTRAINT [PK_Portfolios] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [SecureMessages] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [UserId] nvarchar(max) NULL,
        [Sent] bit NOT NULL,
        [Read] bit NOT NULL,
        [Delivered] bit NOT NULL,
        [Category] nvarchar(max) NULL,
        [SubCategory] nvarchar(max) NULL,
        [AccountId] bigint NULL,
        [Subject] nvarchar(max) NULL,
        [Body] nvarchar(max) NULL,
        [MessageId] uniqueidentifier NOT NULL,
        [ThreadId] uniqueidentifier NOT NULL,
        [EmailId] nvarchar(max) NULL,
        CONSTRAINT [PK_SecureMessages] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [SecureMessageTemplates] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [TemplateId] nvarchar(max) NULL,
        [Category] nvarchar(max) NULL,
        [Subject] nvarchar(max) NULL,
        [Body] nvarchar(max) NULL,
        [AllowedCategories] nvarchar(max) NULL,
        [ReadOnlyBody] nvarchar(max) NULL,
        CONSTRAINT [PK_SecureMessageTemplates] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Sessions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [SessionCorrelationId] nvarchar(max) NULL,
        [UserId] nvarchar(max) NULL,
        [MfaRequired] bit NOT NULL,
        [MfaType] nvarchar(max) NULL,
        [MfaPasscode] nvarchar(max) NULL,
        [TimeoutMinutes] int NOT NULL,
        [LastValidatedAt] datetime2 NOT NULL,
        [SignoutAt] datetime2 NULL,
        [Type] nvarchar(max) NULL,
        [IsAdmin] bit NOT NULL,
        [IpAddress] nvarchar(max) NULL,
        [OtpStatus] nvarchar(max) NULL,
        [Entitlements] nvarchar(max) NULL,
        [IsSupportSession] bit NOT NULL,
        [AdminUserId] nvarchar(max) NULL,
        CONSTRAINT [PK_Sessions] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Sources] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        CONSTRAINT [PK_Sources] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [UserVendorData] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [IntegrationKey] nvarchar(max) NULL,
        [UserId] nvarchar(450) NULL,
        [PortfolioId] bigint NOT NULL,
        [CoreKey] nvarchar(450) NULL,
        [Data] nvarchar(max) NULL,
        CONSTRAINT [PK_UserVendorData] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [VendorIntegrations] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Type] nvarchar(max) NULL,
        [Key] nvarchar(max) NULL,
        [Vendor] nvarchar(max) NULL,
        [IsActive] bit NOT NULL,
        [Url] nvarchar(max) NULL,
        [PageTitle] nvarchar(max) NULL,
        [ConfigurationType] nvarchar(max) NULL,
        [MaintenanceMessage] nvarchar(max) NULL,
        [Configuration] nvarchar(max) NULL,
        CONSTRAINT [PK_VendorIntegrations] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Devices] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [UserId] nvarchar(max) NULL,
        [Key] nvarchar(max) NULL,
        [Uuid] nvarchar(max) NULL,
        [Type] nvarchar(max) NULL,
        [Language] nvarchar(max) NULL,
        [Manufacturer] nvarchar(max) NULL,
        [Model] nvarchar(max) NULL,
        [Os] nvarchar(max) NULL,
        [OsVersion] nvarchar(max) NULL,
        [Region] nvarchar(max) NULL,
        [SdkVersion] nvarchar(max) NULL,
        [Browser] nvarchar(max) NULL,
        [UserAgent] nvarchar(max) NULL,
        [BiometricAuthenticationDeviceSettingId] bigint NULL,
        [TrustedDeviceSettingId] bigint NULL,
        CONSTRAINT [PK_Devices] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Devices_DeviceSettings_BiometricAuthenticationDeviceSettingId] FOREIGN KEY ([BiometricAuthenticationDeviceSettingId]) REFERENCES [DeviceSettings] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Devices_DeviceSettings_TrustedDeviceSettingId] FOREIGN KEY ([TrustedDeviceSettingId]) REFERENCES [DeviceSettings] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [AdminActivityLogs] (
        [Id] bigint NOT NULL IDENTITY,
        [TimeStamp] datetimeoffset NOT NULL,
        [Message] nvarchar(max) NULL,
        [MessageTemplate] nvarchar(max) NULL,
        [Level] nvarchar(max) NULL,
        [Exception] nvarchar(max) NULL,
        [LogEvent] nvarchar(max) NULL,
        [SessionCorrelationId] nvarchar(max) NULL,
        [Module] nvarchar(max) NULL,
        [Action] nvarchar(max) NULL,
        [Subaction] nvarchar(max) NULL,
        [Successful] bit NOT NULL,
        [AdminId] nvarchar(450) NULL,
        CONSTRAINT [PK_AdminActivityLogs] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AdminActivityLogs_DigitalAdmins_AdminId] FOREIGN KEY ([AdminId]) REFERENCES [DigitalAdmins] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DigitalAdminRoles] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [DigitalAdminId] nvarchar(450) NOT NULL,
        [DigitalAdminId1] nvarchar(450) NULL,
        [DigitalRoleId] nvarchar(450) NULL,
        [AssignedByName] nvarchar(max) NULL,
        [AssignedBy] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_DigitalAdminRoles] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_DigitalAdminRoles_DigitalAdmins_DigitalAdminId] FOREIGN KEY ([DigitalAdminId]) REFERENCES [DigitalAdmins] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_DigitalAdminRoles_DigitalAdmins_DigitalAdminId1] FOREIGN KEY ([DigitalAdminId1]) REFERENCES [DigitalAdmins] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_DigitalAdminRoles_DigitalRoles_DigitalRoleId] FOREIGN KEY ([DigitalRoleId]) REFERENCES [DigitalRoles] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [FeaturePermissions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [FeatureId] bigint NOT NULL,
        [Name] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        [Tag] nvarchar(max) NULL,
        [FullyQualifiedName] nvarchar(450) NULL,
        [IsAdmin] bit NOT NULL DEFAULT CAST(0 AS bit),
        CONSTRAINT [PK_FeaturePermissions] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_FeaturePermissions_Features_FeatureId] FOREIGN KEY ([FeatureId]) REFERENCES [Features] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [JobProperties] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [JobId] bigint NOT NULL,
        [ConfigurationId] uniqueidentifier NOT NULL,
        [Name] nvarchar(max) NULL,
        [IsEncrypted] bit NOT NULL,
        [Value] nvarchar(max) NULL,
        CONSTRAINT [PK_JobProperties] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_JobProperties_Jobs_JobId] FOREIGN KEY ([JobId]) REFERENCES [Jobs] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [FeatureProductContexts] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [MetadataDefinitionGroupId] bigint NULL,
        [FeatureId] bigint NOT NULL,
        [Name] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        [Tag] nvarchar(max) NULL,
        [FullyQualifiedName] nvarchar(450) NULL,
        CONSTRAINT [PK_FeatureProductContexts] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_FeatureProductContexts_Features_FeatureId] FOREIGN KEY ([FeatureId]) REFERENCES [Features] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_FeatureProductContexts_MetadataDefinitionGroups_MetadataDefinitionGroupId] FOREIGN KEY ([MetadataDefinitionGroupId]) REFERENCES [MetadataDefinitionGroups] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [ProductDefinitions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [AvailableMetadataDefinitionsId] bigint NULL,
        [Type] nvarchar(max) NOT NULL,
        [SubType] nvarchar(max) NOT NULL,
        [DefaultName] nvarchar(max) NULL,
        [DefaultTypeName] nvarchar(max) NULL,
        [ExternalIdentifiers] nvarchar(max) NULL,
        [ClosedDaysBeforeDelete] int NOT NULL,
        CONSTRAINT [PK_ProductDefinitions] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ProductDefinitions_MetadataDefinitionGroups_AvailableMetadataDefinitionsId] FOREIGN KEY ([AvailableMetadataDefinitionsId]) REFERENCES [MetadataDefinitionGroups] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [MetadataDefinitionGroupLinks] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [MetadataDefinitionId] bigint NOT NULL,
        [MetadataDefinitionGroupId] bigint NOT NULL,
        [Order] int NOT NULL,
        CONSTRAINT [PK_MetadataDefinitionGroupLinks] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_MetadataDefinitionGroupLinks_MetadataDefinitionGroups_MetadataDefinitionGroupId] FOREIGN KEY ([MetadataDefinitionGroupId]) REFERENCES [MetadataDefinitionGroups] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_MetadataDefinitionGroupLinks_MetadataDefinitions_MetadataDefinitionId] FOREIGN KEY ([MetadataDefinitionId]) REFERENCES [MetadataDefinitions] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Addresses] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [PortfolioId] bigint NOT NULL,
        [Type] nvarchar(max) NULL,
        [Primary] bit NOT NULL,
        [AddressLine1] nvarchar(max) NULL,
        [AddressLine2] nvarchar(max) NULL,
        [AddressLine3] nvarchar(max) NULL,
        [AddressLine4] nvarchar(max) NULL,
        [City] nvarchar(max) NULL,
        [State] nvarchar(max) NULL,
        [StateCode] nvarchar(max) NULL,
        [Country] nvarchar(max) NULL,
        [CountryCode] nvarchar(max) NULL,
        [PostalCode] nvarchar(max) NULL,
        [Verified] bit NOT NULL,
        [VerifiedDate] datetime2 NULL,
        [IsInternational] bit NOT NULL,
        CONSTRAINT [PK_Addresses] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Addresses_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DigitalUsers] (
        [Id] nvarchar(450) NOT NULL,
        [InsertAt] datetime2 NOT NULL,
        [UpdateAt] datetime2 NOT NULL,
        [InactiveDateUtc] datetime2 NULL,
        [UserName] nvarchar(256) NULL,
        [Type] int NOT NULL,
        [NormalizedUserName] nvarchar(256) NULL,
        [PasswordHash] nvarchar(max) NULL,
        [PasswordHistory] nvarchar(max) NULL,
        [PasswordChangedUtc] datetime2 NULL,
        [PasswordExpiresUtc] datetime2 NULL,
        [ConcurrencyStamp] nvarchar(max) NULL,
        [AccessLocked] bit NOT NULL DEFAULT CAST(0 AS bit),
        [AccessLockedByAdmin] bit NOT NULL DEFAULT CAST(0 AS bit),
        [DisableReason] nvarchar(200) NULL,
        [AllowAccessSelfUnlock] bit NOT NULL DEFAULT CAST(1 AS bit),
        [AccessSucceededCount] int NOT NULL DEFAULT 0,
        [AccessSucceededCountTotal] int NOT NULL DEFAULT 0,
        [AccessFailedCount] int NOT NULL DEFAULT 0,
        [AccessFailedCountTotal] int NOT NULL DEFAULT 0,
        [RegistrationSucceededUtc] datetime2 NULL,
        [LastAccessSucceededUtc] datetime2 NULL,
        [LastAccessFailedUtc] datetime2 NULL,
        [LastAccessLockedUtc] datetime2 NULL,
        [PortfolioId] bigint NOT NULL,
        [BusinessId] bigint NULL,
        [DigitalUserPreferenceId] bigint NULL,
        [Status] int NOT NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [FullName] nvarchar(max) NULL,
        [MemberNumber] nvarchar(max) NULL,
        [TaxId] nvarchar(max) NULL,
        [PasscodeFailedCount] int NOT NULL,
        [PasscodeFailedCountTotal] int NOT NULL,
        CONSTRAINT [PK_DigitalUsers] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_DigitalUsers_Businesses_BusinessId] FOREIGN KEY ([BusinessId]) REFERENCES [Businesses] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_DigitalUsers_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [EmailAddresses] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [PortfolioId] bigint NOT NULL,
        [Type] nvarchar(max) NULL,
        [Primary] bit NOT NULL,
        [Email] nvarchar(max) NULL,
        [Verified] bit NOT NULL,
        [VerifiedDate] datetime2 NULL,
        CONSTRAINT [PK_EmailAddresses] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_EmailAddresses_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [ExternalAccounts] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [PortfolioId] bigint NOT NULL,
        [AccountNumber] nvarchar(max) NULL,
        [RoutingNumber] nvarchar(max) NULL,
        [Nickname] nvarchar(max) NULL,
        [Status] int NOT NULL,
        [AccountType] int NOT NULL,
        [MicroAmount1] decimal(18,2) NOT NULL,
        [MicroAmount2] decimal(18,2) NOT NULL,
        [MicroGeneratedAt] datetime2 NULL,
        [MicroSentAt] datetime2 NULL,
        [MicroVerifiedAt] datetime2 NULL,
        [IsDeleted] bit NOT NULL,
        CONSTRAINT [PK_ExternalAccounts] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ExternalAccounts_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Persons] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [PortfolioId] bigint NOT NULL,
        [Type] nvarchar(max) NOT NULL,
        [FullName] nvarchar(max) NULL,
        [FirstName] nvarchar(max) NULL,
        [MiddleName] nvarchar(max) NULL,
        [LastName] nvarchar(max) NULL,
        [NickName] nvarchar(max) NULL,
        [NamePrefix] nvarchar(max) NULL,
        [NameSuffix] nvarchar(max) NULL,
        [NameTitle] nvarchar(max) NULL,
        [TaxId] nvarchar(max) NULL,
        [BirthDate] datetime2 NULL,
        [DeathDate] datetime2 NULL,
        CONSTRAINT [PK_Persons] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Persons_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [PhoneNumbers] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [PortfolioId] bigint NOT NULL,
        [Primary] bit NOT NULL,
        [Type] nvarchar(max) NULL,
        [Provider] nvarchar(max) NULL,
        [Number] nvarchar(max) NULL,
        [Extension] nvarchar(max) NULL,
        [CountryCode] nvarchar(max) NULL,
        [SMSEmailAddress] nvarchar(max) NULL,
        [Verified] bit NOT NULL,
        [VerifiedDate] datetime2 NULL,
        CONSTRAINT [PK_PhoneNumbers] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_PhoneNumbers_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [SyncStatuses] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [PortfolioId] bigint NULL,
        [EventId] uniqueidentifier NOT NULL,
        [Done] bit NOT NULL,
        CONSTRAINT [PK_SyncStatuses] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_SyncStatuses_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [TransferRecipients] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [PortfolioId] bigint NULL,
        [AccountNumber] nvarchar(max) NULL,
        [ShareId] nvarchar(max) NULL,
        [ShareType] nvarchar(max) NULL,
        [Nickname] nvarchar(max) NULL,
        [LastName] nvarchar(max) NULL,
        CONSTRAINT [PK_TransferRecipients] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_TransferRecipients_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [SecureMessageAttachments] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [SecureMessageId] bigint NOT NULL,
        [FileName] nvarchar(max) NULL,
        [MimeType] nvarchar(max) NULL,
        [File] varbinary(max) NULL,
        CONSTRAINT [PK_SecureMessageAttachments] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_SecureMessageAttachments_SecureMessages_SecureMessageId] FOREIGN KEY ([SecureMessageId]) REFERENCES [SecureMessages] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Accounts] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [SourceId] bigint NOT NULL,
        [PortfolioId] bigint NOT NULL,
        [UpdateSubAccountsAt] datetime2 NULL,
        [OpenAt] datetime2 NULL,
        [CloseAt] datetime2 NULL,
        [Number] nvarchar(max) NULL,
        [Type] nvarchar(max) NULL,
        CONSTRAINT [PK_Accounts] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Accounts_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Accounts_Sources_SourceId] FOREIGN KEY ([SourceId]) REFERENCES [Sources] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [BillPayProfile] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [VendorDataId] bigint NOT NULL,
        [Name] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [EmailAddress] nvarchar(max) NULL,
        [AddressLine1] nvarchar(max) NULL,
        [AddressLine2] nvarchar(max) NULL,
        [City] nvarchar(max) NULL,
        [State] nvarchar(max) NULL,
        [PostalCode] nvarchar(max) NULL,
        CONSTRAINT [PK_BillPayProfile] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BillPayProfile_UserVendorData_VendorDataId] FOREIGN KEY ([VendorDataId]) REFERENCES [UserVendorData] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DigitalRolePermissions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [DigitalRoleId] nvarchar(450) NULL,
        [FeaturePermissionId] bigint NOT NULL,
        [Allowed] bit NOT NULL,
        CONSTRAINT [PK_DigitalRolePermissions] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_DigitalRolePermissions_DigitalRoles_DigitalRoleId] FOREIGN KEY ([DigitalRoleId]) REFERENCES [DigitalRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_DigitalRolePermissions_FeaturePermissions_FeaturePermissionId] FOREIGN KEY ([FeaturePermissionId]) REFERENCES [FeaturePermissions] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [ProductFeatureContextLinks] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [FeatureProductContextId] bigint NOT NULL,
        [ProductDefinitionId] bigint NOT NULL,
        [Allowed] bit NOT NULL,
        CONSTRAINT [PK_ProductFeatureContextLinks] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ProductFeatureContextLinks_FeatureProductContexts_FeatureProductContextId] FOREIGN KEY ([FeatureProductContextId]) REFERENCES [FeatureProductContexts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_ProductFeatureContextLinks_ProductDefinitions_ProductDefinitionId] FOREIGN KEY ([ProductDefinitionId]) REFERENCES [ProductDefinitions] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [ActivityLogs] (
        [Id] bigint NOT NULL IDENTITY,
        [TimeStamp] datetimeoffset NOT NULL,
        [Message] nvarchar(max) NULL,
        [MessageTemplate] nvarchar(max) NULL,
        [Level] nvarchar(max) NULL,
        [Exception] nvarchar(max) NULL,
        [LogEvent] nvarchar(max) NULL,
        [SessionCorrelationId] nvarchar(max) NULL,
        [Module] nvarchar(max) NULL,
        [Action] nvarchar(max) NULL,
        [Subaction] nvarchar(max) NULL,
        [Successful] bit NOT NULL,
        [UserId] nvarchar(450) NULL,
        CONSTRAINT [PK_ActivityLogs] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ActivityLogs_DigitalUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DigitalUserAnnouncements] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [AnnouncementId] bigint NOT NULL,
        [DigitalUserId] nvarchar(450) NULL,
        [DateReceived] datetime2 NOT NULL,
        [DateReadOrViewed] datetime2 NULL,
        [DeleteAt] datetime2 NULL,
        [DismissAt] datetime2 NULL,
        CONSTRAINT [PK_DigitalUserAnnouncements] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_DigitalUserAnnouncements_Announcements_AnnouncementId] FOREIGN KEY ([AnnouncementId]) REFERENCES [Announcements] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_DigitalUserAnnouncements_DigitalUsers_DigitalUserId] FOREIGN KEY ([DigitalUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DigitalUserPreferences] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [DigitalUserId] nvarchar(450) NULL,
        [SessionTimeoutMinutes] bigint NULL,
        [Nickname] nvarchar(max) NULL,
        CONSTRAINT [PK_DigitalUserPreferences] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_DigitalUserPreferences_DigitalUsers_DigitalUserId] FOREIGN KEY ([DigitalUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DigitalUserRoles] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [DigitalUserId] nvarchar(450) NOT NULL,
        [DigitalUserId1] nvarchar(450) NULL,
        [DigitalRoleId] nvarchar(450) NULL,
        [AssignedByName] nvarchar(max) NULL,
        [AssignedBy] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_DigitalUserRoles] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_DigitalUserRoles_DigitalRoles_DigitalRoleId] FOREIGN KEY ([DigitalRoleId]) REFERENCES [DigitalRoles] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_DigitalUserRoles_DigitalUsers_DigitalUserId] FOREIGN KEY ([DigitalUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_DigitalUserRoles_DigitalUsers_DigitalUserId1] FOREIGN KEY ([DigitalUserId1]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [AccountDocuments] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [AccountId] bigint NOT NULL,
        [ResourceId] uniqueidentifier NOT NULL DEFAULT (NEWSEQUENTIALID()),
        [Name] nvarchar(max) NULL,
        [Type] int NOT NULL,
        [SubType] nvarchar(max) NULL,
        [MimeType] nvarchar(max) NULL,
        [DocumentDate] datetime2 NOT NULL,
        [ViewDocumentCount] bigint NOT NULL DEFAULT CAST(0 AS bigint),
        [File] varbinary(max) NULL,
        CONSTRAINT [PK_AccountDocuments] PRIMARY KEY ([Id]),
        CONSTRAINT [AK_AccountDocuments_ResourceId] UNIQUE ([ResourceId]),
        CONSTRAINT [FK_AccountDocuments_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Cards] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [AccountId] bigint NOT NULL,
        [ProductDefinitionId] bigint NULL,
        [ProductEntitlements] nvarchar(max) NULL,
        [ActiveDate] datetime2 NULL,
        [ExpirationDate] datetime2 NULL,
        [Name] nvarchar(max) NULL,
        [Type] nvarchar(max) NULL,
        [Status] nvarchar(max) NULL,
        [RewardPoints] int NOT NULL,
        [HasRewards] bit NOT NULL,
        [Number] nvarchar(max) NULL,
        CONSTRAINT [PK_Cards] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Cards_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Cards_ProductDefinitions_ProductDefinitionId] FOREIGN KEY ([ProductDefinitionId]) REFERENCES [ProductDefinitions] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Deposits] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [AccountId] bigint NOT NULL,
        [UserId] nvarchar(max) NULL,
        [SubAccountId] nvarchar(max) NULL,
        [DeviceId] nvarchar(max) NULL,
        [Amount] decimal(18,2) NOT NULL,
        [DepositDate] datetime2 NOT NULL,
        [DepositEffectiveDate] datetime2 NOT NULL,
        [Latitude] decimal(18,2) NOT NULL,
        [Longitude] decimal(18,2) NOT NULL,
        [VendorId] nvarchar(max) NULL,
        [VendorStatus] nvarchar(max) NULL,
        [VendorStatusReason] nvarchar(max) NULL,
        [VendorRawRequest] nvarchar(max) NULL,
        [VendorRawResponse] nvarchar(max) NULL,
        [CoreId] nvarchar(max) NULL,
        [CoreStatus] nvarchar(max) NULL,
        [CoreStatusReason] nvarchar(max) NULL,
        [CoreRawRequest] nvarchar(max) NULL,
        [CoreRawResponse] nvarchar(max) NULL,
        CONSTRAINT [PK_Deposits] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Deposits_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Payments] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [AccountId] bigint NULL,
        [PaymentType] int NOT NULL,
        [Source] nvarchar(max) NULL,
        [Note] nvarchar(max) NULL,
        [AccountNumberFrom] nvarchar(max) NULL,
        [RoutingNumberFrom] nvarchar(max) NULL,
        [AccountNumberTo] nvarchar(max) NULL,
        [RoutingNumberTo] nvarchar(max) NULL,
        [SubAccountCoreKeyFrom] nvarchar(max) NULL,
        [SubAccountCoreKeyTo] nvarchar(max) NULL,
        [Amount] decimal(18,2) NOT NULL,
        [StartAt] datetime2 NULL,
        [EndAt] datetime2 NULL,
        [FrequencyName] nvarchar(max) NULL,
        [FrequencyCode] nvarchar(max) NULL,
        [PrincipalOnly] bit NOT NULL,
        [OriginalPaymentDay] int NOT NULL,
        [CoreRequest] nvarchar(max) NULL,
        [CoreResponse] nvarchar(max) NULL,
        [Successful] bit NOT NULL,
        [CreatedByUserId] nvarchar(max) NULL,
        [ConfirmationNumber] nvarchar(max) NULL,
        [AmountCode] nvarchar(max) NULL,
        [LastPaymentDate] datetime2 NULL,
        [NextPaymentDate] datetime2 NULL,
        [ScheduleDay1] int NULL,
        [ScheduleDay2] int NULL,
        [CanEdit] bit NULL,
        [CanDelete] bit NULL,
        [IsDeleted] bit NULL,
        CONSTRAINT [PK_Payments] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Payments_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [SubAccounts] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [AccountId] bigint NOT NULL,
        [ProductDefinitionId] bigint NULL,
        [UpdateTransactionsAt] datetime2 NULL,
        [ProductEntitlements] nvarchar(max) NULL,
        [Number] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [Nickname] nvarchar(max) NULL,
        [Type] nvarchar(max) NULL,
        [SubType] nvarchar(max) NULL,
        [TypeData] nvarchar(max) NULL,
        [Classification] nvarchar(max) NULL,
        [Note] nvarchar(max) NULL,
        [Color] nvarchar(max) NULL,
        [Balance] decimal(18,2) NOT NULL,
        [BalanceAvailable] decimal(18,2) NOT NULL,
        [CreditBalance] decimal(18,2) NOT NULL,
        [CreditAvailable] decimal(18,2) NOT NULL,
        [CreditLimit] decimal(18,2) NOT NULL,
        [OpenAt] datetime2 NULL,
        [CloseAt] datetime2 NULL,
        [MaturityDate] datetime2 NULL,
        [PaymentDueDate] datetime2 NULL,
        [PaymentAmountDue] decimal(18,2) NOT NULL,
        [StandardPaymentAmount] decimal(18,2) NOT NULL,
        [LastPaymentDate] datetime2 NULL,
        [LastPaymentAmount] decimal(18,2) NOT NULL,
        [PayoffAmount] decimal(18,2) NOT NULL,
        [PayoffAsOf] datetime2 NULL,
        [InterestRate] decimal(18,2) NOT NULL,
        [InterestLastMonth] decimal(18,2) NOT NULL,
        [InterestThisYear] decimal(18,2) NOT NULL,
        [InterestLastYear] decimal(18,2) NOT NULL,
        [InterestFromOpen] decimal(18,2) NOT NULL,
        [DividendRate] decimal(18,2) NOT NULL,
        [DividendLastMonth] decimal(18,2) NOT NULL,
        [DividendThisYear] decimal(18,2) NOT NULL,
        [DividendLastYear] decimal(18,2) NOT NULL,
        [DividendFromOpen] decimal(18,2) NOT NULL,
        [OwnerName] nvarchar(max) NULL,
        [OwnerAddress] nvarchar(max) NULL,
        [OwnerTaxId] nvarchar(max) NULL,
        [OwnerData] nvarchar(max) NULL,
        [TaxData] nvarchar(max) NULL,
        [Micr] nvarchar(max) NULL,
        [IsRegD] bit NOT NULL,
        [RegDTransferCount] int NOT NULL,
        [PrimaryOwner] bit NOT NULL,
        CONSTRAINT [PK_SubAccounts] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_SubAccounts_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_SubAccounts_ProductDefinitions_ProductDefinitionId] FOREIGN KEY ([ProductDefinitionId]) REFERENCES [ProductDefinitions] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Transfers] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(max) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [AccountId] bigint NULL,
        [TransferType] int NOT NULL,
        [Source] nvarchar(max) NULL,
        [Note] nvarchar(max) NULL,
        [AccountNumberFrom] nvarchar(max) NULL,
        [RoutingNumberFrom] nvarchar(max) NULL,
        [AccountNumberTo] nvarchar(max) NULL,
        [RoutingNumberTo] nvarchar(max) NULL,
        [SubAccountCoreKeyFrom] nvarchar(max) NULL,
        [SubAccountCoreKeyTo] nvarchar(max) NULL,
        [Amount] decimal(18,2) NOT NULL,
        [OriginalTransferDay] int NOT NULL,
        [CoreRequest] nvarchar(max) NULL,
        [CoreResponse] nvarchar(max) NULL,
        [Successful] bit NOT NULL,
        [CreatedByUserId] nvarchar(max) NULL,
        [ConfirmationNumber] nvarchar(max) NULL,
        [AmountCode] nvarchar(max) NULL,
        [StartAt] datetime2 NULL,
        [EndAt] datetime2 NULL,
        [LastTransferDate] datetime2 NULL,
        [NextTransferDate] datetime2 NULL,
        [ScheduleDay1] int NULL,
        [ScheduleDay2] int NULL,
        [FrequencyName] nvarchar(max) NULL,
        [FrequencyCode] nvarchar(max) NULL,
        [CanEdit] bit NULL,
        [CanDelete] bit NULL,
        [IsDeleted] bit NULL,
        CONSTRAINT [PK_Transfers] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Transfers_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Transfers_Accounts_AccountId1] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [DepositImages] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [DepositId] bigint NOT NULL,
        [CheckFront] nvarchar(max) NULL,
        [CheckBack] nvarchar(max) NULL,
        CONSTRAINT [PK_DepositImages] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_DepositImages_Deposits_DepositId] FOREIGN KEY ([DepositId]) REFERENCES [Deposits] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [AccountStatementOptions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [AccountId] bigint NULL,
        [SubAccountId] bigint NULL,
        [DeliveryOption] int NOT NULL,
        CONSTRAINT [PK_AccountStatementOptions] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AccountStatementOptions_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_AccountStatementOptions_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [BillPayPayees] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [PortfolioId] bigint NOT NULL,
        [Nickname] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [PaymentType] int NOT NULL,
        [PayeeType] int NOT NULL,
        [PayeeAccountType] nvarchar(max) NULL,
        [AccountNumber] nvarchar(max) NULL,
        [RoutingNumber] nvarchar(max) NULL,
        [PhoneNumber] nvarchar(max) NULL,
        [AddressLine1] nvarchar(max) NULL,
        [AddressLine2] nvarchar(max) NULL,
        [City] nvarchar(max) NULL,
        [State] nvarchar(max) NULL,
        [PostalCode] nvarchar(max) NULL,
        [Country] nvarchar(max) NULL,
        [CountryCode] nvarchar(max) NULL,
        [NameOnAccount] nvarchar(max) NULL,
        [LastPaymentId] nvarchar(max) NULL,
        [LastPaymentDate] datetime2 NULL,
        [LastPaymentAmount] decimal(18,2) NULL,
        [DefaultFromAccountId] bigint NULL,
        CONSTRAINT [PK_BillPayPayees] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BillPayPayees_SubAccounts_DefaultFromAccountId] FOREIGN KEY ([DefaultFromAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_BillPayPayees_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [ExternalTransfers] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(max) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [AccountId] bigint NOT NULL,
        [ExternalAccountId] bigint NOT NULL,
        [SubAccountId] bigint NOT NULL,
        [Amount] decimal(18,2) NOT NULL,
        [StartDate] datetime2 NOT NULL,
        [NextDate] datetime2 NULL,
        [EndDate] datetime2 NULL,
        [FrequencyCode] nvarchar(max) NULL,
        [ScheduleDay1] int NOT NULL,
        [ScheduleDay2] int NOT NULL,
        [LastDate] datetime2 NULL,
        [LastAmount] decimal(18,2) NOT NULL,
        [IsDeleted] bit NOT NULL,
        [ConfirmationNumber] nvarchar(max) NULL,
        [Note] nvarchar(max) NULL,
        CONSTRAINT [PK_ExternalTransfers] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ExternalTransfers_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_ExternalTransfers_ExternalAccounts_ExternalAccountId] FOREIGN KEY ([ExternalAccountId]) REFERENCES [ExternalAccounts] ([Id]),
        CONSTRAINT [FK_ExternalTransfers_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Holds] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [SubAccountId] bigint NOT NULL,
        [Amount] decimal(18,2) NULL,
        [CheckNumberStart] nvarchar(max) NULL,
        [CheckNumberEnd] nvarchar(max) NULL,
        [StartDate] datetime2 NULL,
        [ExpirationDate] datetime2 NULL,
        [Payee] nvarchar(max) NULL,
        [FeeAmount] decimal(18,2) NOT NULL,
        [StopPaymentType] int NOT NULL,
        [DeactivateDate] datetime2 NULL,
        [IsDeleted] bit NOT NULL,
        [ConfirmationNumber] nvarchar(max) NULL,
        CONSTRAINT [PK_Holds] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Holds_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [OverdraftSettings] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [SubAccountId] bigint NOT NULL,
        [DebitOptIn] bit NOT NULL,
        [CheckOptIn] bit NOT NULL,
        [SweepAccounts] nvarchar(max) NULL,
        CONSTRAINT [PK_OverdraftSettings] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_OverdraftSettings_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [SubAccountMetadata] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [SubAccountId] bigint NOT NULL,
        [IsDefault] bit NOT NULL,
        [ContextName] nvarchar(max) NULL,
        [Metadata] nvarchar(max) NULL,
        CONSTRAINT [PK_SubAccountMetadata] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_SubAccountMetadata_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [Transactions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [SubAccountId] bigint NOT NULL,
        [MemoPost] bit NOT NULL,
        [PostDate] datetime2 NULL,
        [EffectiveDate] datetime2 NULL,
        [MccCode] nvarchar(max) NULL,
        [Code] nvarchar(max) NULL,
        [CodeData] nvarchar(max) NULL,
        [Pending] bit NOT NULL,
        [Type] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        [Memo] nvarchar(max) NULL,
        [MemoList] nvarchar(max) NULL,
        [MemoData] nvarchar(max) NULL,
        [Amount] decimal(18,2) NOT NULL,
        [AmountEscrow] decimal(18,2) NOT NULL,
        [AmountInterest] decimal(18,2) NOT NULL,
        [AmountFee] decimal(18,2) NOT NULL,
        [AmountPrincipal] decimal(18,2) NOT NULL,
        [AmountUnappied] decimal(18,2) NOT NULL,
        [NewBalance] decimal(18,2) NOT NULL,
        [BalanceChange] decimal(18,2) NOT NULL,
        [PreviousBalance] decimal(18,2) NOT NULL,
        [IsRecurring] bit NOT NULL,
        [Frequency] nvarchar(max) NULL,
        CONSTRAINT [PK_Transactions] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Transactions_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [UserSubAccountSettings] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [UserId] nvarchar(max) NULL,
        [SubAccountId] bigint NOT NULL,
        [IsFavorite] bit NOT NULL,
        [SortOrder] int NOT NULL,
        CONSTRAINT [PK_UserSubAccountSettings] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_UserSubAccountSettings_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [BillPayPayments] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [PortfolioId] bigint NOT NULL,
        [PayeeId] bigint NULL,
        [FromAccountId] bigint NULL,
        [Amount] decimal(18,2) NOT NULL,
        [EstimatedArrivalDate] datetime2 NOT NULL,
        [FrequencyCode] nvarchar(max) NULL,
        [StartDate] datetime2 NOT NULL,
        [EndDate] datetime2 NULL,
        [Memo] nvarchar(max) NULL,
        [PaymentType] int NOT NULL,
        [NextPaymentDate] datetime2 NULL,
        [NextArrivalDate] datetime2 NULL,
        [LastPaymentDate] datetime2 NULL,
        [LastPaymentAmount] decimal(18,2) NULL,
        [ScheduleDay1] int NOT NULL,
        [ScheduleDay2] int NOT NULL,
        [ConfirmationNumber] nvarchar(max) NULL,
        [IsDeleted] bit NOT NULL,
        CONSTRAINT [PK_BillPayPayments] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BillPayPayments_SubAccounts_FromAccountId] FOREIGN KEY ([FromAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_BillPayPayments_BillPayPayees_PayeeId] FOREIGN KEY ([PayeeId]) REFERENCES [BillPayPayees] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_BillPayPayments_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [TransactionChecks] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(450) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        [TransactionId] bigint NOT NULL,
        [Amount] decimal(18,2) NOT NULL,
        [Number] nvarchar(max) NULL,
        [Micr] nvarchar(max) NULL,
        [Trace] nvarchar(max) NULL,
        [Date] datetime2 NULL,
        [ProviderImageAvailable] bit NOT NULL,
        CONSTRAINT [PK_TransactionChecks] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_TransactionChecks_Transactions_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [Transactions] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE TABLE [TransactionCheckImages] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ResourceId] uniqueidentifier NOT NULL,
        [Name] nvarchar(max) NULL,
        [SubType] nvarchar(max) NULL,
        [MimeType] nvarchar(max) NULL,
        [File] varbinary(max) NULL,
        [TransactionCheckId] bigint NOT NULL,
        [TransactionId] bigint NOT NULL,
        CONSTRAINT [PK_TransactionCheckImages] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_TransactionCheckImages_TransactionChecks_TransactionCheckId] FOREIGN KEY ([TransactionCheckId]) REFERENCES [TransactionChecks] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_AccountDocuments_AccountId] ON [AccountDocuments] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Accounts_ExternalKey] ON [Accounts] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Accounts_PortfolioId] ON [Accounts] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Accounts_SourceId] ON [Accounts] ([SourceId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_AccountStatementOptions_AccountId] ON [AccountStatementOptions] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_AccountStatementOptions_SubAccountId] ON [AccountStatementOptions] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ActivityLogs_Id] ON [ActivityLogs] ([Id]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ActivityLogs_UserId] ON [ActivityLogs] ([UserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Addresses_ExternalKey] ON [Addresses] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Addresses_PortfolioId] ON [Addresses] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_AdminActivityLogs_AdminId] ON [AdminActivityLogs] ([AdminId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_AdminActivityLogs_Id] ON [AdminActivityLogs] ([Id]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_BillPayPayees_DefaultFromAccountId] ON [BillPayPayees] ([DefaultFromAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_BillPayPayees_ExternalKey] ON [BillPayPayees] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_BillPayPayees_PortfolioId] ON [BillPayPayees] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_BillPayPayments_ExternalKey] ON [BillPayPayments] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_BillPayPayments_FromAccountId] ON [BillPayPayments] ([FromAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_BillPayPayments_PayeeId] ON [BillPayPayments] ([PayeeId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_BillPayPayments_PortfolioId] ON [BillPayPayments] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_BillPayProfile_VendorDataId] ON [BillPayProfile] ([VendorDataId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Cards_AccountId] ON [Cards] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Cards_ExternalKey] ON [Cards] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Cards_ProductDefinitionId] ON [Cards] ([ProductDefinitionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_Configurations_FullTypeName_InstanceName] ON [Configurations] ([FullTypeName], [InstanceName]) WHERE [FullTypeName] IS NOT NULL AND [InstanceName] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_DepositImages_DepositId] ON [DepositImages] ([DepositId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Deposits_AccountId] ON [Deposits] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Devices_BiometricAuthenticationDeviceSettingId] ON [Devices] ([BiometricAuthenticationDeviceSettingId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Devices_TrustedDeviceSettingId] ON [Devices] ([TrustedDeviceSettingId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DeviceSettings_Key] ON [DeviceSettings] ([Key]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalAdminRoles_DigitalAdminId1] ON [DigitalAdminRoles] ([DigitalAdminId1]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalAdminRoles_DigitalRoleId] ON [DigitalAdminRoles] ([DigitalRoleId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_DigitalAdminRoles_DigitalAdminId_DigitalRoleId] ON [DigitalAdminRoles] ([DigitalAdminId], [DigitalRoleId]) WHERE [DigitalRoleId] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [UserNameIndex] ON [DigitalAdmins] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalRolePermissions_FeaturePermissionId] ON [DigitalRolePermissions] ([FeaturePermissionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_DigitalRolePermissions_DigitalRoleId_FeaturePermissionId] ON [DigitalRolePermissions] ([DigitalRoleId], [FeaturePermissionId]) WHERE [DigitalRoleId] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [RoleNameIndex] ON [DigitalRoles] ([NormalizedName]) WHERE [NormalizedName] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalUserAnnouncements_AnnouncementId] ON [DigitalUserAnnouncements] ([AnnouncementId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalUserAnnouncements_DigitalUserId] ON [DigitalUserAnnouncements] ([DigitalUserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_DigitalUserAnnouncements_DigitalUserId_AnnouncementId] ON [DigitalUserAnnouncements] ([DigitalUserId], [AnnouncementId]) WHERE [DigitalUserId] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_DigitalUserPreferences_DigitalUserId] ON [DigitalUserPreferences] ([DigitalUserId]) WHERE [DigitalUserId] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalUserRoles_DigitalRoleId] ON [DigitalUserRoles] ([DigitalRoleId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalUserRoles_DigitalUserId1] ON [DigitalUserRoles] ([DigitalUserId1]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_DigitalUserRoles_DigitalUserId_DigitalRoleId] ON [DigitalUserRoles] ([DigitalUserId], [DigitalRoleId]) WHERE [DigitalRoleId] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalUsers_BusinessId] ON [DigitalUsers] ([BusinessId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [UserNameIndex] ON [DigitalUsers] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_DigitalUsers_PortfolioId] ON [DigitalUsers] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_EmailAddresses_ExternalKey] ON [EmailAddresses] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_EmailAddresses_PortfolioId] ON [EmailAddresses] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ExternalAccounts_ExternalKey] ON [ExternalAccounts] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ExternalAccounts_PortfolioId] ON [ExternalAccounts] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ExternalTransfers_AccountId] ON [ExternalTransfers] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ExternalTransfers_ExternalAccountId] ON [ExternalTransfers] ([ExternalAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ExternalTransfers_SubAccountId] ON [ExternalTransfers] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_FeaturePermissions_FeatureId] ON [FeaturePermissions] ([FeatureId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_FeaturePermissions_FullyQualifiedName] ON [FeaturePermissions] ([FullyQualifiedName]) WHERE [FullyQualifiedName] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_FeatureProductContexts_FeatureId] ON [FeatureProductContexts] ([FeatureId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_FeatureProductContexts_FullyQualifiedName] ON [FeatureProductContexts] ([FullyQualifiedName]) WHERE [FullyQualifiedName] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_FeatureProductContexts_MetadataDefinitionGroupId] ON [FeatureProductContexts] ([MetadataDefinitionGroupId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_FinancialInstitutions_RoutingNumber] ON [FinancialInstitutions] ([RoutingNumber]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Holds_ExternalKey] ON [Holds] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Holds_SubAccountId] ON [Holds] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_JobProperties_JobId] ON [JobProperties] ([JobId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE UNIQUE INDEX [IX_Jobs_InstanceGuid] ON [Jobs] ([InstanceGuid]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_MetadataDefinitionGroupLinks_MetadataDefinitionId] ON [MetadataDefinitionGroupLinks] ([MetadataDefinitionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_MetadataDefinitionGroupLinks_MetadataDefinitionGroupId_MetadataDefinitionId] ON [MetadataDefinitionGroupLinks] ([MetadataDefinitionGroupId], [MetadataDefinitionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_OverdraftSettings_SubAccountId] ON [OverdraftSettings] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Payments_AccountId] ON [Payments] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Payments_ExternalKey] ON [Payments] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Persons_ExternalKey] ON [Persons] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Persons_PortfolioId] ON [Persons] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_PhoneNumbers_ExternalKey] ON [PhoneNumbers] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_PhoneNumbers_PortfolioId] ON [PhoneNumbers] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ProductDefinitions_AvailableMetadataDefinitionsId] ON [ProductDefinitions] ([AvailableMetadataDefinitionsId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ProductFeatureContextLinks_ProductDefinitionId] ON [ProductFeatureContextLinks] ([ProductDefinitionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_ProductFeatureContextLinks_FeatureProductContextId_ProductDefinitionId] ON [ProductFeatureContextLinks] ([FeatureProductContextId], [ProductDefinitionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_SecureMessageAttachments_SecureMessageId] ON [SecureMessageAttachments] ([SecureMessageId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_SubAccountMetadata_FeatureContextId] ON [SubAccountMetadata] ([FeatureContextId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_SubAccountMetadata_SubAccountId] ON [SubAccountMetadata] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_SubAccounts_AccountId] ON [SubAccounts] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_SubAccounts_ExternalKey] ON [SubAccounts] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_SubAccounts_ProductDefinitionId] ON [SubAccounts] ([ProductDefinitionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_SyncStatuses_PortfolioId] ON [SyncStatuses] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_TransactionCheckImages_TransactionCheckId] ON [TransactionCheckImages] ([TransactionCheckId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_TransactionChecks_ExternalKey] ON [TransactionChecks] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_TransactionChecks_TransactionId] ON [TransactionChecks] ([TransactionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Transactions_ExternalKey] ON [Transactions] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Transactions_SubAccountId] ON [Transactions] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_TransferRecipients_PortfolioId] ON [TransferRecipients] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Transfers_AccountId] ON [Transfers] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_Transfers_AccountId1] ON [Transfers] ([AccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_UserSubAccountSettings_SubAccountId] ON [UserSubAccountSettings] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_UserVendorData_CoreKey] ON [UserVendorData] ([CoreKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_UserVendorData_PortfolioId] ON [UserVendorData] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    CREATE INDEX [IX_UserVendorData_UserId] ON [UserVendorData] ([UserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[AccountDocuments_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[AccountDocuments_InsertAt] ON [dbo].[AccountDocuments] AFTER INSERT AS
                         UPDATE AccountDocuments SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE AccountDocuments.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[AccountDocuments_InsertAt] ON [dbo].[AccountDocuments] AFTER INSERT AS
                         UPDATE AccountDocuments SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE AccountDocuments.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[AccountDocuments] ENABLE TRIGGER[AccountDocuments_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[AccountDocuments_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[AccountDocuments_UpdateAt] ON [dbo].[AccountDocuments] AFTER UPDATE AS
                         UPDATE AccountDocuments SET UpdateAt = getutcdate() FROM INSERTED WHERE AccountDocuments.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[AccountDocuments_UpdateAt] ON [dbo].[AccountDocuments] AFTER UPDATE AS
                         UPDATE AccountDocuments SET UpdateAt = getutcdate() FROM INSERTED WHERE AccountDocuments.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[AccountDocuments] ENABLE TRIGGER[AccountDocuments_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Accounts_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Accounts_InsertAt] ON [dbo].[Accounts] AFTER INSERT AS
                         UPDATE Accounts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Accounts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Accounts_InsertAt] ON [dbo].[Accounts] AFTER INSERT AS
                         UPDATE Accounts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Accounts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Accounts] ENABLE TRIGGER[Accounts_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Accounts_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Accounts_UpdateAt] ON [dbo].[Accounts] AFTER UPDATE AS
                         UPDATE Accounts SET UpdateAt = getutcdate() FROM INSERTED WHERE Accounts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Accounts_UpdateAt] ON [dbo].[Accounts] AFTER UPDATE AS
                         UPDATE Accounts SET UpdateAt = getutcdate() FROM INSERTED WHERE Accounts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Accounts] ENABLE TRIGGER[Accounts_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[AccountStatementOptions_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[AccountStatementOptions_InsertAt] ON [dbo].[AccountStatementOptions] AFTER INSERT AS
                         UPDATE AccountStatementOptions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE AccountStatementOptions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[AccountStatementOptions_InsertAt] ON [dbo].[AccountStatementOptions] AFTER INSERT AS
                         UPDATE AccountStatementOptions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE AccountStatementOptions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[AccountStatementOptions] ENABLE TRIGGER[AccountStatementOptions_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[AccountStatementOptions_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[AccountStatementOptions_UpdateAt] ON [dbo].[AccountStatementOptions] AFTER UPDATE AS
                         UPDATE AccountStatementOptions SET UpdateAt = getutcdate() FROM INSERTED WHERE AccountStatementOptions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[AccountStatementOptions_UpdateAt] ON [dbo].[AccountStatementOptions] AFTER UPDATE AS
                         UPDATE AccountStatementOptions SET UpdateAt = getutcdate() FROM INSERTED WHERE AccountStatementOptions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[AccountStatementOptions] ENABLE TRIGGER[AccountStatementOptions_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ActivityLogLabels_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ActivityLogLabels_InsertAt] ON [dbo].[ActivityLogLabels] AFTER INSERT AS
                         UPDATE ActivityLogLabels SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ActivityLogLabels.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ActivityLogLabels_InsertAt] ON [dbo].[ActivityLogLabels] AFTER INSERT AS
                         UPDATE ActivityLogLabels SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ActivityLogLabels.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ActivityLogLabels] ENABLE TRIGGER[ActivityLogLabels_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ActivityLogLabels_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ActivityLogLabels_UpdateAt] ON [dbo].[ActivityLogLabels] AFTER UPDATE AS
                         UPDATE ActivityLogLabels SET UpdateAt = getutcdate() FROM INSERTED WHERE ActivityLogLabels.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ActivityLogLabels_UpdateAt] ON [dbo].[ActivityLogLabels] AFTER UPDATE AS
                         UPDATE ActivityLogLabels SET UpdateAt = getutcdate() FROM INSERTED WHERE ActivityLogLabels.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ActivityLogLabels] ENABLE TRIGGER[ActivityLogLabels_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Addresses_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Addresses_InsertAt] ON [dbo].[Addresses] AFTER INSERT AS
                         UPDATE Addresses SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Addresses.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Addresses_InsertAt] ON [dbo].[Addresses] AFTER INSERT AS
                         UPDATE Addresses SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Addresses.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Addresses] ENABLE TRIGGER[Addresses_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Addresses_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Addresses_UpdateAt] ON [dbo].[Addresses] AFTER UPDATE AS
                         UPDATE Addresses SET UpdateAt = getutcdate() FROM INSERTED WHERE Addresses.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Addresses_UpdateAt] ON [dbo].[Addresses] AFTER UPDATE AS
                         UPDATE Addresses SET UpdateAt = getutcdate() FROM INSERTED WHERE Addresses.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Addresses] ENABLE TRIGGER[Addresses_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Announcements_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Announcements_InsertAt] ON [dbo].[Announcements] AFTER INSERT AS
                         UPDATE Announcements SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Announcements.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Announcements_InsertAt] ON [dbo].[Announcements] AFTER INSERT AS
                         UPDATE Announcements SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Announcements.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Announcements] ENABLE TRIGGER[Announcements_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Announcements_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Announcements_UpdateAt] ON [dbo].[Announcements] AFTER UPDATE AS
                         UPDATE Announcements SET UpdateAt = getutcdate() FROM INSERTED WHERE Announcements.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Announcements_UpdateAt] ON [dbo].[Announcements] AFTER UPDATE AS
                         UPDATE Announcements SET UpdateAt = getutcdate() FROM INSERTED WHERE Announcements.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Announcements] ENABLE TRIGGER[Announcements_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Assets_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Assets_InsertAt] ON [dbo].[Assets] AFTER INSERT AS
                         UPDATE Assets SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Assets.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Assets_InsertAt] ON [dbo].[Assets] AFTER INSERT AS
                         UPDATE Assets SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Assets.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Assets] ENABLE TRIGGER[Assets_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Assets_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Assets_UpdateAt] ON [dbo].[Assets] AFTER UPDATE AS
                         UPDATE Assets SET UpdateAt = getutcdate() FROM INSERTED WHERE Assets.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Assets_UpdateAt] ON [dbo].[Assets] AFTER UPDATE AS
                         UPDATE Assets SET UpdateAt = getutcdate() FROM INSERTED WHERE Assets.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Assets] ENABLE TRIGGER[Assets_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[BillPayPayees_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[BillPayPayees_InsertAt] ON [dbo].[BillPayPayees] AFTER INSERT AS
                         UPDATE BillPayPayees SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE BillPayPayees.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[BillPayPayees_InsertAt] ON [dbo].[BillPayPayees] AFTER INSERT AS
                         UPDATE BillPayPayees SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE BillPayPayees.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[BillPayPayees] ENABLE TRIGGER[BillPayPayees_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[BillPayPayees_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[BillPayPayees_UpdateAt] ON [dbo].[BillPayPayees] AFTER UPDATE AS
                         UPDATE BillPayPayees SET UpdateAt = getutcdate() FROM INSERTED WHERE BillPayPayees.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[BillPayPayees_UpdateAt] ON [dbo].[BillPayPayees] AFTER UPDATE AS
                         UPDATE BillPayPayees SET UpdateAt = getutcdate() FROM INSERTED WHERE BillPayPayees.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[BillPayPayees] ENABLE TRIGGER[BillPayPayees_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[BillPayPayments_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[BillPayPayments_InsertAt] ON [dbo].[BillPayPayments] AFTER INSERT AS
                         UPDATE BillPayPayments SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE BillPayPayments.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[BillPayPayments_InsertAt] ON [dbo].[BillPayPayments] AFTER INSERT AS
                         UPDATE BillPayPayments SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE BillPayPayments.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[BillPayPayments] ENABLE TRIGGER[BillPayPayments_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[BillPayPayments_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[BillPayPayments_UpdateAt] ON [dbo].[BillPayPayments] AFTER UPDATE AS
                         UPDATE BillPayPayments SET UpdateAt = getutcdate() FROM INSERTED WHERE BillPayPayments.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[BillPayPayments_UpdateAt] ON [dbo].[BillPayPayments] AFTER UPDATE AS
                         UPDATE BillPayPayments SET UpdateAt = getutcdate() FROM INSERTED WHERE BillPayPayments.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[BillPayPayments] ENABLE TRIGGER[BillPayPayments_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[BillPayProfile_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[BillPayProfile_InsertAt] ON [dbo].[BillPayProfile] AFTER INSERT AS
                         UPDATE BillPayProfile SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE BillPayProfile.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[BillPayProfile_InsertAt] ON [dbo].[BillPayProfile] AFTER INSERT AS
                         UPDATE BillPayProfile SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE BillPayProfile.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[BillPayProfile] ENABLE TRIGGER[BillPayProfile_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[BillPayProfile_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[BillPayProfile_UpdateAt] ON [dbo].[BillPayProfile] AFTER UPDATE AS
                         UPDATE BillPayProfile SET UpdateAt = getutcdate() FROM INSERTED WHERE BillPayProfile.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[BillPayProfile_UpdateAt] ON [dbo].[BillPayProfile] AFTER UPDATE AS
                         UPDATE BillPayProfile SET UpdateAt = getutcdate() FROM INSERTED WHERE BillPayProfile.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[BillPayProfile] ENABLE TRIGGER[BillPayProfile_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Businesses_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Businesses_InsertAt] ON [dbo].[Businesses] AFTER INSERT AS
                         UPDATE Businesses SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Businesses.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Businesses_InsertAt] ON [dbo].[Businesses] AFTER INSERT AS
                         UPDATE Businesses SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Businesses.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Businesses] ENABLE TRIGGER[Businesses_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Businesses_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Businesses_UpdateAt] ON [dbo].[Businesses] AFTER UPDATE AS
                         UPDATE Businesses SET UpdateAt = getutcdate() FROM INSERTED WHERE Businesses.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Businesses_UpdateAt] ON [dbo].[Businesses] AFTER UPDATE AS
                         UPDATE Businesses SET UpdateAt = getutcdate() FROM INSERTED WHERE Businesses.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Businesses] ENABLE TRIGGER[Businesses_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Cards_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Cards_InsertAt] ON [dbo].[Cards] AFTER INSERT AS
                         UPDATE Cards SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Cards.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Cards_InsertAt] ON [dbo].[Cards] AFTER INSERT AS
                         UPDATE Cards SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Cards.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Cards] ENABLE TRIGGER[Cards_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Cards_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Cards_UpdateAt] ON [dbo].[Cards] AFTER UPDATE AS
                         UPDATE Cards SET UpdateAt = getutcdate() FROM INSERTED WHERE Cards.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Cards_UpdateAt] ON [dbo].[Cards] AFTER UPDATE AS
                         UPDATE Cards SET UpdateAt = getutcdate() FROM INSERTED WHERE Cards.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Cards] ENABLE TRIGGER[Cards_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Configurations_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Configurations_InsertAt] ON [dbo].[Configurations] AFTER INSERT AS
                         UPDATE Configurations SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Configurations.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Configurations_InsertAt] ON [dbo].[Configurations] AFTER INSERT AS
                         UPDATE Configurations SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Configurations.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Configurations] ENABLE TRIGGER[Configurations_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Configurations_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Configurations_UpdateAt] ON [dbo].[Configurations] AFTER UPDATE AS
                         UPDATE Configurations SET UpdateAt = getutcdate() FROM INSERTED WHERE Configurations.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Configurations_UpdateAt] ON [dbo].[Configurations] AFTER UPDATE AS
                         UPDATE Configurations SET UpdateAt = getutcdate() FROM INSERTED WHERE Configurations.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Configurations] ENABLE TRIGGER[Configurations_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Countries_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Countries_InsertAt] ON [dbo].[Countries] AFTER INSERT AS
                         UPDATE Countries SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Countries.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Countries_InsertAt] ON [dbo].[Countries] AFTER INSERT AS
                         UPDATE Countries SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Countries.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Countries] ENABLE TRIGGER[Countries_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Countries_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Countries_UpdateAt] ON [dbo].[Countries] AFTER UPDATE AS
                         UPDATE Countries SET UpdateAt = getutcdate() FROM INSERTED WHERE Countries.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Countries_UpdateAt] ON [dbo].[Countries] AFTER UPDATE AS
                         UPDATE Countries SET UpdateAt = getutcdate() FROM INSERTED WHERE Countries.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Countries] ENABLE TRIGGER[Countries_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DepositImages_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DepositImages_InsertAt] ON [dbo].[DepositImages] AFTER INSERT AS
                         UPDATE DepositImages SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DepositImages.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DepositImages_InsertAt] ON [dbo].[DepositImages] AFTER INSERT AS
                         UPDATE DepositImages SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DepositImages.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DepositImages] ENABLE TRIGGER[DepositImages_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DepositImages_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DepositImages_UpdateAt] ON [dbo].[DepositImages] AFTER UPDATE AS
                         UPDATE DepositImages SET UpdateAt = getutcdate() FROM INSERTED WHERE DepositImages.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DepositImages_UpdateAt] ON [dbo].[DepositImages] AFTER UPDATE AS
                         UPDATE DepositImages SET UpdateAt = getutcdate() FROM INSERTED WHERE DepositImages.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DepositImages] ENABLE TRIGGER[DepositImages_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Deposits_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Deposits_InsertAt] ON [dbo].[Deposits] AFTER INSERT AS
                         UPDATE Deposits SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Deposits.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Deposits_InsertAt] ON [dbo].[Deposits] AFTER INSERT AS
                         UPDATE Deposits SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Deposits.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Deposits] ENABLE TRIGGER[Deposits_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Deposits_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Deposits_UpdateAt] ON [dbo].[Deposits] AFTER UPDATE AS
                         UPDATE Deposits SET UpdateAt = getutcdate() FROM INSERTED WHERE Deposits.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Deposits_UpdateAt] ON [dbo].[Deposits] AFTER UPDATE AS
                         UPDATE Deposits SET UpdateAt = getutcdate() FROM INSERTED WHERE Deposits.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Deposits] ENABLE TRIGGER[Deposits_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Devices_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Devices_InsertAt] ON [dbo].[Devices] AFTER INSERT AS
                         UPDATE Devices SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Devices.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Devices_InsertAt] ON [dbo].[Devices] AFTER INSERT AS
                         UPDATE Devices SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Devices.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Devices] ENABLE TRIGGER[Devices_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Devices_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Devices_UpdateAt] ON [dbo].[Devices] AFTER UPDATE AS
                         UPDATE Devices SET UpdateAt = getutcdate() FROM INSERTED WHERE Devices.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Devices_UpdateAt] ON [dbo].[Devices] AFTER UPDATE AS
                         UPDATE Devices SET UpdateAt = getutcdate() FROM INSERTED WHERE Devices.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Devices] ENABLE TRIGGER[Devices_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DeviceSettings_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DeviceSettings_InsertAt] ON [dbo].[DeviceSettings] AFTER INSERT AS
                         UPDATE DeviceSettings SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DeviceSettings.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DeviceSettings_InsertAt] ON [dbo].[DeviceSettings] AFTER INSERT AS
                         UPDATE DeviceSettings SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DeviceSettings.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DeviceSettings] ENABLE TRIGGER[DeviceSettings_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DeviceSettings_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DeviceSettings_UpdateAt] ON [dbo].[DeviceSettings] AFTER UPDATE AS
                         UPDATE DeviceSettings SET UpdateAt = getutcdate() FROM INSERTED WHERE DeviceSettings.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DeviceSettings_UpdateAt] ON [dbo].[DeviceSettings] AFTER UPDATE AS
                         UPDATE DeviceSettings SET UpdateAt = getutcdate() FROM INSERTED WHERE DeviceSettings.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DeviceSettings] ENABLE TRIGGER[DeviceSettings_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalAdminRoles_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalAdminRoles_InsertAt] ON [dbo].[DigitalAdminRoles] AFTER INSERT AS
                         UPDATE DigitalAdminRoles SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalAdminRoles.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalAdminRoles_InsertAt] ON [dbo].[DigitalAdminRoles] AFTER INSERT AS
                         UPDATE DigitalAdminRoles SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalAdminRoles.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalAdminRoles] ENABLE TRIGGER[DigitalAdminRoles_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalAdminRoles_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalAdminRoles_UpdateAt] ON [dbo].[DigitalAdminRoles] AFTER UPDATE AS
                         UPDATE DigitalAdminRoles SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalAdminRoles.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalAdminRoles_UpdateAt] ON [dbo].[DigitalAdminRoles] AFTER UPDATE AS
                         UPDATE DigitalAdminRoles SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalAdminRoles.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalAdminRoles] ENABLE TRIGGER[DigitalAdminRoles_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalAdmins_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalAdmins_InsertAt] ON [dbo].[DigitalAdmins] AFTER INSERT AS
                         UPDATE DigitalAdmins SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalAdmins.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalAdmins_InsertAt] ON [dbo].[DigitalAdmins] AFTER INSERT AS
                         UPDATE DigitalAdmins SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalAdmins.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalAdmins] ENABLE TRIGGER[DigitalAdmins_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalAdmins_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalAdmins_UpdateAt] ON [dbo].[DigitalAdmins] AFTER UPDATE AS
                         UPDATE DigitalAdmins SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalAdmins.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalAdmins_UpdateAt] ON [dbo].[DigitalAdmins] AFTER UPDATE AS
                         UPDATE DigitalAdmins SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalAdmins.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalAdmins] ENABLE TRIGGER[DigitalAdmins_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalRolePermissions_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalRolePermissions_InsertAt] ON [dbo].[DigitalRolePermissions] AFTER INSERT AS
                         UPDATE DigitalRolePermissions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalRolePermissions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalRolePermissions_InsertAt] ON [dbo].[DigitalRolePermissions] AFTER INSERT AS
                         UPDATE DigitalRolePermissions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalRolePermissions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalRolePermissions] ENABLE TRIGGER[DigitalRolePermissions_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalRolePermissions_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalRolePermissions_UpdateAt] ON [dbo].[DigitalRolePermissions] AFTER UPDATE AS
                         UPDATE DigitalRolePermissions SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalRolePermissions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalRolePermissions_UpdateAt] ON [dbo].[DigitalRolePermissions] AFTER UPDATE AS
                         UPDATE DigitalRolePermissions SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalRolePermissions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalRolePermissions] ENABLE TRIGGER[DigitalRolePermissions_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalRoles_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalRoles_InsertAt] ON [dbo].[DigitalRoles] AFTER INSERT AS
                         UPDATE DigitalRoles SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalRoles.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalRoles_InsertAt] ON [dbo].[DigitalRoles] AFTER INSERT AS
                         UPDATE DigitalRoles SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalRoles.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalRoles] ENABLE TRIGGER[DigitalRoles_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalRoles_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalRoles_UpdateAt] ON [dbo].[DigitalRoles] AFTER UPDATE AS
                         UPDATE DigitalRoles SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalRoles.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalRoles_UpdateAt] ON [dbo].[DigitalRoles] AFTER UPDATE AS
                         UPDATE DigitalRoles SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalRoles.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalRoles] ENABLE TRIGGER[DigitalRoles_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalUserAnnouncements_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalUserAnnouncements_InsertAt] ON [dbo].[DigitalUserAnnouncements] AFTER INSERT AS
                         UPDATE DigitalUserAnnouncements SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserAnnouncements.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalUserAnnouncements_InsertAt] ON [dbo].[DigitalUserAnnouncements] AFTER INSERT AS
                         UPDATE DigitalUserAnnouncements SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserAnnouncements.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalUserAnnouncements] ENABLE TRIGGER[DigitalUserAnnouncements_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalUserAnnouncements_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalUserAnnouncements_UpdateAt] ON [dbo].[DigitalUserAnnouncements] AFTER UPDATE AS
                         UPDATE DigitalUserAnnouncements SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserAnnouncements.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalUserAnnouncements_UpdateAt] ON [dbo].[DigitalUserAnnouncements] AFTER UPDATE AS
                         UPDATE DigitalUserAnnouncements SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserAnnouncements.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalUserAnnouncements] ENABLE TRIGGER[DigitalUserAnnouncements_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalUserPreferences_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalUserPreferences_InsertAt] ON [dbo].[DigitalUserPreferences] AFTER INSERT AS
                         UPDATE DigitalUserPreferences SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserPreferences.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalUserPreferences_InsertAt] ON [dbo].[DigitalUserPreferences] AFTER INSERT AS
                         UPDATE DigitalUserPreferences SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserPreferences.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalUserPreferences] ENABLE TRIGGER[DigitalUserPreferences_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalUserPreferences_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalUserPreferences_UpdateAt] ON [dbo].[DigitalUserPreferences] AFTER UPDATE AS
                         UPDATE DigitalUserPreferences SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserPreferences.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalUserPreferences_UpdateAt] ON [dbo].[DigitalUserPreferences] AFTER UPDATE AS
                         UPDATE DigitalUserPreferences SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserPreferences.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalUserPreferences] ENABLE TRIGGER[DigitalUserPreferences_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalUserRoles_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalUserRoles_InsertAt] ON [dbo].[DigitalUserRoles] AFTER INSERT AS
                         UPDATE DigitalUserRoles SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserRoles.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalUserRoles_InsertAt] ON [dbo].[DigitalUserRoles] AFTER INSERT AS
                         UPDATE DigitalUserRoles SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserRoles.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalUserRoles] ENABLE TRIGGER[DigitalUserRoles_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalUserRoles_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalUserRoles_UpdateAt] ON [dbo].[DigitalUserRoles] AFTER UPDATE AS
                         UPDATE DigitalUserRoles SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserRoles.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalUserRoles_UpdateAt] ON [dbo].[DigitalUserRoles] AFTER UPDATE AS
                         UPDATE DigitalUserRoles SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUserRoles.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalUserRoles] ENABLE TRIGGER[DigitalUserRoles_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalUsers_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalUsers_InsertAt] ON [dbo].[DigitalUsers] AFTER INSERT AS
                         UPDATE DigitalUsers SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUsers.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalUsers_InsertAt] ON [dbo].[DigitalUsers] AFTER INSERT AS
                         UPDATE DigitalUsers SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUsers.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalUsers] ENABLE TRIGGER[DigitalUsers_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[DigitalUsers_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[DigitalUsers_UpdateAt] ON [dbo].[DigitalUsers] AFTER UPDATE AS
                         UPDATE DigitalUsers SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUsers.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[DigitalUsers_UpdateAt] ON [dbo].[DigitalUsers] AFTER UPDATE AS
                         UPDATE DigitalUsers SET UpdateAt = getutcdate() FROM INSERTED WHERE DigitalUsers.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[DigitalUsers] ENABLE TRIGGER[DigitalUsers_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[EmailAddresses_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[EmailAddresses_InsertAt] ON [dbo].[EmailAddresses] AFTER INSERT AS
                         UPDATE EmailAddresses SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE EmailAddresses.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[EmailAddresses_InsertAt] ON [dbo].[EmailAddresses] AFTER INSERT AS
                         UPDATE EmailAddresses SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE EmailAddresses.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[EmailAddresses] ENABLE TRIGGER[EmailAddresses_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[EmailAddresses_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[EmailAddresses_UpdateAt] ON [dbo].[EmailAddresses] AFTER UPDATE AS
                         UPDATE EmailAddresses SET UpdateAt = getutcdate() FROM INSERTED WHERE EmailAddresses.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[EmailAddresses_UpdateAt] ON [dbo].[EmailAddresses] AFTER UPDATE AS
                         UPDATE EmailAddresses SET UpdateAt = getutcdate() FROM INSERTED WHERE EmailAddresses.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[EmailAddresses] ENABLE TRIGGER[EmailAddresses_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ExternalAccounts_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ExternalAccounts_InsertAt] ON [dbo].[ExternalAccounts] AFTER INSERT AS
                         UPDATE ExternalAccounts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ExternalAccounts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ExternalAccounts_InsertAt] ON [dbo].[ExternalAccounts] AFTER INSERT AS
                         UPDATE ExternalAccounts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ExternalAccounts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ExternalAccounts] ENABLE TRIGGER[ExternalAccounts_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ExternalAccounts_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ExternalAccounts_UpdateAt] ON [dbo].[ExternalAccounts] AFTER UPDATE AS
                         UPDATE ExternalAccounts SET UpdateAt = getutcdate() FROM INSERTED WHERE ExternalAccounts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ExternalAccounts_UpdateAt] ON [dbo].[ExternalAccounts] AFTER UPDATE AS
                         UPDATE ExternalAccounts SET UpdateAt = getutcdate() FROM INSERTED WHERE ExternalAccounts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ExternalAccounts] ENABLE TRIGGER[ExternalAccounts_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ExternalTransfers_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ExternalTransfers_InsertAt] ON [dbo].[ExternalTransfers] AFTER INSERT AS
                         UPDATE ExternalTransfers SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ExternalTransfers.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ExternalTransfers_InsertAt] ON [dbo].[ExternalTransfers] AFTER INSERT AS
                         UPDATE ExternalTransfers SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ExternalTransfers.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ExternalTransfers] ENABLE TRIGGER[ExternalTransfers_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ExternalTransfers_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ExternalTransfers_UpdateAt] ON [dbo].[ExternalTransfers] AFTER UPDATE AS
                         UPDATE ExternalTransfers SET UpdateAt = getutcdate() FROM INSERTED WHERE ExternalTransfers.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ExternalTransfers_UpdateAt] ON [dbo].[ExternalTransfers] AFTER UPDATE AS
                         UPDATE ExternalTransfers SET UpdateAt = getutcdate() FROM INSERTED WHERE ExternalTransfers.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ExternalTransfers] ENABLE TRIGGER[ExternalTransfers_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[FeaturePermissions_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[FeaturePermissions_InsertAt] ON [dbo].[FeaturePermissions] AFTER INSERT AS
                         UPDATE FeaturePermissions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE FeaturePermissions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[FeaturePermissions_InsertAt] ON [dbo].[FeaturePermissions] AFTER INSERT AS
                         UPDATE FeaturePermissions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE FeaturePermissions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[FeaturePermissions] ENABLE TRIGGER[FeaturePermissions_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[FeaturePermissions_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[FeaturePermissions_UpdateAt] ON [dbo].[FeaturePermissions] AFTER UPDATE AS
                         UPDATE FeaturePermissions SET UpdateAt = getutcdate() FROM INSERTED WHERE FeaturePermissions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[FeaturePermissions_UpdateAt] ON [dbo].[FeaturePermissions] AFTER UPDATE AS
                         UPDATE FeaturePermissions SET UpdateAt = getutcdate() FROM INSERTED WHERE FeaturePermissions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[FeaturePermissions] ENABLE TRIGGER[FeaturePermissions_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[FeatureProductContexts_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[FeatureProductContexts_InsertAt] ON [dbo].[FeatureProductContexts] AFTER INSERT AS
                         UPDATE FeatureProductContexts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE FeatureProductContexts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[FeatureProductContexts_InsertAt] ON [dbo].[FeatureProductContexts] AFTER INSERT AS
                         UPDATE FeatureProductContexts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE FeatureProductContexts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[FeatureProductContexts] ENABLE TRIGGER[FeatureProductContexts_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[FeatureProductContexts_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[FeatureProductContexts_UpdateAt] ON [dbo].[FeatureProductContexts] AFTER UPDATE AS
                         UPDATE FeatureProductContexts SET UpdateAt = getutcdate() FROM INSERTED WHERE FeatureProductContexts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[FeatureProductContexts_UpdateAt] ON [dbo].[FeatureProductContexts] AFTER UPDATE AS
                         UPDATE FeatureProductContexts SET UpdateAt = getutcdate() FROM INSERTED WHERE FeatureProductContexts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[FeatureProductContexts] ENABLE TRIGGER[FeatureProductContexts_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Features_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Features_InsertAt] ON [dbo].[Features] AFTER INSERT AS
                         UPDATE Features SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Features.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Features_InsertAt] ON [dbo].[Features] AFTER INSERT AS
                         UPDATE Features SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Features.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Features] ENABLE TRIGGER[Features_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Features_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Features_UpdateAt] ON [dbo].[Features] AFTER UPDATE AS
                         UPDATE Features SET UpdateAt = getutcdate() FROM INSERTED WHERE Features.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Features_UpdateAt] ON [dbo].[Features] AFTER UPDATE AS
                         UPDATE Features SET UpdateAt = getutcdate() FROM INSERTED WHERE Features.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Features] ENABLE TRIGGER[Features_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[FinancialInstitutions_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[FinancialInstitutions_InsertAt] ON [dbo].[FinancialInstitutions] AFTER INSERT AS
                         UPDATE FinancialInstitutions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE FinancialInstitutions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[FinancialInstitutions_InsertAt] ON [dbo].[FinancialInstitutions] AFTER INSERT AS
                         UPDATE FinancialInstitutions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE FinancialInstitutions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[FinancialInstitutions] ENABLE TRIGGER[FinancialInstitutions_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[FinancialInstitutions_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[FinancialInstitutions_UpdateAt] ON [dbo].[FinancialInstitutions] AFTER UPDATE AS
                         UPDATE FinancialInstitutions SET UpdateAt = getutcdate() FROM INSERTED WHERE FinancialInstitutions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[FinancialInstitutions_UpdateAt] ON [dbo].[FinancialInstitutions] AFTER UPDATE AS
                         UPDATE FinancialInstitutions SET UpdateAt = getutcdate() FROM INSERTED WHERE FinancialInstitutions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[FinancialInstitutions] ENABLE TRIGGER[FinancialInstitutions_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Holds_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Holds_InsertAt] ON [dbo].[Holds] AFTER INSERT AS
                         UPDATE Holds SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Holds.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Holds_InsertAt] ON [dbo].[Holds] AFTER INSERT AS
                         UPDATE Holds SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Holds.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Holds] ENABLE TRIGGER[Holds_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Holds_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Holds_UpdateAt] ON [dbo].[Holds] AFTER UPDATE AS
                         UPDATE Holds SET UpdateAt = getutcdate() FROM INSERTED WHERE Holds.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Holds_UpdateAt] ON [dbo].[Holds] AFTER UPDATE AS
                         UPDATE Holds SET UpdateAt = getutcdate() FROM INSERTED WHERE Holds.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Holds] ENABLE TRIGGER[Holds_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[JobProperties_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[JobProperties_InsertAt] ON [dbo].[JobProperties] AFTER INSERT AS
                         UPDATE JobProperties SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE JobProperties.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[JobProperties_InsertAt] ON [dbo].[JobProperties] AFTER INSERT AS
                         UPDATE JobProperties SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE JobProperties.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[JobProperties] ENABLE TRIGGER[JobProperties_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[JobProperties_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[JobProperties_UpdateAt] ON [dbo].[JobProperties] AFTER UPDATE AS
                         UPDATE JobProperties SET UpdateAt = getutcdate() FROM INSERTED WHERE JobProperties.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[JobProperties_UpdateAt] ON [dbo].[JobProperties] AFTER UPDATE AS
                         UPDATE JobProperties SET UpdateAt = getutcdate() FROM INSERTED WHERE JobProperties.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[JobProperties] ENABLE TRIGGER[JobProperties_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Jobs_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Jobs_InsertAt] ON [dbo].[Jobs] AFTER INSERT AS
                         UPDATE Jobs SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Jobs.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Jobs_InsertAt] ON [dbo].[Jobs] AFTER INSERT AS
                         UPDATE Jobs SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Jobs.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Jobs] ENABLE TRIGGER[Jobs_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Jobs_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Jobs_UpdateAt] ON [dbo].[Jobs] AFTER UPDATE AS
                         UPDATE Jobs SET UpdateAt = getutcdate() FROM INSERTED WHERE Jobs.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Jobs_UpdateAt] ON [dbo].[Jobs] AFTER UPDATE AS
                         UPDATE Jobs SET UpdateAt = getutcdate() FROM INSERTED WHERE Jobs.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Jobs] ENABLE TRIGGER[Jobs_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[MetadataDefinitionGroupLinks_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[MetadataDefinitionGroupLinks_InsertAt] ON [dbo].[MetadataDefinitionGroupLinks] AFTER INSERT AS
                         UPDATE MetadataDefinitionGroupLinks SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitionGroupLinks.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[MetadataDefinitionGroupLinks_InsertAt] ON [dbo].[MetadataDefinitionGroupLinks] AFTER INSERT AS
                         UPDATE MetadataDefinitionGroupLinks SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitionGroupLinks.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[MetadataDefinitionGroupLinks] ENABLE TRIGGER[MetadataDefinitionGroupLinks_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[MetadataDefinitionGroupLinks_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[MetadataDefinitionGroupLinks_UpdateAt] ON [dbo].[MetadataDefinitionGroupLinks] AFTER UPDATE AS
                         UPDATE MetadataDefinitionGroupLinks SET UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitionGroupLinks.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[MetadataDefinitionGroupLinks_UpdateAt] ON [dbo].[MetadataDefinitionGroupLinks] AFTER UPDATE AS
                         UPDATE MetadataDefinitionGroupLinks SET UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitionGroupLinks.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[MetadataDefinitionGroupLinks] ENABLE TRIGGER[MetadataDefinitionGroupLinks_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[MetadataDefinitionGroups_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[MetadataDefinitionGroups_InsertAt] ON [dbo].[MetadataDefinitionGroups] AFTER INSERT AS
                         UPDATE MetadataDefinitionGroups SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitionGroups.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[MetadataDefinitionGroups_InsertAt] ON [dbo].[MetadataDefinitionGroups] AFTER INSERT AS
                         UPDATE MetadataDefinitionGroups SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitionGroups.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[MetadataDefinitionGroups] ENABLE TRIGGER[MetadataDefinitionGroups_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[MetadataDefinitionGroups_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[MetadataDefinitionGroups_UpdateAt] ON [dbo].[MetadataDefinitionGroups] AFTER UPDATE AS
                         UPDATE MetadataDefinitionGroups SET UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitionGroups.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[MetadataDefinitionGroups_UpdateAt] ON [dbo].[MetadataDefinitionGroups] AFTER UPDATE AS
                         UPDATE MetadataDefinitionGroups SET UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitionGroups.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[MetadataDefinitionGroups] ENABLE TRIGGER[MetadataDefinitionGroups_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[MetadataDefinitions_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[MetadataDefinitions_InsertAt] ON [dbo].[MetadataDefinitions] AFTER INSERT AS
                         UPDATE MetadataDefinitions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[MetadataDefinitions_InsertAt] ON [dbo].[MetadataDefinitions] AFTER INSERT AS
                         UPDATE MetadataDefinitions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[MetadataDefinitions] ENABLE TRIGGER[MetadataDefinitions_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[MetadataDefinitions_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[MetadataDefinitions_UpdateAt] ON [dbo].[MetadataDefinitions] AFTER UPDATE AS
                         UPDATE MetadataDefinitions SET UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[MetadataDefinitions_UpdateAt] ON [dbo].[MetadataDefinitions] AFTER UPDATE AS
                         UPDATE MetadataDefinitions SET UpdateAt = getutcdate() FROM INSERTED WHERE MetadataDefinitions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[MetadataDefinitions] ENABLE TRIGGER[MetadataDefinitions_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[OverdraftSettings_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[OverdraftSettings_InsertAt] ON [dbo].[OverdraftSettings] AFTER INSERT AS
                         UPDATE OverdraftSettings SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE OverdraftSettings.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[OverdraftSettings_InsertAt] ON [dbo].[OverdraftSettings] AFTER INSERT AS
                         UPDATE OverdraftSettings SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE OverdraftSettings.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[OverdraftSettings] ENABLE TRIGGER[OverdraftSettings_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[OverdraftSettings_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[OverdraftSettings_UpdateAt] ON [dbo].[OverdraftSettings] AFTER UPDATE AS
                         UPDATE OverdraftSettings SET UpdateAt = getutcdate() FROM INSERTED WHERE OverdraftSettings.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[OverdraftSettings_UpdateAt] ON [dbo].[OverdraftSettings] AFTER UPDATE AS
                         UPDATE OverdraftSettings SET UpdateAt = getutcdate() FROM INSERTED WHERE OverdraftSettings.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[OverdraftSettings] ENABLE TRIGGER[OverdraftSettings_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Payments_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Payments_InsertAt] ON [dbo].[Payments] AFTER INSERT AS
                         UPDATE Payments SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Payments.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Payments_InsertAt] ON [dbo].[Payments] AFTER INSERT AS
                         UPDATE Payments SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Payments.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Payments] ENABLE TRIGGER[Payments_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Payments_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Payments_UpdateAt] ON [dbo].[Payments] AFTER UPDATE AS
                         UPDATE Payments SET UpdateAt = getutcdate() FROM INSERTED WHERE Payments.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Payments_UpdateAt] ON [dbo].[Payments] AFTER UPDATE AS
                         UPDATE Payments SET UpdateAt = getutcdate() FROM INSERTED WHERE Payments.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Payments] ENABLE TRIGGER[Payments_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Persons_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Persons_InsertAt] ON [dbo].[Persons] AFTER INSERT AS
                         UPDATE Persons SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Persons.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Persons_InsertAt] ON [dbo].[Persons] AFTER INSERT AS
                         UPDATE Persons SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Persons.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Persons] ENABLE TRIGGER[Persons_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Persons_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Persons_UpdateAt] ON [dbo].[Persons] AFTER UPDATE AS
                         UPDATE Persons SET UpdateAt = getutcdate() FROM INSERTED WHERE Persons.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Persons_UpdateAt] ON [dbo].[Persons] AFTER UPDATE AS
                         UPDATE Persons SET UpdateAt = getutcdate() FROM INSERTED WHERE Persons.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Persons] ENABLE TRIGGER[Persons_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[PhoneNumbers_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[PhoneNumbers_InsertAt] ON [dbo].[PhoneNumbers] AFTER INSERT AS
                         UPDATE PhoneNumbers SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE PhoneNumbers.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[PhoneNumbers_InsertAt] ON [dbo].[PhoneNumbers] AFTER INSERT AS
                         UPDATE PhoneNumbers SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE PhoneNumbers.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[PhoneNumbers] ENABLE TRIGGER[PhoneNumbers_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[PhoneNumbers_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[PhoneNumbers_UpdateAt] ON [dbo].[PhoneNumbers] AFTER UPDATE AS
                         UPDATE PhoneNumbers SET UpdateAt = getutcdate() FROM INSERTED WHERE PhoneNumbers.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[PhoneNumbers_UpdateAt] ON [dbo].[PhoneNumbers] AFTER UPDATE AS
                         UPDATE PhoneNumbers SET UpdateAt = getutcdate() FROM INSERTED WHERE PhoneNumbers.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[PhoneNumbers] ENABLE TRIGGER[PhoneNumbers_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Portfolios_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Portfolios_InsertAt] ON [dbo].[Portfolios] AFTER INSERT AS
                         UPDATE Portfolios SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Portfolios.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Portfolios_InsertAt] ON [dbo].[Portfolios] AFTER INSERT AS
                         UPDATE Portfolios SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Portfolios.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Portfolios] ENABLE TRIGGER[Portfolios_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Portfolios_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Portfolios_UpdateAt] ON [dbo].[Portfolios] AFTER UPDATE AS
                         UPDATE Portfolios SET UpdateAt = getutcdate() FROM INSERTED WHERE Portfolios.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Portfolios_UpdateAt] ON [dbo].[Portfolios] AFTER UPDATE AS
                         UPDATE Portfolios SET UpdateAt = getutcdate() FROM INSERTED WHERE Portfolios.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Portfolios] ENABLE TRIGGER[Portfolios_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ProductDefinitions_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ProductDefinitions_InsertAt] ON [dbo].[ProductDefinitions] AFTER INSERT AS
                         UPDATE ProductDefinitions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ProductDefinitions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ProductDefinitions_InsertAt] ON [dbo].[ProductDefinitions] AFTER INSERT AS
                         UPDATE ProductDefinitions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ProductDefinitions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ProductDefinitions] ENABLE TRIGGER[ProductDefinitions_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ProductDefinitions_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ProductDefinitions_UpdateAt] ON [dbo].[ProductDefinitions] AFTER UPDATE AS
                         UPDATE ProductDefinitions SET UpdateAt = getutcdate() FROM INSERTED WHERE ProductDefinitions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ProductDefinitions_UpdateAt] ON [dbo].[ProductDefinitions] AFTER UPDATE AS
                         UPDATE ProductDefinitions SET UpdateAt = getutcdate() FROM INSERTED WHERE ProductDefinitions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ProductDefinitions] ENABLE TRIGGER[ProductDefinitions_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ProductFeatureContextLinks_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ProductFeatureContextLinks_InsertAt] ON [dbo].[ProductFeatureContextLinks] AFTER INSERT AS
                         UPDATE ProductFeatureContextLinks SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ProductFeatureContextLinks.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ProductFeatureContextLinks_InsertAt] ON [dbo].[ProductFeatureContextLinks] AFTER INSERT AS
                         UPDATE ProductFeatureContextLinks SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ProductFeatureContextLinks.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ProductFeatureContextLinks] ENABLE TRIGGER[ProductFeatureContextLinks_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[ProductFeatureContextLinks_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ProductFeatureContextLinks_UpdateAt] ON [dbo].[ProductFeatureContextLinks] AFTER UPDATE AS
                         UPDATE ProductFeatureContextLinks SET UpdateAt = getutcdate() FROM INSERTED WHERE ProductFeatureContextLinks.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ProductFeatureContextLinks_UpdateAt] ON [dbo].[ProductFeatureContextLinks] AFTER UPDATE AS
                         UPDATE ProductFeatureContextLinks SET UpdateAt = getutcdate() FROM INSERTED WHERE ProductFeatureContextLinks.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[ProductFeatureContextLinks] ENABLE TRIGGER[ProductFeatureContextLinks_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SecureMessageAttachments_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SecureMessageAttachments_InsertAt] ON [dbo].[SecureMessageAttachments] AFTER INSERT AS
                         UPDATE SecureMessageAttachments SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessageAttachments.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SecureMessageAttachments_InsertAt] ON [dbo].[SecureMessageAttachments] AFTER INSERT AS
                         UPDATE SecureMessageAttachments SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessageAttachments.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SecureMessageAttachments] ENABLE TRIGGER[SecureMessageAttachments_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SecureMessageAttachments_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SecureMessageAttachments_UpdateAt] ON [dbo].[SecureMessageAttachments] AFTER UPDATE AS
                         UPDATE SecureMessageAttachments SET UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessageAttachments.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SecureMessageAttachments_UpdateAt] ON [dbo].[SecureMessageAttachments] AFTER UPDATE AS
                         UPDATE SecureMessageAttachments SET UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessageAttachments.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SecureMessageAttachments] ENABLE TRIGGER[SecureMessageAttachments_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SecureMessages_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SecureMessages_InsertAt] ON [dbo].[SecureMessages] AFTER INSERT AS
                         UPDATE SecureMessages SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessages.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SecureMessages_InsertAt] ON [dbo].[SecureMessages] AFTER INSERT AS
                         UPDATE SecureMessages SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessages.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SecureMessages] ENABLE TRIGGER[SecureMessages_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SecureMessages_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SecureMessages_UpdateAt] ON [dbo].[SecureMessages] AFTER UPDATE AS
                         UPDATE SecureMessages SET UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessages.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SecureMessages_UpdateAt] ON [dbo].[SecureMessages] AFTER UPDATE AS
                         UPDATE SecureMessages SET UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessages.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SecureMessages] ENABLE TRIGGER[SecureMessages_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SecureMessageTemplates_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SecureMessageTemplates_InsertAt] ON [dbo].[SecureMessageTemplates] AFTER INSERT AS
                         UPDATE SecureMessageTemplates SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessageTemplates.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SecureMessageTemplates_InsertAt] ON [dbo].[SecureMessageTemplates] AFTER INSERT AS
                         UPDATE SecureMessageTemplates SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessageTemplates.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SecureMessageTemplates] ENABLE TRIGGER[SecureMessageTemplates_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SecureMessageTemplates_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SecureMessageTemplates_UpdateAt] ON [dbo].[SecureMessageTemplates] AFTER UPDATE AS
                         UPDATE SecureMessageTemplates SET UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessageTemplates.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SecureMessageTemplates_UpdateAt] ON [dbo].[SecureMessageTemplates] AFTER UPDATE AS
                         UPDATE SecureMessageTemplates SET UpdateAt = getutcdate() FROM INSERTED WHERE SecureMessageTemplates.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SecureMessageTemplates] ENABLE TRIGGER[SecureMessageTemplates_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Sessions_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Sessions_InsertAt] ON [dbo].[Sessions] AFTER INSERT AS
                         UPDATE Sessions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Sessions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Sessions_InsertAt] ON [dbo].[Sessions] AFTER INSERT AS
                         UPDATE Sessions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Sessions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Sessions] ENABLE TRIGGER[Sessions_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Sessions_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Sessions_UpdateAt] ON [dbo].[Sessions] AFTER UPDATE AS
                         UPDATE Sessions SET UpdateAt = getutcdate() FROM INSERTED WHERE Sessions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Sessions_UpdateAt] ON [dbo].[Sessions] AFTER UPDATE AS
                         UPDATE Sessions SET UpdateAt = getutcdate() FROM INSERTED WHERE Sessions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Sessions] ENABLE TRIGGER[Sessions_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Sources_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Sources_InsertAt] ON [dbo].[Sources] AFTER INSERT AS
                         UPDATE Sources SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Sources.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Sources_InsertAt] ON [dbo].[Sources] AFTER INSERT AS
                         UPDATE Sources SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Sources.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Sources] ENABLE TRIGGER[Sources_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Sources_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Sources_UpdateAt] ON [dbo].[Sources] AFTER UPDATE AS
                         UPDATE Sources SET UpdateAt = getutcdate() FROM INSERTED WHERE Sources.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Sources_UpdateAt] ON [dbo].[Sources] AFTER UPDATE AS
                         UPDATE Sources SET UpdateAt = getutcdate() FROM INSERTED WHERE Sources.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Sources] ENABLE TRIGGER[Sources_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SubAccountMetadata_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SubAccountMetadata_InsertAt] ON [dbo].[SubAccountMetadata] AFTER INSERT AS
                         UPDATE SubAccountMetadata SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SubAccountMetadata.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SubAccountMetadata_InsertAt] ON [dbo].[SubAccountMetadata] AFTER INSERT AS
                         UPDATE SubAccountMetadata SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SubAccountMetadata.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SubAccountMetadata] ENABLE TRIGGER[SubAccountMetadata_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SubAccountMetadata_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SubAccountMetadata_UpdateAt] ON [dbo].[SubAccountMetadata] AFTER UPDATE AS
                         UPDATE SubAccountMetadata SET UpdateAt = getutcdate() FROM INSERTED WHERE SubAccountMetadata.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SubAccountMetadata_UpdateAt] ON [dbo].[SubAccountMetadata] AFTER UPDATE AS
                         UPDATE SubAccountMetadata SET UpdateAt = getutcdate() FROM INSERTED WHERE SubAccountMetadata.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SubAccountMetadata] ENABLE TRIGGER[SubAccountMetadata_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SubAccounts_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SubAccounts_InsertAt] ON [dbo].[SubAccounts] AFTER INSERT AS
                         UPDATE SubAccounts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SubAccounts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SubAccounts_InsertAt] ON [dbo].[SubAccounts] AFTER INSERT AS
                         UPDATE SubAccounts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SubAccounts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SubAccounts] ENABLE TRIGGER[SubAccounts_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SubAccounts_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SubAccounts_UpdateAt] ON [dbo].[SubAccounts] AFTER UPDATE AS
                         UPDATE SubAccounts SET UpdateAt = getutcdate() FROM INSERTED WHERE SubAccounts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SubAccounts_UpdateAt] ON [dbo].[SubAccounts] AFTER UPDATE AS
                         UPDATE SubAccounts SET UpdateAt = getutcdate() FROM INSERTED WHERE SubAccounts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SubAccounts] ENABLE TRIGGER[SubAccounts_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SyncStatuses_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SyncStatuses_InsertAt] ON [dbo].[SyncStatuses] AFTER INSERT AS
                         UPDATE SyncStatuses SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SyncStatuses.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SyncStatuses_InsertAt] ON [dbo].[SyncStatuses] AFTER INSERT AS
                         UPDATE SyncStatuses SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE SyncStatuses.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SyncStatuses] ENABLE TRIGGER[SyncStatuses_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[SyncStatuses_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[SyncStatuses_UpdateAt] ON [dbo].[SyncStatuses] AFTER UPDATE AS
                         UPDATE SyncStatuses SET UpdateAt = getutcdate() FROM INSERTED WHERE SyncStatuses.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[SyncStatuses_UpdateAt] ON [dbo].[SyncStatuses] AFTER UPDATE AS
                         UPDATE SyncStatuses SET UpdateAt = getutcdate() FROM INSERTED WHERE SyncStatuses.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[SyncStatuses] ENABLE TRIGGER[SyncStatuses_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[TransactionCheckImages_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[TransactionCheckImages_InsertAt] ON [dbo].[TransactionCheckImages] AFTER INSERT AS
                         UPDATE TransactionCheckImages SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE TransactionCheckImages.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[TransactionCheckImages_InsertAt] ON [dbo].[TransactionCheckImages] AFTER INSERT AS
                         UPDATE TransactionCheckImages SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE TransactionCheckImages.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[TransactionCheckImages] ENABLE TRIGGER[TransactionCheckImages_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[TransactionCheckImages_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[TransactionCheckImages_UpdateAt] ON [dbo].[TransactionCheckImages] AFTER UPDATE AS
                         UPDATE TransactionCheckImages SET UpdateAt = getutcdate() FROM INSERTED WHERE TransactionCheckImages.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[TransactionCheckImages_UpdateAt] ON [dbo].[TransactionCheckImages] AFTER UPDATE AS
                         UPDATE TransactionCheckImages SET UpdateAt = getutcdate() FROM INSERTED WHERE TransactionCheckImages.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[TransactionCheckImages] ENABLE TRIGGER[TransactionCheckImages_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[TransactionChecks_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[TransactionChecks_InsertAt] ON [dbo].[TransactionChecks] AFTER INSERT AS
                         UPDATE TransactionChecks SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE TransactionChecks.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[TransactionChecks_InsertAt] ON [dbo].[TransactionChecks] AFTER INSERT AS
                         UPDATE TransactionChecks SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE TransactionChecks.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[TransactionChecks] ENABLE TRIGGER[TransactionChecks_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[TransactionChecks_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[TransactionChecks_UpdateAt] ON [dbo].[TransactionChecks] AFTER UPDATE AS
                         UPDATE TransactionChecks SET UpdateAt = getutcdate() FROM INSERTED WHERE TransactionChecks.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[TransactionChecks_UpdateAt] ON [dbo].[TransactionChecks] AFTER UPDATE AS
                         UPDATE TransactionChecks SET UpdateAt = getutcdate() FROM INSERTED WHERE TransactionChecks.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[TransactionChecks] ENABLE TRIGGER[TransactionChecks_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Transactions_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Transactions_InsertAt] ON [dbo].[Transactions] AFTER INSERT AS
                         UPDATE Transactions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Transactions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Transactions_InsertAt] ON [dbo].[Transactions] AFTER INSERT AS
                         UPDATE Transactions SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Transactions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Transactions] ENABLE TRIGGER[Transactions_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Transactions_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Transactions_UpdateAt] ON [dbo].[Transactions] AFTER UPDATE AS
                         UPDATE Transactions SET UpdateAt = getutcdate() FROM INSERTED WHERE Transactions.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Transactions_UpdateAt] ON [dbo].[Transactions] AFTER UPDATE AS
                         UPDATE Transactions SET UpdateAt = getutcdate() FROM INSERTED WHERE Transactions.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Transactions] ENABLE TRIGGER[Transactions_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[TransferRecipients_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[TransferRecipients_InsertAt] ON [dbo].[TransferRecipients] AFTER INSERT AS
                         UPDATE TransferRecipients SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE TransferRecipients.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[TransferRecipients_InsertAt] ON [dbo].[TransferRecipients] AFTER INSERT AS
                         UPDATE TransferRecipients SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE TransferRecipients.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[TransferRecipients] ENABLE TRIGGER[TransferRecipients_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[TransferRecipients_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[TransferRecipients_UpdateAt] ON [dbo].[TransferRecipients] AFTER UPDATE AS
                         UPDATE TransferRecipients SET UpdateAt = getutcdate() FROM INSERTED WHERE TransferRecipients.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[TransferRecipients_UpdateAt] ON [dbo].[TransferRecipients] AFTER UPDATE AS
                         UPDATE TransferRecipients SET UpdateAt = getutcdate() FROM INSERTED WHERE TransferRecipients.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[TransferRecipients] ENABLE TRIGGER[TransferRecipients_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Transfers_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Transfers_InsertAt] ON [dbo].[Transfers] AFTER INSERT AS
                         UPDATE Transfers SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Transfers.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Transfers_InsertAt] ON [dbo].[Transfers] AFTER INSERT AS
                         UPDATE Transfers SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE Transfers.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Transfers] ENABLE TRIGGER[Transfers_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[Transfers_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[Transfers_UpdateAt] ON [dbo].[Transfers] AFTER UPDATE AS
                         UPDATE Transfers SET UpdateAt = getutcdate() FROM INSERTED WHERE Transfers.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[Transfers_UpdateAt] ON [dbo].[Transfers] AFTER UPDATE AS
                         UPDATE Transfers SET UpdateAt = getutcdate() FROM INSERTED WHERE Transfers.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[Transfers] ENABLE TRIGGER[Transfers_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[UserSubAccountSettings_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[UserSubAccountSettings_InsertAt] ON [dbo].[UserSubAccountSettings] AFTER INSERT AS
                         UPDATE UserSubAccountSettings SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE UserSubAccountSettings.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[UserSubAccountSettings_InsertAt] ON [dbo].[UserSubAccountSettings] AFTER INSERT AS
                         UPDATE UserSubAccountSettings SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE UserSubAccountSettings.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[UserSubAccountSettings] ENABLE TRIGGER[UserSubAccountSettings_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[UserSubAccountSettings_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[UserSubAccountSettings_UpdateAt] ON [dbo].[UserSubAccountSettings] AFTER UPDATE AS
                         UPDATE UserSubAccountSettings SET UpdateAt = getutcdate() FROM INSERTED WHERE UserSubAccountSettings.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[UserSubAccountSettings_UpdateAt] ON [dbo].[UserSubAccountSettings] AFTER UPDATE AS
                         UPDATE UserSubAccountSettings SET UpdateAt = getutcdate() FROM INSERTED WHERE UserSubAccountSettings.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[UserSubAccountSettings] ENABLE TRIGGER[UserSubAccountSettings_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[UserVendorData_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[UserVendorData_InsertAt] ON [dbo].[UserVendorData] AFTER INSERT AS
                         UPDATE UserVendorData SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE UserVendorData.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[UserVendorData_InsertAt] ON [dbo].[UserVendorData] AFTER INSERT AS
                         UPDATE UserVendorData SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE UserVendorData.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[UserVendorData] ENABLE TRIGGER[UserVendorData_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[UserVendorData_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[UserVendorData_UpdateAt] ON [dbo].[UserVendorData] AFTER UPDATE AS
                         UPDATE UserVendorData SET UpdateAt = getutcdate() FROM INSERTED WHERE UserVendorData.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[UserVendorData_UpdateAt] ON [dbo].[UserVendorData] AFTER UPDATE AS
                         UPDATE UserVendorData SET UpdateAt = getutcdate() FROM INSERTED WHERE UserVendorData.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[UserVendorData] ENABLE TRIGGER[UserVendorData_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[VendorIntegrations_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[VendorIntegrations_InsertAt] ON [dbo].[VendorIntegrations] AFTER INSERT AS
                         UPDATE VendorIntegrations SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE VendorIntegrations.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[VendorIntegrations_InsertAt] ON [dbo].[VendorIntegrations] AFTER INSERT AS
                         UPDATE VendorIntegrations SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE VendorIntegrations.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[VendorIntegrations] ENABLE TRIGGER[VendorIntegrations_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    IF OBJECT_ID(N'[dbo].[VendorIntegrations_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[VendorIntegrations_UpdateAt] ON [dbo].[VendorIntegrations] AFTER UPDATE AS
                         UPDATE VendorIntegrations SET UpdateAt = getutcdate() FROM INSERTED WHERE VendorIntegrations.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[VendorIntegrations_UpdateAt] ON [dbo].[VendorIntegrations] AFTER UPDATE AS
                         UPDATE VendorIntegrations SET UpdateAt = getutcdate() FROM INSERTED WHERE VendorIntegrations.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    ALTER TABLE[dbo].[VendorIntegrations] ENABLE TRIGGER[VendorIntegrations_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123228_PersonCentricSchema')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200520123228_PersonCentricSchema', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123229_AddDirectionColumnToStoreForExternalTransfers')
BEGIN
    ALTER TABLE [ExternalTransfers] ADD [Direction] int NOT NULL DEFAULT 0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200520123229_AddDirectionColumnToStoreForExternalTransfers')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200520123229_AddDirectionColumnToStoreForExternalTransfers', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200527153046_DeviceLastLoginFields')
BEGIN
    ALTER TABLE [Devices] ADD [LastIpAddress] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200527153046_DeviceLastLoginFields')
BEGIN
    ALTER TABLE [Devices] ADD [LastLogin] datetime2 NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200527153046_DeviceLastLoginFields')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200527153046_DeviceLastLoginFields', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200612154340_AddExternalIdentifiersColumnToDigitalRole')
BEGIN
    ALTER TABLE [DigitalRoles] ADD [ExternalIdentifiers] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200612154340_AddExternalIdentifiersColumnToDigitalRole')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200612154340_AddExternalIdentifiersColumnToDigitalRole', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200615111025_AddIsDraftColumnToSecureMessage')
BEGIN
    ALTER TABLE [SecureMessages] ADD [IsDraft] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200615111025_AddIsDraftColumnToSecureMessage')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200615111025_AddIsDraftColumnToSecureMessage', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200619101520_AddIsTemporaryPasswordToDigitalUser')
BEGIN
    ALTER TABLE [DigitalUsers] ADD [IsTemporaryPassword] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200619101520_AddIsTemporaryPasswordToDigitalUser')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200619101520_AddIsTemporaryPasswordToDigitalUser', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200623101312_ChangeDepositAccountForeignKey')
BEGIN
    ALTER TABLE [Deposits] DROP CONSTRAINT [FK_Deposits_Accounts_AccountId];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200623101312_ChangeDepositAccountForeignKey')
BEGIN
    EXEC sp_rename N'[Deposits].[AccountId]', N'PortfolioId', N'COLUMN';
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200623101312_ChangeDepositAccountForeignKey')
BEGIN
    EXEC sp_rename N'[Deposits].[IX_Deposits_AccountId]', N'IX_Deposits_PortfolioId', N'INDEX';
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200623101312_ChangeDepositAccountForeignKey')
BEGIN
    ALTER TABLE [Deposits] ADD CONSTRAINT [FK_Deposits_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200623101312_ChangeDepositAccountForeignKey')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200623101312_ChangeDepositAccountForeignKey', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200626154343_AlterBillPayPaymentFrequencyColumn')
BEGIN
    DECLARE @var0 sysname;
    SELECT @var0 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BillPayPayments]') AND [c].[name] = N'FrequencyCode');
    IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [BillPayPayments] DROP CONSTRAINT [' + @var0 + '];');
    ALTER TABLE [BillPayPayments] DROP COLUMN [FrequencyCode];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200626154343_AlterBillPayPaymentFrequencyColumn')
BEGIN
    ALTER TABLE [BillPayPayments] ADD [Frequency] int NOT NULL DEFAULT 0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200626154343_AlterBillPayPaymentFrequencyColumn')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200626154343_AlterBillPayPaymentFrequencyColumn', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709130200_AddMemoColumnToHold')
BEGIN
    ALTER TABLE [Holds] ADD [Memo] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709130200_AddMemoColumnToHold')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200709130200_AddMemoColumnToHold', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709191627_AddTextConfigurationsTable')
BEGIN
    CREATE TABLE [TextConfigurations] (
        [Id] bigint NOT NULL IDENTITY,
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [IsActive] bit NOT NULL,
        [File] varbinary(max) NULL,
        [EffectiveDate] datetime2 NOT NULL,
        [VersionTag] nvarchar(450) NULL,
        [Locale] nvarchar(max) NULL,
        CONSTRAINT [PK_TextConfigurations] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709191627_AddTextConfigurationsTable')
BEGIN
    CREATE INDEX [IX_TextConfigurations_VersionTag] ON [TextConfigurations] ([VersionTag]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709191627_AddTextConfigurationsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200709191627_AddTextConfigurationsTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709203239_AddMissingColumnsTextConfigurations')
BEGIN
    ALTER TABLE [TextConfigurations] ADD [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate());
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709203239_AddMissingColumnsTextConfigurations')
BEGIN
    ALTER TABLE [TextConfigurations] ADD [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate());
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709203239_AddMissingColumnsTextConfigurations')
BEGIN
    IF OBJECT_ID(N'[dbo].[TextConfigurations_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[TextConfigurations_InsertAt] ON [dbo].[TextConfigurations] AFTER INSERT AS
                         UPDATE TextConfigurations SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE TextConfigurations.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[TextConfigurations_InsertAt] ON [dbo].[TextConfigurations] AFTER INSERT AS
                         UPDATE TextConfigurations SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE TextConfigurations.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709203239_AddMissingColumnsTextConfigurations')
BEGIN
    ALTER TABLE[dbo].[TextConfigurations] ENABLE TRIGGER[TextConfigurations_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709203239_AddMissingColumnsTextConfigurations')
BEGIN
    IF OBJECT_ID(N'[dbo].[TextConfigurations_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[TextConfigurations_UpdateAt] ON [dbo].[TextConfigurations] AFTER UPDATE AS
                         UPDATE TextConfigurations SET UpdateAt = getutcdate() FROM INSERTED WHERE TextConfigurations.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[TextConfigurations_UpdateAt] ON [dbo].[TextConfigurations] AFTER UPDATE AS
                         UPDATE TextConfigurations SET UpdateAt = getutcdate() FROM INSERTED WHERE TextConfigurations.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709203239_AddMissingColumnsTextConfigurations')
BEGIN
    ALTER TABLE[dbo].[TextConfigurations] ENABLE TRIGGER[TextConfigurations_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200709203239_AddMissingColumnsTextConfigurations')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200709203239_AddMissingColumnsTextConfigurations', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200710183657_AddNoteForTextConfigurations')
BEGIN
    ALTER TABLE [TextConfigurations] ADD [Note] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200710183657_AddNoteForTextConfigurations')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200710183657_AddNoteForTextConfigurations', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200713044816_AddedSoftDeleteToBillPayPayee')
BEGIN
    ALTER TABLE [BillPayPayees] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200713044816_AddedSoftDeleteToBillPayPayee')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200713044816_AddedSoftDeleteToBillPayPayee', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200721190313_AddThirdPartyApiSchema')
BEGIN
    CREATE TABLE [ApiClientTokens] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ClientId] nvarchar(450) NULL,
        [ClientName] nvarchar(max) NULL,
        [Secret] nvarchar(max) NULL,
        CONSTRAINT [PK_ApiClientTokens] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200721190313_AddThirdPartyApiSchema')
BEGIN
    CREATE TABLE [ApiHookRequests] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [RequestId] nvarchar(450) NULL,
        [ResponseId] nvarchar(450) NULL,
        [IsReplay] bit NOT NULL,
        [Success] bit NOT NULL,
        [RequestBody] nvarchar(max) NULL,
        [ResponseBody] nvarchar(max) NULL,
        CONSTRAINT [PK_ApiHookRequests] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200721190313_AddThirdPartyApiSchema')
BEGIN
    CREATE INDEX [IX_ApiClientTokens_ClientId] ON [ApiClientTokens] ([ClientId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200721190313_AddThirdPartyApiSchema')
BEGIN
    CREATE INDEX [IX_ApiHookRequests_RequestId] ON [ApiHookRequests] ([RequestId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200721190313_AddThirdPartyApiSchema')
BEGIN
    CREATE INDEX [IX_ApiHookRequests_ResponseId] ON [ApiHookRequests] ([ResponseId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200721190313_AddThirdPartyApiSchema')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200721190313_AddThirdPartyApiSchema', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723124701_AddThirdPartyAccountClassification')
BEGIN
    ALTER TABLE [SubAccounts] ADD [IsThirdPartyAccount] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723124701_AddThirdPartyAccountClassification')
BEGIN
    ALTER TABLE [SubAccounts] ADD [ThirdPartyClassification] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723124701_AddThirdPartyAccountClassification')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200723124701_AddThirdPartyAccountClassification', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    ALTER TABLE [ApiHookRequests] ADD [SessionCorrelationId] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    IF OBJECT_ID(N'[dbo].[ApiHookRequests_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ApiHookRequests_InsertAt] ON [dbo].[ApiHookRequests] AFTER INSERT AS
                         UPDATE ApiHookRequests SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ApiHookRequests.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ApiHookRequests_InsertAt] ON [dbo].[ApiHookRequests] AFTER INSERT AS
                         UPDATE ApiHookRequests SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ApiHookRequests.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    ALTER TABLE[dbo].[ApiHookRequests] ENABLE TRIGGER[ApiHookRequests_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    IF OBJECT_ID(N'[dbo].[ApiHookRequests_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ApiHookRequests_UpdateAt] ON [dbo].[ApiHookRequests] AFTER UPDATE AS
                         UPDATE ApiHookRequests SET UpdateAt = getutcdate() FROM INSERTED WHERE ApiHookRequests.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ApiHookRequests_UpdateAt] ON [dbo].[ApiHookRequests] AFTER UPDATE AS
                         UPDATE ApiHookRequests SET UpdateAt = getutcdate() FROM INSERTED WHERE ApiHookRequests.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    ALTER TABLE[dbo].[ApiHookRequests] ENABLE TRIGGER[ApiHookRequests_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    IF OBJECT_ID(N'[dbo].[ApiClientTokens_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ApiClientTokens_InsertAt] ON [dbo].[ApiClientTokens] AFTER INSERT AS
                         UPDATE ApiClientTokens SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ApiClientTokens.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ApiClientTokens_InsertAt] ON [dbo].[ApiClientTokens] AFTER INSERT AS
                         UPDATE ApiClientTokens SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE ApiClientTokens.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    ALTER TABLE[dbo].[ApiClientTokens] ENABLE TRIGGER[ApiClientTokens_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    IF OBJECT_ID(N'[dbo].[ApiClientTokens_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[ApiClientTokens_UpdateAt] ON [dbo].[ApiClientTokens] AFTER UPDATE AS
                         UPDATE ApiClientTokens SET UpdateAt = getutcdate() FROM INSERTED WHERE ApiClientTokens.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[ApiClientTokens_UpdateAt] ON [dbo].[ApiClientTokens] AFTER UPDATE AS
                         UPDATE ApiClientTokens SET UpdateAt = getutcdate() FROM INSERTED WHERE ApiClientTokens.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    ALTER TABLE[dbo].[ApiClientTokens] ENABLE TRIGGER[ApiClientTokens_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200723171850_AddSessionMapFieldToHookRequestTracking')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200723171850_AddSessionMapFieldToHookRequestTracking', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    ALTER TABLE [MetadataDefinitionGroupLinks] ADD [MetadataContextId] bigint NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    CREATE TABLE [MetadataContexts] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        [ModelTypeFullName] nvarchar(max) NULL,
        [ModelTypeName] nvarchar(max) NULL,
        CONSTRAINT [PK_MetadataContexts] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    CREATE INDEX [IX_MetadataDefinitionGroupLinks_MetadataContextId] ON [MetadataDefinitionGroupLinks] ([MetadataContextId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    ALTER TABLE [MetadataDefinitionGroupLinks] ADD CONSTRAINT [FK_MetadataDefinitionGroupLinks_MetadataContexts_MetadataContextId] FOREIGN KEY ([MetadataContextId]) REFERENCES [MetadataContexts] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    IF OBJECT_ID(N'[dbo].[MetadataContexts_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[MetadataContexts_InsertAt] ON [dbo].[MetadataContexts] AFTER INSERT AS
                         UPDATE MetadataContexts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE MetadataContexts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[MetadataContexts_InsertAt] ON [dbo].[MetadataContexts] AFTER INSERT AS
                         UPDATE MetadataContexts SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE MetadataContexts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    ALTER TABLE[dbo].[MetadataContexts] ENABLE TRIGGER[MetadataContexts_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    IF OBJECT_ID(N'[dbo].[MetadataContexts_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[MetadataContexts_UpdateAt] ON [dbo].[MetadataContexts] AFTER UPDATE AS
                         UPDATE MetadataContexts SET UpdateAt = getutcdate() FROM INSERTED WHERE MetadataContexts.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[MetadataContexts_UpdateAt] ON [dbo].[MetadataContexts] AFTER UPDATE AS
                         UPDATE MetadataContexts SET UpdateAt = getutcdate() FROM INSERTED WHERE MetadataContexts.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    ALTER TABLE[dbo].[MetadataContexts] ENABLE TRIGGER[MetadataContexts_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200727213245_AddMetadataContextEntity')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200727213245_AddMetadataContextEntity', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200731145333_AddFrequencyNameAndSuccessfulColumnToExternalTransfer')
BEGIN
    ALTER TABLE [ExternalTransfers] ADD [FrequencyName] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200731145333_AddFrequencyNameAndSuccessfulColumnToExternalTransfer')
BEGIN
    ALTER TABLE [ExternalTransfers] ADD [Successful] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200731145333_AddFrequencyNameAndSuccessfulColumnToExternalTransfer')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200731145333_AddFrequencyNameAndSuccessfulColumnToExternalTransfer', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200804112015_AddedProcessingDaystoBillPayPayee')
BEGIN
    ALTER TABLE [BillPayPayees] ADD [ProcessingDays] int NOT NULL DEFAULT 0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200804112015_AddedProcessingDaystoBillPayPayee')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200804112015_AddedProcessingDaystoBillPayPayee', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200804150248_AddVisibleFlagToExternalAccounts')
BEGIN
    ALTER TABLE [ExternalAccounts] ADD [UserVisible] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200804150248_AddVisibleFlagToExternalAccounts')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200804150248_AddVisibleFlagToExternalAccounts', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200804190037_AddTypeDetailsForMetadataGroups')
BEGIN
    ALTER TABLE [MetadataDefinitionGroups] ADD [FullTypeName] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200804190037_AddTypeDetailsForMetadataGroups')
BEGIN
    ALTER TABLE [MetadataDefinitionGroups] ADD [Type] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200804190037_AddTypeDetailsForMetadataGroups')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200804190037_AddTypeDetailsForMetadataGroups', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200805121629_AddVendorIntegrationLoggingToggle')
BEGIN
    ALTER TABLE [VendorIntegrations] ADD [LoggingEnabled] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200805121629_AddVendorIntegrationLoggingToggle')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200805121629_AddVendorIntegrationLoggingToggle', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    ALTER TABLE [AccountDocuments] DROP CONSTRAINT [FK_AccountDocuments_Accounts_AccountId];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    DECLARE @var1 sysname;
    SELECT @var1 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[AccountDocuments]') AND [c].[name] = N'AccountId');
    IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [AccountDocuments] DROP CONSTRAINT [' + @var1 + '];');
    ALTER TABLE [AccountDocuments] ALTER COLUMN [AccountId] bigint NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    ALTER TABLE [AccountDocuments] ADD [PortfolioId] bigint NOT NULL DEFAULT CAST(0 AS bigint);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    ALTER TABLE [AccountDocuments] ADD [SubAccountId] bigint NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    CREATE INDEX [IX_AccountDocuments_PortfolioId] ON [AccountDocuments] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    CREATE INDEX [IX_AccountDocuments_SubAccountId] ON [AccountDocuments] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    ALTER TABLE [AccountDocuments] ADD CONSTRAINT [FK_AccountDocuments_Accounts_AccountId] FOREIGN KEY ([AccountId]) REFERENCES [Accounts] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    ALTER TABLE [AccountDocuments] ADD CONSTRAINT [FK_AccountDocuments_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    ALTER TABLE [AccountDocuments] ADD CONSTRAINT [FK_AccountDocuments_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200813180856_AddPortFolioToAccountDocuments')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200813180856_AddPortFolioToAccountDocuments', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200902191320_AddNachaFilesTable')
BEGIN
    CREATE TABLE [NachaFiles] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [FileName] nvarchar(max) NULL,
        [TransferCount] int NOT NULL,
        [DebitTotalAmount] decimal(18,2) NOT NULL,
        [CreditTotalAmount] decimal(18,2) NOT NULL,
        [Type] nvarchar(max) NOT NULL,
        [File] varbinary(max) NULL,
        [IsDeleted] bit NOT NULL,
        CONSTRAINT [PK_NachaFiles] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200902191320_AddNachaFilesTable')
BEGIN
    IF OBJECT_ID(N'[dbo].[NachaFiles_InsertAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[NachaFiles_InsertAt] ON [dbo].[NachaFiles] AFTER INSERT AS
                         UPDATE NachaFiles SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE NachaFiles.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[NachaFiles_InsertAt] ON [dbo].[NachaFiles] AFTER INSERT AS
                         UPDATE NachaFiles SET InsertAt = getutcdate(), UpdateAt = getutcdate() FROM INSERTED WHERE NachaFiles.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200902191320_AddNachaFilesTable')
BEGIN
    ALTER TABLE[dbo].[NachaFiles] ENABLE TRIGGER[NachaFiles_InsertAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200902191320_AddNachaFilesTable')
BEGIN
    IF OBJECT_ID(N'[dbo].[NachaFiles_UpdateAt]', N'TR') IS NOT NULL
                        EXEC('
                         ALTER TRIGGER[dbo].[NachaFiles_UpdateAt] ON [dbo].[NachaFiles] AFTER UPDATE AS
                         UPDATE NachaFiles SET UpdateAt = getutcdate() FROM INSERTED WHERE NachaFiles.Id = INSERTED.Id
                        ');
                      ELSE
                        EXEC('
                         CREATE TRIGGER[dbo].[NachaFiles_UpdateAt] ON [dbo].[NachaFiles] AFTER UPDATE AS
                         UPDATE NachaFiles SET UpdateAt = getutcdate() FROM INSERTED WHERE NachaFiles.Id = INSERTED.Id
                        ');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200902191320_AddNachaFilesTable')
BEGIN
    ALTER TABLE[dbo].[NachaFiles] ENABLE TRIGGER[NachaFiles_UpdateAt]
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200902191320_AddNachaFilesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200902191320_AddNachaFilesTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903150128_UpdateMetadataContextToUseEnumName')
BEGIN
    ALTER TABLE [SubAccountMetadata] DROP CONSTRAINT [FK_SubAccountMetadata_FeatureProductContexts_FeatureContextId];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903150128_UpdateMetadataContextToUseEnumName')
BEGIN
    DROP INDEX [IX_SubAccountMetadata_FeatureContextId] ON [SubAccountMetadata];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903150128_UpdateMetadataContextToUseEnumName')
BEGIN
    DECLARE @var2 sysname;
    SELECT @var2 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[SubAccountMetadata]') AND [c].[name] = N'FeatureContextId');
    IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [SubAccountMetadata] DROP CONSTRAINT [' + @var2 + '];');
    ALTER TABLE [SubAccountMetadata] DROP COLUMN [FeatureContextId];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903150128_UpdateMetadataContextToUseEnumName')
BEGIN
    DECLARE @var3 sysname;
    SELECT @var3 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[SubAccountMetadata]') AND [c].[name] = N'ContextName');
    IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [SubAccountMetadata] DROP CONSTRAINT [' + @var3 + '];');
    ALTER TABLE [SubAccountMetadata] ALTER COLUMN [ContextName] nvarchar(max) NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903150128_UpdateMetadataContextToUseEnumName')
BEGIN
    DECLARE @var4 sysname;
    SELECT @var4 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[MetadataContexts]') AND [c].[name] = N'Name');
    IF @var4 IS NOT NULL EXEC(N'ALTER TABLE [MetadataContexts] DROP CONSTRAINT [' + @var4 + '];');
    ALTER TABLE [MetadataContexts] ALTER COLUMN [Name] nvarchar(max) NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903150128_UpdateMetadataContextToUseEnumName')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200903150128_UpdateMetadataContextToUseEnumName', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903161631_AddLastConfirmAtAndSkipCountColumnsToPortfolioTable')
BEGIN
    ALTER TABLE [Portfolios] ADD [LastConfirmAt] datetime2 NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903161631_AddLastConfirmAtAndSkipCountColumnsToPortfolioTable')
BEGIN
    ALTER TABLE [Portfolios] ADD [SkipCount] int NOT NULL DEFAULT 0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200903161631_AddLastConfirmAtAndSkipCountColumnsToPortfolioTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200903161631_AddLastConfirmAtAndSkipCountColumnsToPortfolioTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200904092048_AddFeeAmountToPayment')
BEGIN
    ALTER TABLE [Payments] ADD [FeeAmount] decimal(18,2) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200904092048_AddFeeAmountToPayment')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200904092048_AddFeeAmountToPayment', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200904102131_AddPaymentAmountTypeToPayment')
BEGIN
    ALTER TABLE [Payments] ADD [PaymentAmountType] int NOT NULL DEFAULT 0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200904102131_AddPaymentAmountTypeToPayment')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200904102131_AddPaymentAmountTypeToPayment', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200911075741_AddAmountTypeToExternalTransfer')
BEGIN
    ALTER TABLE [ExternalTransfers] ADD [AmountType] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200911075741_AddAmountTypeToExternalTransfer')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200911075741_AddAmountTypeToExternalTransfer', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200914191712_AddIFrameFlagColumnToVendorIntegrations')
BEGIN
    ALTER TABLE [VendorIntegrations] ADD [UseIFrame] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200914191712_AddIFrameFlagColumnToVendorIntegrations')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200914191712_AddIFrameFlagColumnToVendorIntegrations', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200925130033_AddHoldTypeToHold')
BEGIN
    DECLARE @var5 sysname;
    SELECT @var5 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Holds]') AND [c].[name] = N'StopPaymentType');
    IF @var5 IS NOT NULL EXEC(N'ALTER TABLE [Holds] DROP CONSTRAINT [' + @var5 + '];');
    ALTER TABLE [Holds] ALTER COLUMN [StopPaymentType] int NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200925130033_AddHoldTypeToHold')
BEGIN
    DECLARE @var6 sysname;
    SELECT @var6 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Holds]') AND [c].[name] = N'FeeAmount');
    IF @var6 IS NOT NULL EXEC(N'ALTER TABLE [Holds] DROP CONSTRAINT [' + @var6 + '];');
    ALTER TABLE [Holds] ALTER COLUMN [FeeAmount] decimal(18,2) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200925130033_AddHoldTypeToHold')
BEGIN
    ALTER TABLE [Holds] ADD [HoldType] int NOT NULL DEFAULT 0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20200925130033_AddHoldTypeToHold')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20200925130033_AddHoldTypeToHold', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201005145023_SubAccountCertificateFields')
BEGIN
    ALTER TABLE [SubAccounts] ADD [DividendAvailable] decimal(18,2) NOT NULL DEFAULT 0.0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201005145023_SubAccountCertificateFields')
BEGIN
    ALTER TABLE [SubAccounts] ADD [LastMaturityDate] datetime2 NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201005145023_SubAccountCertificateFields')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201005145023_SubAccountCertificateFields', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201007173626_AddUserTetherToken')
BEGIN
    ALTER TABLE [DigitalUsers] ADD [TetheredLoginToken] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201007173626_AddUserTetherToken')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201007173626_AddUserTetherToken', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201008095630_AddTypeToTransactionHold')
BEGIN
    ALTER TABLE [Holds] ADD [Type] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201008095630_AddTypeToTransactionHold')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201008095630_AddTypeToTransactionHold', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    IF SCHEMA_ID(N'alert') IS NULL EXEC(N'CREATE SCHEMA [alert];');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[Alerts] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NOT NULL,
        [Description] nvarchar(max) NULL,
        [Serial] varchar(100) NOT NULL,
        [Status] varchar(50) NOT NULL,
        [DefaultCondition] nvarchar(max) NULL,
        [DefaultNotificationType] nvarchar(max) NOT NULL,
        [DefaultTargetType] nvarchar(max) NOT NULL,
        [DefaultSchedule] nvarchar(max) NULL,
        CONSTRAINT [PK_Alerts] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[DeliveryChannels] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NOT NULL,
        [Description] nvarchar(max) NULL,
        [IsExternal] bit NOT NULL,
        [Status] varchar(50) NOT NULL,
        CONSTRAINT [PK_DeliveryChannels] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[Sources] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NOT NULL,
        [Description] nvarchar(max) NULL,
        [Status] varchar(50) NOT NULL,
        [VendorIntegrationId] bigint NULL,
        CONSTRAINT [PK_Sources] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[Types] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NOT NULL,
        [Description] nvarchar(max) NULL,
        [Status] varchar(50) NOT NULL,
        CONSTRAINT [PK_Types] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[AlertsDeliveryChannels] (
        [AlertId] bigint NOT NULL,
        [DeliveryChannelId] bigint NOT NULL,
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Type] varchar(50) NOT NULL,
        CONSTRAINT [PK_AlertsDeliveryChannels] PRIMARY KEY ([AlertId], [DeliveryChannelId]),
        CONSTRAINT [FK_AlertsDeliveryChannels_Alerts_AlertId] FOREIGN KEY ([AlertId]) REFERENCES [alert].[Alerts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AlertsDeliveryChannels_DeliveryChannels_DeliveryChannelId] FOREIGN KEY ([DeliveryChannelId]) REFERENCES [alert].[DeliveryChannels] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[MessageTemplates] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [TemplateId] nvarchar(max) NOT NULL,
        [Subject] nvarchar(max) NOT NULL,
        [Body] nvarchar(max) NOT NULL,
        [ReadOnlyBody] nvarchar(max) NULL,
        [DeliveryChannelId] bigint NULL,
        [IsCustomized] bit NOT NULL,
        [AlertId] bigint NOT NULL,
        CONSTRAINT [PK_MessageTemplates] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_MessageTemplates_Alerts_AlertId] FOREIGN KEY ([AlertId]) REFERENCES [alert].[Alerts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_MessageTemplates_DeliveryChannels_DeliveryChannelId] FOREIGN KEY ([DeliveryChannelId]) REFERENCES [alert].[DeliveryChannels] ([Id]) ON DELETE SET NULL
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[AlertsSources] (
        [AlertId] bigint NOT NULL,
        [SourceId] bigint NOT NULL,
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Type] varchar(50) NOT NULL,
        CONSTRAINT [PK_AlertAlertSources] PRIMARY KEY ([AlertId], [SourceId]),
        CONSTRAINT [FK_AlertsSources_Alerts_AlertId] FOREIGN KEY ([AlertId]) REFERENCES [alert].[Alerts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AlertsSources_Sources_SourceId] FOREIGN KEY ([SourceId]) REFERENCES [alert].[Sources] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[AlertsTypes] (
        [AlertId] bigint NOT NULL,
        [TypeId] bigint NOT NULL,
        CONSTRAINT [PK_AlertsAlertTypes] PRIMARY KEY ([AlertId], [TypeId]),
        CONSTRAINT [FK_AlertsTypes_Alerts_AlertId] FOREIGN KEY ([AlertId]) REFERENCES [alert].[Alerts] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_AlertsTypes_Types_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [alert].[Types] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[Enrollments] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [AlertId] bigint NOT NULL,
        [DigitalUserId] nvarchar(450) NULL,
        [Status] varchar(50) NOT NULL,
        [Condition] nvarchar(max) NULL,
        [Schedule] nvarchar(max) NULL,
        [TargetId] nvarchar(max) NULL,
        [TargetType] nvarchar(max) NOT NULL,
        [NotificationType] nvarchar(max) NOT NULL,
        CONSTRAINT [PK_Enrollments] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Enrollments_Alerts_AlertId] FOREIGN KEY ([AlertId]) REFERENCES [alert].[Alerts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Enrollments_DigitalUsers_DigitalUserId] FOREIGN KEY ([DigitalUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[EnrollmentsDeliveryChannels] (
        [EnrollmentDeliveryChannelId] uniqueidentifier NOT NULL,
        [EnrollmentId] bigint NOT NULL,
        [DeliveryChannelId] bigint NOT NULL,
        [InsertAt] datetime2 NOT NULL,
        [UpdateAt] datetime2 NOT NULL,
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [MessageTemplateId] bigint NULL,
        [Status] varchar(50) NOT NULL,
        [EmailAddressId] bigint NULL,
        [PhoneNumberId] bigint NULL,
        CONSTRAINT [PK_EnrollmentsDeliveryChannels_1] PRIMARY KEY ([EnrollmentDeliveryChannelId]),
        CONSTRAINT [FK_EnrollmentsDeliveryChannels_DeliveryChannels_DeliveryChannelId] FOREIGN KEY ([DeliveryChannelId]) REFERENCES [alert].[DeliveryChannels] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_EnrollmentsDeliveryChannels_Enrollments_EnrollmentId] FOREIGN KEY ([EnrollmentId]) REFERENCES [alert].[Enrollments] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_EnrollmentsDeliveryChannels_MessageTemplates_MessageTemplateId] FOREIGN KEY ([MessageTemplateId]) REFERENCES [alert].[MessageTemplates] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE TABLE [alert].[Messages] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Subject] nvarchar(max) NOT NULL,
        [Body] nvarchar(max) NOT NULL,
        [SourceId] bigint NOT NULL,
        [DestinationId] nvarchar(max) NULL,
        [DestinationType] nvarchar(max) NOT NULL,
        [TransportMessageId] nvarchar(max) NULL,
        [SentAt] datetime2 NULL,
        [DeliveredAt] datetime2 NULL,
        [DeliveredTo] nvarchar(max) NULL,
        [ReadAt] datetime2 NULL,
        [Status] varchar(50) NOT NULL,
        [Encrypted] bit NOT NULL,
        [AlertId] bigint NOT NULL,
        [EnrollmentDeliveryChannelId] uniqueidentifier NULL,
        CONSTRAINT [PK_Messages] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Messages_Alerts_AlertId] FOREIGN KEY ([AlertId]) REFERENCES [alert].[Alerts] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Messages_EnrollmentsDeliveryChannels_EnrollmentDeliveryChannelId] FOREIGN KEY ([EnrollmentDeliveryChannelId]) REFERENCES [alert].[EnrollmentsDeliveryChannels] ([EnrollmentDeliveryChannelId]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Messages_Sources_SourceId] FOREIGN KEY ([SourceId]) REFERENCES [alert].[Sources] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_AlertsDeliveryChannels_DeliveryChannelId] ON [alert].[AlertsDeliveryChannels] ([DeliveryChannelId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_AlertsSources_SourceId] ON [alert].[AlertsSources] ([SourceId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_AlertsTypes_TypeId] ON [alert].[AlertsTypes] ([TypeId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_Enrollments_AlertId] ON [alert].[Enrollments] ([AlertId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_Enrollments_DigitalUserId] ON [alert].[Enrollments] ([DigitalUserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_Messages_AlertId] ON [alert].[Messages] ([AlertId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_Messages_EnrollmentDeliveryChannelId] ON [alert].[Messages] ([EnrollmentDeliveryChannelId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_Messages_SourceId] ON [alert].[Messages] ([SourceId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_MessageTemplates_AlertId] ON [alert].[MessageTemplates] ([AlertId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_MessageTemplates_DeliveryChannelId] ON [alert].[MessageTemplates] ([DeliveryChannelId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_EnrollmentsDeliveryChannels_DeliveryChannelId] ON [alert].[EnrollmentsDeliveryChannels] ([DeliveryChannelId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_EnrollmentsDeliveryChannels_EnrollmentId] ON [alert].[EnrollmentsDeliveryChannels] ([EnrollmentId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    CREATE INDEX [IX_EnrollmentsDeliveryChannels_MessageTemplateId] ON [alert].[EnrollmentsDeliveryChannels] ([MessageTemplateId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020012551_AddAlerts')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201020012551_AddAlerts', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020195725_DepositsChangeSubAccountIDToForeingKeyReference')
BEGIN
    ALTER TABLE [Deposits] DROP CONSTRAINT [FK_Deposits_Portfolios_PortfolioId];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020195725_DepositsChangeSubAccountIDToForeingKeyReference')
BEGIN
    DROP INDEX [IX_Deposits_PortfolioId] ON [Deposits];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020195725_DepositsChangeSubAccountIDToForeingKeyReference')
BEGIN
    DECLARE @var7 sysname;
    SELECT @var7 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Deposits]') AND [c].[name] = N'SubAccountId');
    IF @var7 IS NOT NULL EXEC(N'ALTER TABLE [Deposits] DROP CONSTRAINT [' + @var7 + '];');
    ALTER TABLE [Deposits] ALTER COLUMN [SubAccountId] bigint NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020195725_DepositsChangeSubAccountIDToForeingKeyReference')
BEGIN
    CREATE INDEX [IX_Deposits_SubAccountId] ON [Deposits] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020195725_DepositsChangeSubAccountIDToForeingKeyReference')
BEGIN
    ALTER TABLE [Deposits] ADD CONSTRAINT [FK_Deposits_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE CASCADE;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201020195725_DepositsChangeSubAccountIDToForeingKeyReference')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201020195725_DepositsChangeSubAccountIDToForeingKeyReference', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201021211619_AddIndexForTetherLoginToken')
BEGIN
    DECLARE @var8 sysname;
    SELECT @var8 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[DigitalUsers]') AND [c].[name] = N'TetheredLoginToken');
    IF @var8 IS NOT NULL EXEC(N'ALTER TABLE [DigitalUsers] DROP CONSTRAINT [' + @var8 + '];');
    ALTER TABLE [DigitalUsers] ALTER COLUMN [TetheredLoginToken] nvarchar(450) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201021211619_AddIndexForTetherLoginToken')
BEGIN
    CREATE UNIQUE INDEX [UserTokenIndex] ON [DigitalUsers] ([TetheredLoginToken]) WHERE [TetheredLoginToken] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201021211619_AddIndexForTetherLoginToken')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201021211619_AddIndexForTetherLoginToken', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201022184246_DepositsAddsNxtStatus')
BEGIN
    ALTER TABLE [Deposits] ADD [NXTStatus] nvarchar(max) NOT NULL DEFAULT N'';
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201022184246_DepositsAddsNxtStatus')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201022184246_DepositsAddsNxtStatus', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201023155152_AddedAccessRestrictionTable')
BEGIN
    CREATE TABLE [AccessRestrictions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [BusinessId] bigint NOT NULL,
        [DigitalUserId] nvarchar(450) NOT NULL,
        [DayOfWeek] int NOT NULL,
        [AccessAllowed] bit NOT NULL,
        [StartTime] nvarchar(max) NULL,
        [EndTime] nvarchar(max) NULL,
        [IsDeleted] bit NOT NULL,
        CONSTRAINT [PK_AccessRestrictions] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AccessRestrictions_Businesses_BusinessId] FOREIGN KEY ([BusinessId]) REFERENCES [Businesses] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AccessRestrictions_DigitalUsers_DigitalUserId] FOREIGN KEY ([DigitalUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201023155152_AddedAccessRestrictionTable')
BEGIN
    CREATE INDEX [IX_AccessRestrictions_BusinessId] ON [AccessRestrictions] ([BusinessId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201023155152_AddedAccessRestrictionTable')
BEGIN
    CREATE INDEX [IX_AccessRestrictions_DigitalUserId] ON [AccessRestrictions] ([DigitalUserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201023155152_AddedAccessRestrictionTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201023155152_AddedAccessRestrictionTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201028221225_DepositsAddMICR')
BEGIN
    ALTER TABLE [Deposits] ADD [MICRAccount] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201028221225_DepositsAddMICR')
BEGIN
    ALTER TABLE [Deposits] ADD [MICRAuxOnUs] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201028221225_DepositsAddMICR')
BEGIN
    ALTER TABLE [Deposits] ADD [MICRField4] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201028221225_DepositsAddMICR')
BEGIN
    ALTER TABLE [Deposits] ADD [MICRRouting] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201028221225_DepositsAddMICR')
BEGIN
    ALTER TABLE [Deposits] ADD [MICRTrancode] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201028221225_DepositsAddMICR')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201028221225_DepositsAddMICR', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201103175226_CreateMerchantsTable')
BEGIN
    ALTER TABLE [Transactions] ADD [MerchantId] bigint NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201103175226_CreateMerchantsTable')
BEGIN
    ALTER TABLE [Transactions] ADD [UserAssignedMerchant] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201103175226_CreateMerchantsTable')
BEGIN
    CREATE TABLE [Merchants] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [DisplayName] nvarchar(max) NULL,
        [Rules] nvarchar(max) NULL,
        [DefaultCategoryCode] nvarchar(max) NULL,
        CONSTRAINT [PK_Merchants] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201103175226_CreateMerchantsTable')
BEGIN
    CREATE INDEX [IX_Transactions_MerchantId] ON [Transactions] ([MerchantId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201103175226_CreateMerchantsTable')
BEGIN
    ALTER TABLE [Transactions] ADD CONSTRAINT [FK_Transactions_Merchants_MerchantId] FOREIGN KEY ([MerchantId]) REFERENCES [Merchants] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201103175226_CreateMerchantsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201103175226_CreateMerchantsTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201110091750_AddColumnPrimaryToAccounts')
BEGIN
    ALTER TABLE [Accounts] ADD [Primary] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201110091750_AddColumnPrimaryToAccounts')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201110091750_AddColumnPrimaryToAccounts', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201113212409_MakeBaseTransferPaymentSoftDelete')
BEGIN
    DECLARE @var9 sysname;
    SELECT @var9 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transfers]') AND [c].[name] = N'IsDeleted');
    IF @var9 IS NOT NULL EXEC(N'ALTER TABLE [Transfers] DROP CONSTRAINT [' + @var9 + '];');
    ALTER TABLE [Transfers] ALTER COLUMN [IsDeleted] bit NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201113212409_MakeBaseTransferPaymentSoftDelete')
BEGIN
    DECLARE @var10 sysname;
    SELECT @var10 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Payments]') AND [c].[name] = N'IsDeleted');
    IF @var10 IS NOT NULL EXEC(N'ALTER TABLE [Payments] DROP CONSTRAINT [' + @var10 + '];');
    ALTER TABLE [Payments] ALTER COLUMN [IsDeleted] bit NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201113212409_MakeBaseTransferPaymentSoftDelete')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201113212409_MakeBaseTransferPaymentSoftDelete', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117135652_AddLogoFieldsToMerchant')
BEGIN
    ALTER TABLE [Merchants] ADD [LogoFile] varbinary(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117135652_AddLogoFieldsToMerchant')
BEGIN
    ALTER TABLE [Merchants] ADD [LogoFileType] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117135652_AddLogoFieldsToMerchant')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201117135652_AddLogoFieldsToMerchant', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117184502_BusinessRegistration')
BEGIN
    DECLARE @var11 sysname;
    SELECT @var11 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Businesses]') AND [c].[name] = N'Name');
    IF @var11 IS NOT NULL EXEC(N'ALTER TABLE [Businesses] DROP CONSTRAINT [' + @var11 + '];');
    ALTER TABLE [Businesses] ALTER COLUMN [Name] nvarchar(450) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117184502_BusinessRegistration')
BEGIN
    ALTER TABLE [Businesses] ADD [TaxId] nvarchar(450) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117184502_BusinessRegistration')
BEGIN
    CREATE UNIQUE INDEX [BusinessIdentifierUniqueIndex] ON [Businesses] ([Name], [TaxId]) WHERE [Name] IS NOT NULL AND [TaxId] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117184502_BusinessRegistration')
BEGIN
    ALTER TABLE [EmailAddresses] ADD [DigitalUserId] nvarchar(450) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117184502_BusinessRegistration')
BEGIN
    ALTER TABLE [EmailAddresses] ADD CONSTRAINT [FK_EmailAddress_Digital_User_Id] FOREIGN KEY ([DigitalUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117184502_BusinessRegistration')
BEGIN
    ALTER TABLE [PhoneNumbers] ADD [DigitalUserId] nvarchar(450) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117184502_BusinessRegistration')
BEGIN
    ALTER TABLE [PhoneNumbers] ADD CONSTRAINT [FK_PhoneNumber_Digital_User_Id] FOREIGN KEY ([DigitalUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201117184502_BusinessRegistration')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201117184502_BusinessRegistration', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201118021650_AlertsAdjustNotificationTypes')
BEGIN
    EXEC sp_rename N'[alert].[Alerts].[DefaultNotificationType]', N'DefaultNotificationTypes', N'COLUMN';
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201118021650_AlertsAdjustNotificationTypes')
BEGIN
    EXEC sp_rename N'[alert].[Enrollments].[NotificationType]', N'NotificationTypes', N'COLUMN';
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201118021650_AlertsAdjustNotificationTypes')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201118021650_AlertsAdjustNotificationTypes', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201118141502_AddStatementFieldsForSubAccounts')
BEGIN
    ALTER TABLE [SubAccounts] ADD [LastStatementDate] datetime2 NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201118141502_AddStatementFieldsForSubAccounts')
BEGIN
    ALTER TABLE [SubAccounts] ADD [StatementBalance] decimal(18,2) NOT NULL DEFAULT 0.0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201118141502_AddStatementFieldsForSubAccounts')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201118141502_AddStatementFieldsForSubAccounts', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE TABLE [BusinessBankingPermissions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [FullyQualifiedName] nvarchar(max) NULL,
        [DisplayName] nvarchar(max) NULL,
        [Description] nvarchar(max) NULL,
        [IsAdmin] bit NOT NULL,
        CONSTRAINT [PK_BusinessBankingPermissions] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE TABLE [BusinessBankingRoles] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [IsDefault] bit NOT NULL,
        [IsActive] bit NOT NULL,
        [BusinessId] bigint NULL,
        CONSTRAINT [PK_BusinessBankingRoles] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BusinessBankingRoles_Businesses_BusinessId] FOREIGN KEY ([BusinessId]) REFERENCES [Businesses] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE TABLE [BusinessUserSubAccount] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [UserId] nvarchar(450) NULL,
        [SubAccountId] bigint NOT NULL,
        [AccessType] int NOT NULL,
        CONSTRAINT [PK_BusinessUserSubAccount] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BusinessUserSubAccount_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_BusinessUserSubAccount_DigitalUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE TABLE [BusinessBankingRolePermissions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [BusinessBankingRoleId] bigint NOT NULL,
        [BusinessBankingPermissionId] bigint NOT NULL,
        [Allowed] bit NOT NULL,
        CONSTRAINT [PK_BusinessBankingRolePermissions] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BusinessBankingRolePermissions_BusinessBankingPermissions_BusinessBankingPermissionId] FOREIGN KEY ([BusinessBankingPermissionId]) REFERENCES [BusinessBankingPermissions] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_BusinessBankingRolePermissions_BusinessBankingRoles_BusinessBankingRoleId] FOREIGN KEY ([BusinessBankingRoleId]) REFERENCES [BusinessBankingRoles] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE TABLE [BusinessUserRoles] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [DigitalUserId] nvarchar(450) NULL,
        [BusinessBankingRoleId] bigint NOT NULL,
        CONSTRAINT [PK_BusinessUserRoles] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BusinessUserRoles_BusinessBankingRoles_BusinessBankingRoleId] FOREIGN KEY ([BusinessBankingRoleId]) REFERENCES [BusinessBankingRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_BusinessUserRoles_DigitalUsers_DigitalUserId] FOREIGN KEY ([DigitalUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE TABLE [BusinessUserRoleSubAccount] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [BusinessUserRoleId] bigint NOT NULL,
        [SubAccountId] bigint NOT NULL,
        [AccessType] int NOT NULL,
        CONSTRAINT [PK_BusinessUserRoleSubAccount] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_BusinessUserRoleSubAccount_BusinessUserRoles_BusinessUserRoleId] FOREIGN KEY ([BusinessUserRoleId]) REFERENCES [BusinessUserRoles] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_BusinessUserRoleSubAccount_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE INDEX [IX_BusinessBankingRolePermissions_BusinessBankingPermissionId] ON [BusinessBankingRolePermissions] ([BusinessBankingPermissionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE UNIQUE INDEX [IX_BusinessBankingRolePermissions_BusinessBankingRoleId_BusinessBankingPermissionId] ON [BusinessBankingRolePermissions] ([BusinessBankingRoleId], [BusinessBankingPermissionId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE INDEX [IX_BusinessBankingRoles_BusinessId] ON [BusinessBankingRoles] ([BusinessId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE INDEX [IX_BusinessUserRoles_BusinessBankingRoleId] ON [BusinessUserRoles] ([BusinessBankingRoleId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE INDEX [IX_BusinessUserRoles_DigitalUserId] ON [BusinessUserRoles] ([DigitalUserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE INDEX [IX_BusinessUserRoleSubAccount_BusinessUserRoleId] ON [BusinessUserRoleSubAccount] ([BusinessUserRoleId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE INDEX [IX_BusinessUserRoleSubAccount_SubAccountId] ON [BusinessUserRoleSubAccount] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE INDEX [IX_BusinessUserSubAccount_SubAccountId] ON [BusinessUserSubAccount] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    CREATE INDEX [IX_BusinessUserSubAccount_UserId] ON [BusinessUserSubAccount] ([UserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120101848_AddBusinessUserSubAccountMapping')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201120101848_AddBusinessUserSubAccountMapping', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120210238_ChangeBusinessRoleIdForeignKeyType')
BEGIN
    ALTER TABLE [BusinessUserRoles] DROP CONSTRAINT [FK_BusinessUserRoles_BusinessBankingRoles_BusinessBankingRoleId];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120210238_ChangeBusinessRoleIdForeignKeyType')
BEGIN
    DROP INDEX [IX_BusinessUserRoles_BusinessBankingRoleId] ON [BusinessUserRoles];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120210238_ChangeBusinessRoleIdForeignKeyType')
BEGIN
    DECLARE @var12 sysname;
    SELECT @var12 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BusinessUserRoles]') AND [c].[name] = N'BusinessBankingRoleId');
    IF @var12 IS NOT NULL EXEC(N'ALTER TABLE [BusinessUserRoles] DROP CONSTRAINT [' + @var12 + '];');
    ALTER TABLE [BusinessUserRoles] ALTER COLUMN [BusinessBankingRoleId] bigint NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120210238_ChangeBusinessRoleIdForeignKeyType')
BEGIN
    CREATE INDEX [IX_BusinessUserRoles_BusinessBankingRoleId] ON [BusinessUserRoles] ([BusinessBankingRoleId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120210238_ChangeBusinessRoleIdForeignKeyType')
BEGIN
    ALTER TABLE [BusinessUserRoles] ADD CONSTRAINT [FK_BusinessUserRoles_BusinessBankingRoles_BusinessBankingRoleId] FOREIGN KEY ([BusinessBankingRoleId]) REFERENCES [BusinessBankingRoles] ([Id]) ON DELETE CASCADE;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120210238_ChangeBusinessRoleIdForeignKeyType')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201120210238_ChangeBusinessRoleIdForeignKeyType', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201120210930_AlertSchemaUpdates')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201120210930_AlertSchemaUpdates', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201124160400_AddedTermsAndConditions')
BEGIN
    CREATE TABLE [DigitalUserTermsAcceptance] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [UserId] nvarchar(450) NULL,
        [FeatureName] nvarchar(max) NULL,
        [AcceptedTerms] nvarchar(max) NULL,
        CONSTRAINT [PK_DigitalUserTermsAcceptance] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_DigitalUserTermsAcceptance_DigitalUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201124160400_AddedTermsAndConditions')
BEGIN
    CREATE TABLE [TermsAndConditions] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [FeatureName] nvarchar(450) NULL,
        [RequiresAcceptance] bit NOT NULL,
        [Text] nvarchar(max) NULL,
        CONSTRAINT [PK_TermsAndConditions] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201124160400_AddedTermsAndConditions')
BEGIN
    CREATE TABLE [TermsLink] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [TermsAndConditionsId] bigint NOT NULL,
        [Href] nvarchar(max) NULL,
        [File] varbinary(max) NULL,
        [DisplayText] nvarchar(max) NULL,
        CONSTRAINT [PK_TermsLink] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_TermsLink_TermsAndConditions_TermsAndConditionsId] FOREIGN KEY ([TermsAndConditionsId]) REFERENCES [TermsAndConditions] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201124160400_AddedTermsAndConditions')
BEGIN
    CREATE INDEX [IX_DigitalUserTermsAcceptance_UserId] ON [DigitalUserTermsAcceptance] ([UserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201124160400_AddedTermsAndConditions')
BEGIN
    CREATE UNIQUE INDEX [TermsAndConditionsFeatureNameUniqueIndex] ON [TermsAndConditions] ([FeatureName]) WHERE [FeatureName] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201124160400_AddedTermsAndConditions')
BEGIN
    CREATE INDEX [IX_TermsLink_TermsAndConditionsId] ON [TermsLink] ([TermsAndConditionsId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201124160400_AddedTermsAndConditions')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201124160400_AddedTermsAndConditions', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201126021953_AddPaymentMethodFieldSubAccounts')
BEGIN
    ALTER TABLE [SubAccounts] ADD [PaymentMethod] int NOT NULL DEFAULT 0;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201126021953_AddPaymentMethodFieldSubAccounts')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201126021953_AddPaymentMethodFieldSubAccounts', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201126024542_AddCategoryTable')
BEGIN
    ALTER TABLE [Transactions] ADD [CategoryId] bigint NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201126024542_AddCategoryTable')
BEGIN
    ALTER TABLE [Transactions] ADD [UserAssignedCategory] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201126024542_AddCategoryTable')
BEGIN
    CREATE TABLE [Categories] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [Name] nvarchar(max) NULL,
        [DisplayName] nvarchar(max) NULL,
        [MccCodes] nvarchar(max) NULL,
        CONSTRAINT [PK_Categories] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201126024542_AddCategoryTable')
BEGIN
    CREATE INDEX [IX_Transactions_CategoryId] ON [Transactions] ([CategoryId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201126024542_AddCategoryTable')
BEGIN
    ALTER TABLE [Transactions] ADD CONSTRAINT [FK_Transactions_Categories_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES [Categories] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201126024542_AddCategoryTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201126024542_AddCategoryTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130143206_AddUserFeatureSettingsTable')
BEGIN
    CREATE TABLE [UserFeatureSettings] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [UserId] nvarchar(450) NULL,
        [Feature] nvarchar(max) NULL,
        [Enabled] bit NOT NULL,
        CONSTRAINT [PK_UserFeatureSettings] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_UserFeatureSettings_DigitalUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130143206_AddUserFeatureSettingsTable')
BEGIN
    CREATE INDEX [IX_UserFeatureSettings_UserId] ON [UserFeatureSettings] ([UserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130143206_AddUserFeatureSettingsTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201130143206_AddUserFeatureSettingsTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130151027_AddAccountSavingsGoalTable')
BEGIN
    CREATE TABLE [AccountSavingsGoals] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [PortfolioId] bigint NOT NULL,
        [SubAccountId] bigint NOT NULL,
        [Name] nvarchar(max) NULL,
        [GoalAmount] decimal(18,2) NOT NULL,
        [TargetDate] datetime2 NOT NULL,
        CONSTRAINT [PK_AccountSavingsGoals] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_AccountSavingsGoals_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_AccountSavingsGoals_SubAccounts_SubAccountId] FOREIGN KEY ([SubAccountId]) REFERENCES [SubAccounts] ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130151027_AddAccountSavingsGoalTable')
BEGIN
    CREATE INDEX [IX_AccountSavingsGoals_PortfolioId] ON [AccountSavingsGoals] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130151027_AddAccountSavingsGoalTable')
BEGIN
    CREATE INDEX [IX_AccountSavingsGoals_SubAccountId] ON [AccountSavingsGoals] ([SubAccountId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130151027_AddAccountSavingsGoalTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201130151027_AddAccountSavingsGoalTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130155337_AddUserBudgetCategoriesTable')
BEGIN
    CREATE TABLE [UserBudgetCategories] (
        [Id] bigint NOT NULL IDENTITY,
        [InsertAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [UpdateAt] datetime2 NOT NULL DEFAULT (getutcdate()),
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [PortfolioId] bigint NOT NULL,
        [CategoryId] bigint NOT NULL,
        [Month] datetime2 NOT NULL,
        [AmountBudgeted] decimal(18,2) NOT NULL,
        [AmountSpent] decimal(18,2) NOT NULL,
        CONSTRAINT [PK_UserBudgetCategories] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_UserBudgetCategories_Categories_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES [Categories] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_UserBudgetCategories_Portfolios_PortfolioId] FOREIGN KEY ([PortfolioId]) REFERENCES [Portfolios] ([Id]) ON DELETE CASCADE
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130155337_AddUserBudgetCategoriesTable')
BEGIN
    CREATE INDEX [IX_UserBudgetCategories_CategoryId] ON [UserBudgetCategories] ([CategoryId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130155337_AddUserBudgetCategoriesTable')
BEGIN
    CREATE INDEX [IX_UserBudgetCategories_PortfolioId] ON [UserBudgetCategories] ([PortfolioId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201130155337_AddUserBudgetCategoriesTable')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201130155337_AddUserBudgetCategoriesTable', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201208114222_AddColumnIsDeletedToAccountSavingsGoal')
BEGIN
    ALTER TABLE [AccountSavingsGoals] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201208114222_AddColumnIsDeletedToAccountSavingsGoal')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201208114222_AddColumnIsDeletedToAccountSavingsGoal', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201212071524_AddedAdminIdToMessage')
BEGIN
    ALTER TABLE [SecureMessages] ADD [DigitalAdminId] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201212071524_AddedAdminIdToMessage')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201212071524_AddedAdminIdToMessage', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201214114829_AddedSenderTypeInSecureMessage')
BEGIN
    ALTER TABLE [SecureMessages] ADD [Sender] int NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201214114829_AddedSenderTypeInSecureMessage')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201214114829_AddedSenderTypeInSecureMessage', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201215005704_AddedDefaultTargetIdToAlerts')
BEGIN
    ALTER TABLE [alert].[Alerts] ADD [DefaultTargetId] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201215005704_AddedDefaultTargetIdToAlerts')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201215005704_AddedDefaultTargetIdToAlerts', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201217065159_AddColumnActiveToPortfolios')
BEGIN
    ALTER TABLE [Portfolios] ADD [Active] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201217065159_AddColumnActiveToPortfolios')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201217065159_AddColumnActiveToPortfolios', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222033749_AddedFeaturePermissionToAlerts')
BEGIN
    ALTER TABLE [alert].[Alerts] ADD [FeaturePermission] nvarchar(450) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222033749_AddedFeaturePermissionToAlerts')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201222033749_AddedFeaturePermissionToAlerts', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    EXEC sp_rename N'[DigitalAdmins].[UserNameIndex]', N'AdminUserNameIndex', N'INDEX';
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    EXEC sp_rename N'[AccessRestrictions].[IX_AccessRestrictions_DigitalUserId]', N'AccessRestrictionUserId', N'INDEX';
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_UserVendorData_UserId] ON [UserVendorData];
    DECLARE @var13 sysname;
    SELECT @var13 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UserVendorData]') AND [c].[name] = N'UserId');
    IF @var13 IS NOT NULL EXEC(N'ALTER TABLE [UserVendorData] DROP CONSTRAINT [' + @var13 + '];');
    ALTER TABLE [UserVendorData] ALTER COLUMN [UserId] nvarchar(40) NULL;
    CREATE INDEX [IX_UserVendorData_UserId] ON [UserVendorData] ([UserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var14 sysname;
    SELECT @var14 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transfers]') AND [c].[name] = N'ConfirmationNumber');
    IF @var14 IS NOT NULL EXEC(N'ALTER TABLE [Transfers] DROP CONSTRAINT [' + @var14 + '];');
    ALTER TABLE [Transfers] ALTER COLUMN [ConfirmationNumber] nvarchar(50) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_Transactions_ExternalKey] ON [Transactions];
    DECLARE @var15 sysname;
    SELECT @var15 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'ExternalKey');
    IF @var15 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var15 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_Transactions_ExternalKey] ON [Transactions] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_TransactionChecks_ExternalKey] ON [TransactionChecks];
    DECLARE @var16 sysname;
    SELECT @var16 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[TransactionChecks]') AND [c].[name] = N'ExternalKey');
    IF @var16 IS NOT NULL EXEC(N'ALTER TABLE [TransactionChecks] DROP CONSTRAINT [' + @var16 + '];');
    ALTER TABLE [TransactionChecks] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_TransactionChecks_ExternalKey] ON [TransactionChecks] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_SubAccounts_ExternalKey] ON [SubAccounts];
    DECLARE @var17 sysname;
    SELECT @var17 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[SubAccounts]') AND [c].[name] = N'ExternalKey');
    IF @var17 IS NOT NULL EXEC(N'ALTER TABLE [SubAccounts] DROP CONSTRAINT [' + @var17 + '];');
    ALTER TABLE [SubAccounts] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_SubAccounts_ExternalKey] ON [SubAccounts] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var18 sysname;
    SELECT @var18 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sessions]') AND [c].[name] = N'Type');
    IF @var18 IS NOT NULL EXEC(N'ALTER TABLE [Sessions] DROP CONSTRAINT [' + @var18 + '];');
    ALTER TABLE [Sessions] ALTER COLUMN [Type] nvarchar(20) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var19 sysname;
    SELECT @var19 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sessions]') AND [c].[name] = N'SessionCorrelationId');
    IF @var19 IS NOT NULL EXEC(N'ALTER TABLE [Sessions] DROP CONSTRAINT [' + @var19 + '];');
    ALTER TABLE [Sessions] ALTER COLUMN [SessionCorrelationId] nvarchar(40) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var20 sysname;
    SELECT @var20 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sessions]') AND [c].[name] = N'MfaType');
    IF @var20 IS NOT NULL EXEC(N'ALTER TABLE [Sessions] DROP CONSTRAINT [' + @var20 + '];');
    ALTER TABLE [Sessions] ALTER COLUMN [MfaType] nvarchar(20) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var21 sysname;
    SELECT @var21 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sessions]') AND [c].[name] = N'MfaPasscode');
    IF @var21 IS NOT NULL EXEC(N'ALTER TABLE [Sessions] DROP CONSTRAINT [' + @var21 + '];');
    ALTER TABLE [Sessions] ALTER COLUMN [MfaPasscode] nvarchar(10) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var22 sysname;
    SELECT @var22 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sessions]') AND [c].[name] = N'IpAddress');
    IF @var22 IS NOT NULL EXEC(N'ALTER TABLE [Sessions] DROP CONSTRAINT [' + @var22 + '];');
    ALTER TABLE [Sessions] ALTER COLUMN [IpAddress] nvarchar(40) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_PhoneNumbers_ExternalKey] ON [PhoneNumbers];
    DECLARE @var23 sysname;
    SELECT @var23 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[PhoneNumbers]') AND [c].[name] = N'ExternalKey');
    IF @var23 IS NOT NULL EXEC(N'ALTER TABLE [PhoneNumbers] DROP CONSTRAINT [' + @var23 + '];');
    ALTER TABLE [PhoneNumbers] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_PhoneNumbers_ExternalKey] ON [PhoneNumbers] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_Persons_ExternalKey] ON [Persons];
    DECLARE @var24 sysname;
    SELECT @var24 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Persons]') AND [c].[name] = N'ExternalKey');
    IF @var24 IS NOT NULL EXEC(N'ALTER TABLE [Persons] DROP CONSTRAINT [' + @var24 + '];');
    ALTER TABLE [Persons] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_Persons_ExternalKey] ON [Persons] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_Payments_ExternalKey] ON [Payments];
    DECLARE @var25 sysname;
    SELECT @var25 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Payments]') AND [c].[name] = N'ExternalKey');
    IF @var25 IS NOT NULL EXEC(N'ALTER TABLE [Payments] DROP CONSTRAINT [' + @var25 + '];');
    ALTER TABLE [Payments] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_Payments_ExternalKey] ON [Payments] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var26 sysname;
    SELECT @var26 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Payments]') AND [c].[name] = N'ConfirmationNumber');
    IF @var26 IS NOT NULL EXEC(N'ALTER TABLE [Payments] DROP CONSTRAINT [' + @var26 + '];');
    ALTER TABLE [Payments] ALTER COLUMN [ConfirmationNumber] nvarchar(50) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_Holds_ExternalKey] ON [Holds];
    DECLARE @var27 sysname;
    SELECT @var27 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Holds]') AND [c].[name] = N'ExternalKey');
    IF @var27 IS NOT NULL EXEC(N'ALTER TABLE [Holds] DROP CONSTRAINT [' + @var27 + '];');
    ALTER TABLE [Holds] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_Holds_ExternalKey] ON [Holds] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var28 sysname;
    SELECT @var28 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ExternalTransfers]') AND [c].[name] = N'ConfirmationNumber');
    IF @var28 IS NOT NULL EXEC(N'ALTER TABLE [ExternalTransfers] DROP CONSTRAINT [' + @var28 + '];');
    ALTER TABLE [ExternalTransfers] ALTER COLUMN [ConfirmationNumber] nvarchar(50) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_ExternalAccounts_ExternalKey] ON [ExternalAccounts];
    DECLARE @var29 sysname;
    SELECT @var29 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ExternalAccounts]') AND [c].[name] = N'ExternalKey');
    IF @var29 IS NOT NULL EXEC(N'ALTER TABLE [ExternalAccounts] DROP CONSTRAINT [' + @var29 + '];');
    ALTER TABLE [ExternalAccounts] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_ExternalAccounts_ExternalKey] ON [ExternalAccounts] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_EmailAddresses_ExternalKey] ON [EmailAddresses];
    DECLARE @var30 sysname;
    SELECT @var30 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[EmailAddresses]') AND [c].[name] = N'ExternalKey');
    IF @var30 IS NOT NULL EXEC(N'ALTER TABLE [EmailAddresses] DROP CONSTRAINT [' + @var30 + '];');
    ALTER TABLE [EmailAddresses] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_EmailAddresses_ExternalKey] ON [EmailAddresses] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [AdminUserNameIndex] ON [DigitalAdmins];
    DECLARE @var31 sysname;
    SELECT @var31 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[DigitalAdmins]') AND [c].[name] = N'NormalizedUserName');
    IF @var31 IS NOT NULL EXEC(N'ALTER TABLE [DigitalAdmins] DROP CONSTRAINT [' + @var31 + '];');
    ALTER TABLE [DigitalAdmins] ALTER COLUMN [NormalizedUserName] nvarchar(150) NULL;
    CREATE UNIQUE INDEX [AdminUserNameIndex] ON [DigitalAdmins] ([NormalizedUserName]) WHERE [NormalizedUserName] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var32 sysname;
    SELECT @var32 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'Uuid');
    IF @var32 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var32 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [Uuid] nvarchar(256) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var33 sysname;
    SELECT @var33 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'UserAgent');
    IF @var33 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var33 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [UserAgent] nvarchar(400) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var34 sysname;
    SELECT @var34 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'SdkVersion');
    IF @var34 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var34 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [SdkVersion] nvarchar(100) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var35 sysname;
    SELECT @var35 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'Region');
    IF @var35 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var35 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [Region] nvarchar(100) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var36 sysname;
    SELECT @var36 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'OsVersion');
    IF @var36 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var36 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [OsVersion] nvarchar(100) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var37 sysname;
    SELECT @var37 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'Os');
    IF @var37 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var37 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [Os] nvarchar(100) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var38 sysname;
    SELECT @var38 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'Language');
    IF @var38 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var38 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [Language] nvarchar(100) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var39 sysname;
    SELECT @var39 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'Key');
    IF @var39 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var39 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [Key] nvarchar(256) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var40 sysname;
    SELECT @var40 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Devices]') AND [c].[name] = N'Browser');
    IF @var40 IS NOT NULL EXEC(N'ALTER TABLE [Devices] DROP CONSTRAINT [' + @var40 + '];');
    ALTER TABLE [Devices] ALTER COLUMN [Browser] nvarchar(200) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_Cards_ExternalKey] ON [Cards];
    DECLARE @var41 sysname;
    SELECT @var41 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Cards]') AND [c].[name] = N'ExternalKey');
    IF @var41 IS NOT NULL EXEC(N'ALTER TABLE [Cards] DROP CONSTRAINT [' + @var41 + '];');
    ALTER TABLE [Cards] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_Cards_ExternalKey] ON [Cards] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_BillPayPayments_ExternalKey] ON [BillPayPayments];
    DECLARE @var42 sysname;
    SELECT @var42 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BillPayPayments]') AND [c].[name] = N'ExternalKey');
    IF @var42 IS NOT NULL EXEC(N'ALTER TABLE [BillPayPayments] DROP CONSTRAINT [' + @var42 + '];');
    ALTER TABLE [BillPayPayments] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_BillPayPayments_ExternalKey] ON [BillPayPayments] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var43 sysname;
    SELECT @var43 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BillPayPayments]') AND [c].[name] = N'ConfirmationNumber');
    IF @var43 IS NOT NULL EXEC(N'ALTER TABLE [BillPayPayments] DROP CONSTRAINT [' + @var43 + '];');
    ALTER TABLE [BillPayPayments] ALTER COLUMN [ConfirmationNumber] nvarchar(50) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_BillPayPayees_ExternalKey] ON [BillPayPayees];
    DECLARE @var44 sysname;
    SELECT @var44 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[BillPayPayees]') AND [c].[name] = N'ExternalKey');
    IF @var44 IS NOT NULL EXEC(N'ALTER TABLE [BillPayPayees] DROP CONSTRAINT [' + @var44 + '];');
    ALTER TABLE [BillPayPayees] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_BillPayPayees_ExternalKey] ON [BillPayPayees] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_Addresses_ExternalKey] ON [Addresses];
    DECLARE @var45 sysname;
    SELECT @var45 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Addresses]') AND [c].[name] = N'ExternalKey');
    IF @var45 IS NOT NULL EXEC(N'ALTER TABLE [Addresses] DROP CONSTRAINT [' + @var45 + '];');
    ALTER TABLE [Addresses] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_Addresses_ExternalKey] ON [Addresses] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var46 sysname;
    SELECT @var46 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActivityLogs]') AND [c].[name] = N'Subaction');
    IF @var46 IS NOT NULL EXEC(N'ALTER TABLE [ActivityLogs] DROP CONSTRAINT [' + @var46 + '];');
    ALTER TABLE [ActivityLogs] ALTER COLUMN [Subaction] nvarchar(75) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var47 sysname;
    SELECT @var47 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActivityLogs]') AND [c].[name] = N'SessionCorrelationId');
    IF @var47 IS NOT NULL EXEC(N'ALTER TABLE [ActivityLogs] DROP CONSTRAINT [' + @var47 + '];');
    ALTER TABLE [ActivityLogs] ALTER COLUMN [SessionCorrelationId] nvarchar(40) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var48 sysname;
    SELECT @var48 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActivityLogs]') AND [c].[name] = N'Module');
    IF @var48 IS NOT NULL EXEC(N'ALTER TABLE [ActivityLogs] DROP CONSTRAINT [' + @var48 + '];');
    ALTER TABLE [ActivityLogs] ALTER COLUMN [Module] nvarchar(75) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DECLARE @var49 sysname;
    SELECT @var49 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[ActivityLogs]') AND [c].[name] = N'Action');
    IF @var49 IS NOT NULL EXEC(N'ALTER TABLE [ActivityLogs] DROP CONSTRAINT [' + @var49 + '];');
    ALTER TABLE [ActivityLogs] ALTER COLUMN [Action] nvarchar(75) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    DROP INDEX [IX_Accounts_ExternalKey] ON [Accounts];
    DECLARE @var50 sysname;
    SELECT @var50 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Accounts]') AND [c].[name] = N'ExternalKey');
    IF @var50 IS NOT NULL EXEC(N'ALTER TABLE [Accounts] DROP CONSTRAINT [' + @var50 + '];');
    ALTER TABLE [Accounts] ALTER COLUMN [ExternalKey] nvarchar(100) NULL;
    CREATE INDEX [IX_Accounts_ExternalKey] ON [Accounts] ([ExternalKey]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    CREATE UNIQUE INDEX [TransferConfirmationNumber] ON [Transfers] ([ConfirmationNumber]) WHERE [ConfirmationNumber] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    CREATE UNIQUE INDEX [IX_Sessions_SessionCorrelationId] ON [Sessions] ([SessionCorrelationId]) WHERE [SessionCorrelationId] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    CREATE UNIQUE INDEX [PaymentConfirmationNumber] ON [Payments] ([ConfirmationNumber]) WHERE [ConfirmationNumber] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    CREATE UNIQUE INDEX [ExternalTransferConfirmationNumber] ON [ExternalTransfers] ([ConfirmationNumber]) WHERE [ConfirmationNumber] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    CREATE INDEX [DeviceKeyIndex] ON [Devices] ([Key]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    CREATE INDEX [DeviceUuidIndex] ON [Devices] ([Uuid]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    CREATE UNIQUE INDEX [BillPayPaymentConfirmationNumber] ON [BillPayPayments] ([ConfirmationNumber]) WHERE [ConfirmationNumber] IS NOT NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    CREATE INDEX [IX_ActivityLogs_SessionCorrelationId] ON [ActivityLogs] ([SessionCorrelationId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20201222201449_ColumnSizeAndIndexPerformanceTuning')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20201222201449_ColumnSizeAndIndexPerformanceTuning', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210106190450_TokenExpiry')
BEGIN
    ALTER TABLE [DigitalUsers] ADD [TokenExpiresUtc] datetime2 NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210106190450_TokenExpiry')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210106190450_TokenExpiry', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210119215731_AddLookup')
BEGIN
    CREATE TABLE [Lookups] (
        [Id] bigint NOT NULL IDENTITY,
        [ExpireAt] datetime2 NULL,
        [ExtendData] nvarchar(max) NULL,
        [ExternalKey] nvarchar(max) NULL,
        [ExternalHash] nvarchar(max) NULL,
        [ExternalUpdateAt] datetime2 NULL,
        CONSTRAINT [PK_Lookups] PRIMARY KEY ([Id])
    );
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210119215731_AddLookup')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210119215731_AddLookup', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210121192151_PasswordEmailSentDate')
BEGIN
    ALTER TABLE [DigitalUsers] ADD [PasswordResetEmailSentUtc] datetime2 NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210121192151_PasswordEmailSentDate')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210121192151_PasswordEmailSentDate', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210122130340_AddIsBusinessToDigitalRole')
BEGIN
    ALTER TABLE [DigitalRoles] ADD [IsBusiness] bit NOT NULL DEFAULT CAST(0 AS bit);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210122130340_AddIsBusinessToDigitalRole')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210122130340_AddIsBusinessToDigitalRole', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210124042627_AddSubAccountProductMappingDateTracking')
BEGIN
    ALTER TABLE [SubAccounts] ADD [LastProductMapping] datetime2 NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210124042627_AddSubAccountProductMappingDateTracking')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210124042627_AddSubAccountProductMappingDateTracking', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210128194053_AddIdentifierHashToPortfolioForIndexedLookup')
BEGIN
    ALTER TABLE [Portfolios] ADD [IdentifierHash] nvarchar(64) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210128194053_AddIdentifierHashToPortfolioForIndexedLookup')
BEGIN
    CREATE INDEX [IX_Portfolios_IdentifierHash] ON [Portfolios] ([IdentifierHash]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210128194053_AddIdentifierHashToPortfolioForIndexedLookup')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210128194053_AddIdentifierHashToPortfolioForIndexedLookup', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210203013449_AlertsExternalKeys')
BEGIN
    ALTER TABLE [alert].[Types] ADD [ExternalCategories] nvarchar(4000) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210203013449_AlertsExternalKeys')
BEGIN
    ALTER TABLE [alert].[Types] ADD [ExternalTypes] nvarchar(4000) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210203013449_AlertsExternalKeys')
BEGIN
    ALTER TABLE [alert].[Enrollments] ADD [ExternalHash] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210203013449_AlertsExternalKeys')
BEGIN
    ALTER TABLE [alert].[Enrollments] ADD [ExternalKey] nvarchar(max) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210203013449_AlertsExternalKeys')
BEGIN
    ALTER TABLE [alert].[Enrollments] ADD [ExternalUpdateAt] datetime2 NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210203013449_AlertsExternalKeys')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210203013449_AlertsExternalKeys', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210210011338_AlertsMessagesToDeliveryChannels')
BEGIN
    ALTER TABLE [alert].[Messages] ADD [DeliveryChannelId] bigint NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210210011338_AlertsMessagesToDeliveryChannels')
BEGIN
    CREATE INDEX [IX_Messages_DeliveryChannelId] ON [alert].[Messages] ([DeliveryChannelId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210210011338_AlertsMessagesToDeliveryChannels')
BEGIN
    ALTER TABLE [alert].[Messages] ADD CONSTRAINT [FK_Messages_DeliveryChannels_DeliveryChannelId] FOREIGN KEY ([DeliveryChannelId]) REFERENCES [alert].[DeliveryChannels] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210210011338_AlertsMessagesToDeliveryChannels')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210210011338_AlertsMessagesToDeliveryChannels', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210223173534_UpdatedAdminActivityLog')
BEGIN
    ALTER TABLE [AdminActivityLogs] ADD [TargetedAdminId] nvarchar(450) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210223173534_UpdatedAdminActivityLog')
BEGIN
    ALTER TABLE [AdminActivityLogs] ADD [TargetedEndUserId] nvarchar(450) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210223173534_UpdatedAdminActivityLog')
BEGIN
    CREATE INDEX [IX_AdminActivityLogs_TargetedAdminId] ON [AdminActivityLogs] ([TargetedAdminId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210223173534_UpdatedAdminActivityLog')
BEGIN
    CREATE INDEX [IX_AdminActivityLogs_TargetedEndUserId] ON [AdminActivityLogs] ([TargetedEndUserId]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210223173534_UpdatedAdminActivityLog')
BEGIN
    ALTER TABLE [AdminActivityLogs] ADD CONSTRAINT [FK_AdminActivityLogs_DigitalAdmins_TargetedAdminId] FOREIGN KEY ([TargetedAdminId]) REFERENCES [DigitalAdmins] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210223173534_UpdatedAdminActivityLog')
BEGIN
    ALTER TABLE [AdminActivityLogs] ADD CONSTRAINT [FK_AdminActivityLogs_DigitalUsers_TargetedEndUserId] FOREIGN KEY ([TargetedEndUserId]) REFERENCES [DigitalUsers] ([Id]) ON DELETE NO ACTION;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210223173534_UpdatedAdminActivityLog')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210223173534_UpdatedAdminActivityLog', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    IF FULLTEXTSERVICEPROPERTY('IsFullTextInstalled') = 1 AND EXISTS(SELECT * FROM sys.fulltext_catalogs WHERE Name='TransactionCatalog')
                    BEGIN
                            DROP FULLTEXT INDEX ON Transactions;
                            DROP FULLTEXT CATALOG TransactionCatalog;
                    END;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var51 sysname;
    SELECT @var51 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Type');
    IF @var51 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var51 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Type] nvarchar(50) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var52 sysname;
    SELECT @var52 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'MemoList');
    IF @var52 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var52 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [MemoList] nvarchar(512) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var53 sysname;
    SELECT @var53 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'MemoData');
    IF @var53 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var53 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [MemoData] nvarchar(512) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var54 sysname;
    SELECT @var54 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Memo');
    IF @var54 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var54 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Memo] nvarchar(256) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var55 sysname;
    SELECT @var55 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'MccCode');
    IF @var55 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var55 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [MccCode] nvarchar(5) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var56 sysname;
    SELECT @var56 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Frequency');
    IF @var56 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var56 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Frequency] nvarchar(20) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var57 sysname;
    SELECT @var57 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'ExternalHash');
    IF @var57 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var57 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [ExternalHash] nvarchar(128) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var58 sysname;
    SELECT @var58 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Description');
    IF @var58 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var58 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Description] nvarchar(512) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var59 sysname;
    SELECT @var59 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'CodeData');
    IF @var59 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var59 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [CodeData] nvarchar(24) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    DECLARE @var60 sysname;
    SELECT @var60 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Code');
    IF @var60 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var60 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Code] nvarchar(24) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    CREATE INDEX [IX_Transactions_Composite] ON [Transactions] ([Description], [Memo], [MemoList], [Type]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210301112331_AddTransactionCompositeIndex')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210301112331_AddTransactionCompositeIndex', N'3.1.7');
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DROP INDEX [IX_AdminActivityLogs_Id] ON [AdminActivityLogs];
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DROP INDEX [IX_Transactions_Composite] ON [Transactions];
    DECLARE @var61 sysname;
    SELECT @var61 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Type');
    IF @var61 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var61 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Type] nvarchar(64) NULL;
    CREATE INDEX [IX_Transactions_Composite] ON [Transactions] ([Description], [Memo], [MemoList], [Type]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DROP INDEX [IX_Transactions_Composite] ON [Transactions];
    DECLARE @var62 sysname;
    SELECT @var62 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'MemoList');
    IF @var62 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var62 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [MemoList] nvarchar(1024) NULL;
    CREATE INDEX [IX_Transactions_Composite] ON [Transactions] ([Description], [Memo], [MemoList], [Type]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DECLARE @var63 sysname;
    SELECT @var63 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'MemoData');
    IF @var63 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var63 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [MemoData] nvarchar(1024) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DECLARE @var64 sysname;
    SELECT @var64 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'MccCode');
    IF @var64 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var64 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [MccCode] nvarchar(8) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DECLARE @var65 sysname;
    SELECT @var65 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Frequency');
    IF @var65 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var65 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Frequency] nvarchar(32) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DROP INDEX [IX_Transactions_Composite] ON [Transactions];
    DECLARE @var66 sysname;
    SELECT @var66 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Description');
    IF @var66 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var66 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Description] nvarchar(256) NULL;
    CREATE INDEX [IX_Transactions_Composite] ON [Transactions] ([Description], [Memo], [MemoList], [Type]);
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DECLARE @var67 sysname;
    SELECT @var67 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'CodeData');
    IF @var67 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var67 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [CodeData] nvarchar(32) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    DECLARE @var68 sysname;
    SELECT @var68 = [d].[name]
    FROM [sys].[default_constraints] [d]
    INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
    WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Transactions]') AND [c].[name] = N'Code');
    IF @var68 IS NOT NULL EXEC(N'ALTER TABLE [Transactions] DROP CONSTRAINT [' + @var68 + '];');
    ALTER TABLE [Transactions] ALTER COLUMN [Code] nvarchar(32) NULL;
END;


IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20210303012642_ReduceActivityLogKeyLengthsToMatch')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20210303012642_ReduceActivityLogKeyLengthsToMatch', N'3.1.7');
END;

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'MetadataDefinitions' and column_name='ProductContexts')
BEGIN
alter table MetadataDefinitions
add ProductContexts nvarchar(2048) null
END;

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'sessions' and column_name='OtpExpireAt')
BEGIN
alter table sessions
add  OtpExpireAt datetime2 null
END;

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Addresses' and column_name='AccountId')
BEGIN
alter table [Addresses]
add [AccountId] [bigint] NULL;
alter table [Addresses]
add CONSTRAINT [FK_Addresses_Accounts_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Accounts] ([Id]);
END;

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'features' and column_name='ParentFeatureFlag')
BEGIN
alter table features 
add ParentFeatureFlag int;
END;

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Merchants' and column_name='IsDefault')
BEGIN
alter table Merchants 
add IsDefault bit not null;
END;