﻿#Revision: 10

[CmdletBinding()]
Param(
    [string]$sqlInstance    = ".",
    [string]$branch         = "",
	[string]$buildId        = "",
	[string]$buildType      = "",
    [string]$useBuildFolder = "",
    [string]$runSQLScript   = "",
    [switch]$resetDatabase,
    [switch]$runAllSQL,
    [switch]$prepareForTestRun,
	[string]$rootURL = "",
	[string]$tcpwd = "",
	[string]$dbuname = "",
	[string]$dbpwd = "",
	[string]$uname = "DGT-CT-DEV-01$",
	[int]$port = ""
)

if($rootURL -eq "")
{
	$rootURL = "http://localhost:8100";
	$port = 8100;
}
else
{
	$array = $rootURL.Split(":");
	$port = $array[2];
	Write-Host "Port is" $port;
}
if($branch -eq "")
{
	if($buildId -ne "")
	{
		$branch = $buildId;
	}
	else
	{
		$branch = "custom";
	}
}
[string]$todaysDate = Get-Date -UFormat "%Y%m%d";
[string]$localFolder = "C:\Digital\" + $todaysDate + "\" + $branch.Replace("/", "_").Replace("<", "_").Replace(">", "_");
[string]$teamCityBuilds = "https://build.jwaala.com/httpAuth/app/rest/builds";
#[string]$localUserName = "$env:computername\$uname";
[string]$localUserName = "$env:computername\Digital_User";
[string]$localUserPassword = "Password1!";
[string]$appPoolName = "DigitalAppPool";
[string]$defaultPool = "DefaultAppPool";
[string]$adminPool = "DigitalAdminPool";
[string]$webSiteName = "Digital";

# Powershell settings
Set-StrictMode -Version 2.0
$DebugPreference = "Continue";
$ErrorActionPreference = "Stop";

Import-Module WebAdministration;

#UTILITY FUNCTIONS----------------------------------------------------------------------------------------------

function CheckPrerequisites()
#---------------------------------------------------------------------------------------------------------------
{
    $major = $PSVersionTable.PSVersion.Major
    if ($major -lt 4)
    {
	    Write-Host "This script requires Powershell version 4.0 or greater" -ForegroundColor Red
	    exit 1
    }

    dotnet --info | Out-Null;
    if ($LastExitCode -ne 0)
    {
        Write-Host ".NET Core Runtime 2.1.4 or later is required" -ForegroundColor Red
        exit 1
    }

    #$dotnetVersion = dotnet --version;
    #if ([Version]$dotnetVersion -lt [Version]"2.1.400")
    #{
    #    Write-Host ".NET Core Runtime 2.1.4 or later is required" -ForegroundColor Red
    #    exit
    #}
     
    if (!(Test-Path -Path $localFolder))
    {
        New-Item -ItemType directory -Path $localFolder;
    }

    if ($useBuildFolder -ne "")
    {
        if (!(Test-Path -Path $useBuildFolder))
        {
            Write-Host "Build folder not found: $useBuildFolder" -ForegroundColor Red
            exit 1
        }
    }

    if ($runSqlScript -ne "")
    {
        if (!(Test-Path -Path $runSqlScript))
        {
            Write-Host "SQL script not found: $runSqlScript" -ForegroundColor Red
            exit 1
        }
    }
}

function FindLocalBuild()
#---------------------------------------------------------------------------------------------------------------
{
    if ($useBuildFolder -ne "")
    {
        Write-Host "Using build folder: $useBuildFolder";
        $script:buildFolder = $useBuildFolder;
		$localBuildFolder = $useBuildFolder;
        return;
    }

    $localBuildFolder = Get-ChildItem -Directory $localFolder | `
        Where-Object {$_.Name -match "^\d+$"} | `
        Sort-Object -Descending | `
        Select-Object -First 1;

    if ($localBuildFolder)
    {
        $script:buildFolder = $localBuildFolder.FullName;
        Write-Host "Found local build: $script:buildFolder" -ForegroundColor Green;
    }
}

function DownloadBuild()
#---------------------------------------------------------------------------------------------------------------
{
    # if ($useBuildFolder -ne "")
    # {
        # return;
    # }

    $password = ConvertTo-SecureString $tcpwd -AsPlainText -Force

	$creds = New-Object System.Management.Automation.PSCredential ("db2-readonly", $password)
add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy;
	if ($buildId -eq "")
	{
		$response = Invoke-WebRequest "$teamCityBuilds/?locator=branch:$branch,buildType:$buildType,count:1,state:finished,status:success" -Credential $creds -UseBasicParsing;
		$contentXml = [xml]$response.Content;
		$buildNumber = $contentXml.builds.build.number;
		Write-Host "Latest build number is $buildNumber";
	}
	else
	{
		$response = Invoke-WebRequest "$teamCityBuilds/?locator=id:$buildId,state:finished,status:success" -Credential $creds -UseBasicParsing;
		$contentXml = [xml]$response.Content;
		$buildNumber = $contentXml.builds.build.number;
		Write-Host "Requested build number is $buildNumber";
	}
    
	$buildId = $contentXml.builds.build.id;	

    $script:buildFolder = "$localFolder\$buildNumber";

    if (Test-Path "$script:buildFolder")
    {
        Write-Host "Latest build already downloaded";
        return;
    }

    $tempFile = "$env:TEMP\Digital_$buildNumber.zip";

    Write-Host "Downloading artifact...";

    $repsonse2 = Invoke-WebRequest "$teamCityBuilds/id:$buildId/artifacts/archived" -Credential $creds -OutFile $tempFile;

    Expand-Archive $tempFile -DestinationPath $script:buildFolder -Force;

    Write-Host "Extracted to $script:buildFolder" -ForegroundColor Green;;
}

function ExecuteSQL($database, $sqlCommand)
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "Entering ExecuteSQL";
	if($dbuname -eq "")
	{
		$connectionString = "Data Source=$script:sqlInstance;Integrated Security=SSPI;Initial Catalog=$database";
	}
	else
	{
    # $connectionString = "Data Source=$script:sqlInstance;Integrated Security=SSPI;Initial Catalog=$database";
	$connectionString = 'Data Source={0};database={1};User ID={2};Password={3}' -f $script:sqlInstance,$database,$dbuname,$dbpwd;
	}

    $connection = New-Object System.Data.SqlClient.SQLConnection($connectionString);
    $command = New-Object System.Data.Sqlclient.SQLCommand($sqlCommand, $connection);
    $connection.Open();
    try
    {
        $command.ExecuteNonQuery() | Out-Null;
		Write-Host "Exiting ExecuteSQL";
    }
    finally
    {
        $connection.Close();
    }
}

function DisablePasswordExpiryForUser($user)
#---------------------------------------------------------------------------------------------------------------
{
    $userObject = Get-WmiObject -Class Win32_UserAccount -Namespace "root\cimv2" -Filter "LocalAccount='$True'" | where { $_.Name -eq $user };
    $userObject.PasswordExpires = $false;
    $userObject.Put() | Out-Null;

    Write-Debug "Disabled password expiry for $user";
}

function CreateLocalUserAccount()
#---------------------------------------------------------------------------------------------------------------
{
    $user = $localUserName.Split('\')[1]; #Remove computer name
    $localUsers = ([ADSI]"WinNT://$env:computername").Children;
    $localAccount = $localUsers | where { $_.Name -eq $user }

    if ($localAccount -eq $null)
    {
        net user $user $localUserPassword /add /expires:never /passwordchg:no
        Write-Host "Created local user $user" -ForegroundColor Green;
    }
    
    DisablePasswordExpiryForUser $user;
}

function CreateApplicationPool()
#---------------------------------------------------------------------------------------------------------------
{
    if (!(Test-Path "IIS:\AppPools\DefaultAppPool"))
    {
        $defaultPool = New-Item "IIS:\AppPools\DefaultAppPool";
        $defaultPool.managedRuntimeVersion = "";
        $defaultPool.processModel.identityType = 3;
        $defaultPool.processModel.userName = $localUserName;
        $defaultPool.processModel.password = $localUserPassword;
        $defaultPool | Set-Item;

        Write-Host "Created IIS application pool DefaultAppPool"  -ForegroundColor Green;
    }
	if (!(Test-Path "IIS:\AppPools\DigitalAdminPool"))
    {
        $adminPool = New-Item "IIS:\AppPools\DigitalAdminPool";
        $adminPool.managedRuntimeVersion = "";
        $adminPool.processModel.identityType = 3;
        $adminPool.processModel.userName = $localUserName;
        $adminPool.processModel.password = $localUserPassword;
        $adminPool | Set-Item;

        Write-Host "Created IIS application pool DigitalAdminPool"  -ForegroundColor Green;
    }
	
	if (!(Test-Path "IIS:\AppPools\DigitalAppPool"))
    {
        $appPool = New-Item "IIS:\AppPools\DigitalAppPool";
        $appPool.managedRuntimeVersion = "";
        $appPool.processModel.identityType = 3;
        $appPool.processModel.userName = $localUserName;
        $appPool.processModel.password = $localUserPassword;
        $appPool | Set-Item;

        Write-Host "Created IIS application pool DigitalAppPool"  -ForegroundColor Green;
    }
}

function CreateWebSite()
#---------------------------------------------------------------------------------------------------------------
{
    if (!(Test-Path "IIS:\Sites\\$webSiteName"))
    {
        New-WebSite -Name $webSiteName -ApplicationPool $defaultPool -PhysicalPath $script:buildFolder\Admin -Port $port;

        Write-Host "Created web site - $webSiteName" -ForegroundColor Green;
    }
    else
    {
       Set-ItemProperty -Path "IIS:\Sites\\$webSiteName" -Name physicalPath -Value $script:buildFolder\Admin;

        Write-Host "Updated path of IIS web site - $webSiteName"  -ForegroundColor Green;
    }
}

function CreateVirtualDirectories()
#---------------------------------------------------------------------------------------------------------------
{
    if (!(Test-Path "IIS:\Sites\\$webSiteName\app"))
    {
        New-WebApplication -Name app -Site $webSiteName -PhysicalPath $script:buildFolder\User -ApplicationPool $appPoolName;
        Write-Host "Created virtual directory - app" -ForegroundColor Green;
    }
    else
    {
        Set-ItemProperty -Path "IIS:\Sites\\$webSiteName\app" -Name physicalPath -Value $script:buildFolder\User;
        Write-Host "Updated path of virtual directory - app"  -ForegroundColor Green;
    }
    
    if (!(Test-Path "IIS:\Sites\\$webSiteName\admin"))
    {
        New-WebApplication -Name admin -Site $webSiteName -PhysicalPath $script:buildFolder\Admin -ApplicationPool $adminPool;
        Write-Host "Created virtual directory - admin" -ForegroundColor Green;
    }
    else
    {
        Set-ItemProperty -Path "IIS:\Sites\\$webSiteName\admin" -Name physicalPath -Value $script:buildFolder\Admin;
        Write-Host "Updated path of virtual directory - admin"  -ForegroundColor Green;
    }
}

function UpdateWebConfig()
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "In UpdateWebConfig"
    $configFile = "$script:buildFolder\User\web.config";
    [xml]$xml = Get-Content $configFile;
    # $node = $xml.SelectSingleNode("//handlers")
    # $xmlElt = $xml.CreateElement("remove")
    # $xmlAtt = $xml.CreateAttribute("name")
    # $xmlAtt.Value = "aspNetCore"
    # $xmlElt.Attributes.Append($xmlAtt) | out-null;
    # $node.PrependChild($xmlElt) | out-null;
    # $xml.save($ConfigFile)
	$parent_xpath = '/configuration/location/system.webServer'
    $nodes = $xml.SelectNodes($parent_xpath)
    $nodes | % {
	try{
        $child_node = $_.SelectSingleNode('rewrite')
        $_.RemoveChild($child_node) | Out-Null
		}
		catch{}
    }
	$xml.save($ConfigFile)
	
    $configFile = "$script:buildFolder\Admin\web.config";
    [xml]$xml = Get-Content $configFile;
    # $node = $xml.SelectSingleNode("//handlers")
    # $xmlElt = $xml.CreateElement("remove")
    # $xmlAtt = $xml.CreateAttribute("name")
    # $xmlAtt.Value = "aspNetCore"
    # $xmlElt.Attributes.Append($xmlAtt) | out-null;
    # $node.PrependChild($xmlElt) | out-null;
	$parent_xpath = '/configuration/location/system.webServer'
    $nodes = $xml.SelectNodes($parent_xpath)
    $nodes | % {
        try{
        $child_node = $_.SelectSingleNode('rewrite')
        $_.RemoveChild($child_node) | Out-Null
		}
		catch{}
    }
	$xml.save($ConfigFile)
	
}

function UpdateAppSettingsJson()
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "Start UpdateAppSettingsJson"
	$jsonFile = "$script:buildFolder\User\appsettings.json";
	$jsonContent = Get-Content $jsonFile | ConvertFrom-Json;
	if($dbuname -eq "")
	{
		$jsonContent.ConnectionStrings.DefaultConnection = $jsonContent.ConnectionStrings.DefaultConnection -replace "Server=.*?;", "Server=$sqlInstance;" 
		$jsonContent.FeatureDefinitionFile = "$($script:buildFolder)\features.json";
		$jsonContent | Add-Member -Type NoteProperty -Name 'DbInitializationScriptsDirectory' -Value "$($script:buildFolder)\InitScripts" -Force
		ConvertTo-Json $jsonContent -Depth 4 | Out-File $jsonFile -Force;
		
		$jsonFile = "$script:buildFolder\Admin\appsettings.json";
		$jsonContent = Get-Content $jsonFile | ConvertFrom-Json;
		$jsonContent.ConnectionStrings.DefaultConnection = $jsonContent.ConnectionStrings.DefaultConnection -replace "Server=.*?;", "Server=$sqlInstance;" 
		$jsonContent | Add-Member -Type NoteProperty -Name 'DbInitializationScriptsDirectory' -Value "$($script:buildFolder)\InitScripts" -Force
		ConvertTo-Json $jsonContent -Depth 4 | Out-File $jsonFile -Force;
	}
	else
	{
		$jsonContent.ConnectionStrings.DefaultConnection = $jsonContent.ConnectionStrings.DefaultConnection -replace "Server=.*?;", "Server=$sqlInstance;" 
		$jsonContent.ConnectionStrings.DefaultConnection = $jsonContent.ConnectionStrings.DefaultConnection -replace "Trusted_Connection=True", "user id=$dbuname;password=$dbpwd"
		$jsonContent.FeatureDefinitionFile = "$($script:buildFolder)\features.json";
		$jsonContent | Add-Member -Type NoteProperty -Name 'DbInitializationScriptsDirectory' -Value "$($script:buildFolder)\InitScripts" -Force
		ConvertTo-Json $jsonContent -Depth 4 | Out-File $jsonFile -Force;
		
		$jsonFile = "$script:buildFolder\Admin\appsettings.json";
		$jsonContent = Get-Content $jsonFile | ConvertFrom-Json;
		$jsonContent.ConnectionStrings.DefaultConnection = $jsonContent.ConnectionStrings.DefaultConnection -replace "Server=.*?;", "Server=$sqlInstance;" 
		$jsonContent.ConnectionStrings.DefaultConnection = $jsonContent.ConnectionStrings.DefaultConnection -replace "Trusted_Connection=True", "user id=$dbuname;password=$dbpwd"
		$jsonContent | Add-Member -Type NoteProperty -Name 'DbInitializationScriptsDirectory' -Value "$($script:buildFolder)\InitScripts" -Force
		ConvertTo-Json $jsonContent -Depth 4 | Out-File $jsonFile -Force;
	}

	
}

function UpdateURLinJSFiles()
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "Start UpdateURLinJSFiles"
    $jsFiles = Get-ChildItem $script:buildFolder *.js -rec;
	# Write-Host $jsFiles
    foreach ($file in $jsFiles)
    {
	# Write-Host "$file - " [System.IO.File]::Exists($file.PSPath)

	        # If ( ((Get-Item $file.PSPath).length -gt 0kb) -and ((Get-Content $file.PSPath).Contains("https://test-digital.alogent.com") -or (Get-Content $file.PSPath).Contains("http://localhost:8100") -or (Get-Content $file.PSPath).Contains("https://nxt-stage.alogent.com")))
        # {
            # Write-Host $file.PSPath;
            (Get-Content $file.PSPath).replace("http://localhost:8100", $rootURL) | Set-Content $file.PSPath;
            (Get-Content $file.PSPath).replace("https://test-digital.alogent.com", $rootURL) | Set-Content $file.PSPath;
			(Get-Content $file.PSPath).replace("https://nxt-stage.alogent.com", $rootURL) | Set-Content $file.PSPath;
			 
        # }
       
    }
}

function CreateDatabase()
#---------------------------------------------------------------------------------------------------------------
{
    if ($resetDatabase.IsPresent)
    {
	try{
		Write-Host "Dropping Existing Database..."
        ExecuteSQL MASTER "IF EXISTS (SELECT name FROM sys.databases WHERE name = 'AlogentDigital') DROP DATABASE AlogentDigital";
		}
		catch
		{}
    }
	Write-Host "Creating the Database..."
	Start-Sleep -s 10
    ExecuteSQL MASTER "IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = 'AlogentDigital') CREATE DATABASE AlogentDigital";
	Write-Host "End CreateDatabase"
}

function CreateSQLUser()
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "Start CreateSQLUser"
    ExecuteSQL MASTER "IF NOT EXISTS (SELECT * FROM DBO.SYSLOGINS WHERE NAME = '$localUserName') CREATE LOGIN [$localUserName] FROM WINDOWS";
	Write-Host "CreateSQLUser -2"
    ExecuteSQL MASTER "EXEC sp_addsrvrolemember '$localUserName', 'sysadmin'";
	Write-Host "CreateSQLUser -3"
	Write-Host "End CreateSQLUser"
}

function SetSeedDatabase()
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "Start SetSeedDatabase"
    $jsonFile = "$script:buildFolder\User\appsettings.json";
    
    $json = Get-Content $jsonfile | Out-String | ConvertFrom-Json;
    $json | Add-Member -Type NoteProperty -Name 'SeedDatabase' -Value '1' -Force;
    $json | ConvertTo-Json | Set-Content $jsonfile;
	Write-Host "End SetSeedDatabase"
}

function SetLogFolder()
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "Start SetLogFolder"
    $jsonFile = "$script:buildFolder\User\appsettings.Production.json";
    $logFolder = "$script:buildFolder\Logs";

    $json = Get-Content $jsonfile | Out-String | ConvertFrom-Json;
    $json.Serilog | Add-Member -Type NoteProperty -Name 'LogFolder' -Value "$logFolder" -Force;
    $json | ConvertTo-Json | Set-Content $jsonfile;

    if (!(Test-Path -Path $logFolder))
    {
        New-Item -ItemType directory -Path $logFolder;
    }
	Write-Host "End SetLogFolder"
}

function SetEnvironmentVariables()
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "Start SetEnvironmentVariables"
    [Environment]::SetEnvironmentVariable("ALOGENT_DATA_KEY", "KgEPNBGmjgPUa7Lp7fjrFA==", "Machine")
    [Environment]::SetEnvironmentVariable("ALOGENT_JWT_SIGNINGKEY", "KgEPNBGmjgPUa7Lp7fjrFA==", "Machine")
	[Environment]::SetEnvironmentVariable("ASPNETCORE_ENVIRONMENT", "Production", "Machine")
}

function InvokeApp()
#---------------------------------------------------------------------------------------------------------------
{

#just creating a copy of sql scripts under InitScripts folder
	$src = "$PSScriptRoot"
	$dst = "$script:buildFolder\InitScripts\scripts"
	if (!(Test-Path -Path $dst))
    {
        New-Item -ItemType directory -Path $dst;
    }

	Get-ChildItem $src -Filter "*.sql" | Copy-Item -Destination $dst -Force
	Start-Process "iisreset.exe" -NoNewWindow -Wait;
    Write-Host "Invoking user application URL";
	[int]$Retries = 3
    [int]$SecondsDelay = 60
	$retryCount = 0
    $completed = $false
    $response = $null
	while (-not $completed) {
		try 
		{
			$response = Invoke-WebRequest "$rootURL/app"; 
			Write-Host "Application initialized successfully" -ForegroundColor Green;
			$completed = $true
		} 
		catch 
		{
		
				if ($retrycount -ge $Retries) {
				try{
					Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ -ForegroundColor Red;
					Write-Host "Details:" $_.ErrorDetails.Message -ForegroundColor Red;
					Write-Host;
					Write-Host "Application failed to initialize!" -ForegroundColor Red
					Write-Host;
					break;
					}
					catch{
					break;
					}
				} else {
					Write-Warning "Request to $rootURL/app failed. Retrying in $SecondsDelay seconds."
					Start-Sleep $SecondsDelay
					$retrycount++
				}	
			
		}
	}
	Write-Host "Start UpdateAppSettingsJson"
    $jsonFile = "$script:buildFolder\User\appsettings.json";
    $jsonContent = Get-Content $jsonFile | ConvertFrom-Json;
	$jsonContent.PsObject.Members.Remove('DbInitializationScriptsDirectory');
    $jsonContent | Add-Member -Type NoteProperty -Name 'DbInitializationScriptsDirectory' -Value "$($script:buildFolder)\InitScripts\scripts" -Force
    ConvertTo-Json $jsonContent -Depth 4 | Out-File $jsonFile -Force;
	
	# restarting iis as we targetted the second set of sql scripts to runAllSQL
	Start-Process "iisreset.exe" -NoNewWindow -Wait;
    Write-Host "Re-Invoking user application URL";
	[int]$Retries = 3
    [int]$SecondsDelay = 60
	$retryCount = 0
    $completed = $false
    $response = $null
	while (-not $completed) {
		try 
		{
			$response = Invoke-WebRequest "$rootURL/app"; 
			Write-Host "Application initialized successfully" -ForegroundColor Green;
			$completed = $true
		} 
		catch 
		{
		
			if ($retrycount -ge $Retries) {
			try{
                Write-Host "StatusCode:" $_.Exception.Response.StatusCode.value__ -ForegroundColor Red;
				Write-Host "Details:" $_.ErrorDetails.Message -ForegroundColor Red;
				Write-Host;
				Write-Host "Application failed to initialize!" -ForegroundColor Red
				Write-Host;
				break;
				}
					catch{
					break;
					}
				} else {
					Write-Warning "Request to $rootURLapp failed. Retrying in $SecondsDelay seconds."
					Start-Sleep $SecondsDelay
					$retrycount++
				}
            			
		}
	}
}

function RunSqlScript()
#---------------------------------------------------------------------------------------------------------------
{
    if ($runSqlScript -ne "")
    {
	    $args = "-d AlogentDigital -E -i $runSQLScript -S $sqlInstance -V 1 -f 65001";

        Invoke-Expression "& sqlcmd --% $args";

        if ($LASTEXITCODE -ne 0)
        {
            Write-Host "Failed to execute $runSQLScript. Error code: $LASTEXITCODE" -ForegroundColor Red;
            Write-Host;
            exit 1;
        }

        Write-Host "Executed $runSQLScript" -ForegroundColor Green;
    }
}

function RunAllSQL()
#---------------------------------------------------------------------------------------------------------------
{
	Write-Host "Running Script : RunAllSQL()"
	Write-Host "runAllSQL is "$runAllSQL
	
        $scripts = Get-ChildItem | Where-Object {$_.Extension -eq ".sql"}
		Write-Host "sql files "$scripts
    if ($runAllSQL.IsPresent)
    {
		Write-Host "runAllSQL is true"
        $scripts = Get-ChildItem | Where-Object {$_.Extension -eq ".sql"}
  
        foreach ($s in $scripts)
        {
		try{
			Start-Sleep -s 1
            Write-Host "Running Script : " $s.Name -BackgroundColor DarkGreen -ForegroundColor White
            $script = $s.FullName
            $args = "-d AlogentDigital -E -i $s -S $sqlInstance -V 1 -f 65001";
            Invoke-Expression "& sqlcmd --% $args";

            if ($LASTEXITCODE -ne 0)
            {
                Write-Host "Failed to execute $runSQLScript. Error code: $LASTEXITCODE" -ForegroundColor Red;
                Write-Host;
                continue;
            }

            Write-Host "Executed $script" -ForegroundColor Green;
			}
			catch
			{}
        }
    }
}

function SetMockSeedData()
#---------------------------------------------------------------------------------------------------------------
{
    $jsonFile = "$script:buildFolder\User\mockSeedData.json";
    $json = Get-Content $jsonfile | Out-String | ConvertFrom-Json;
    
    $json.mockSeedData.mortgageSettings.transactionMonthsToGenerate = 0
    $json.mockSeedData.shareSettings.transactionsPerShareToGenerate = 0
    $json.mockSeedData.loanSettings.transactionsPerLoanToGenerate = 0
    
    $json | ConvertTo-Json | Set-Content $jsonfile;
}

#MAIN LOGIC--------------------------------------------------------------------------------------------------

CheckPrerequisites;

FindLocalBuild;
DownloadBuild;

CreateLocalUserAccount;
CreateApplicationPool;
CreateWebSite;
CreateVirtualDirectories;
UpdateWebConfig;
UpdateAppSettingsJson;
UpdateURLinJSFiles;

CreateDatabase;
CreateSQLUser;
SetSeedDatabase;
SetLogFolder;

SetEnvironmentVariables;

if ($prepareForTestRun)
{
    SetMockSeedData;
}
InvokeApp;
# RunAllSQL;
RunSqlScript;

Write-Host;
Write-Host "To access application, launch the browser and navigate to:" -ForegroundColor Yellow;
Write-Host "    User : $rootURL/app" -ForegroundColor Yellow;
Write-Host "    Admin: $rootURL/admin" -ForegroundColor Yellow;
Write-Host;