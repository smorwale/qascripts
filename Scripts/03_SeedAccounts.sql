DECLARE 
    @AccountNumber VARCHAR(1000) = '111223333', 
    @SourceId BIGINT = 0, 
    @Type VARCHAR(1000) = 'Consumer', 
    @PortfolioId BIGINT = 0,
    @MultiAccountPortfolioKey VARCHAR(1000) = '999880000',
    @MultiAccountNumber1 VARCHAR(1000) = '111998801',
    @MultiAccountNumber2 VARCHAR(1000) = '111998802'

SELECT TOP 1 @SourceId = Id FROM [dbo].Sources

BEGIN TRAN

    SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END
	
	SET @PortfolioId = 0
	SET @AccountNumber = '000000001'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000002'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000003'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000004'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000005'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000006'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000007'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000008'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000009'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000010'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
	SET @AccountNumber = '000000011'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END
	
	SET @PortfolioId = 0
	SET @AccountNumber = '000000012'
	SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @AccountNumber
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@AccountNumber)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @AccountNumber) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @AccountNumber, @AccountNumber, @Type, @PortfolioId)
    END

	SET @PortfolioId = 0
    SELECT @PortfolioId = Id FROM Portfolios WHERE Identifier = @MultiAccountPortfolioKey
    IF (@PortfolioId = 0)
    BEGIN
        INSERT INTO Portfolios
            (Identifier) 
        VALUES
            (@MultiAccountPortfolioKey)

        SET @PortfolioId = @@IDENTITY
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @MultiAccountNumber1) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @MultiAccountNumber1, @MultiAccountNumber1, @Type, @PortfolioId)
    END

    IF (NOT EXISTS (SELECT * FROM Accounts WITH (UPDLOCK,SERIALIZABLE) WHERE ExternalKey = @MultiAccountNumber2) AND (@SourceId > 0))
    BEGIN
        INSERT INTO Accounts 
            (InsertAt, UpdateAt, SourceId, ExternalKey, Number, [Type], PortfolioId)
        VALUES
            (GETDATE(), GETDATE(), @SourceId, @MultiAccountNumber2, @MultiAccountNumber2, @Type, @PortfolioId)
    END

COMMIT TRAN